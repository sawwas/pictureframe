package com.luck.picture.lib.observable;

public abstract interface SubjectListener
{
  public abstract void add(ObserverListener paramObserverListener);
  
  public abstract void remove(ObserverListener paramObserverListener);
}


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\observable\SubjectListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */