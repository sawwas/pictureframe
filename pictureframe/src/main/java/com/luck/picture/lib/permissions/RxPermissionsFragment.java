/*     */ package com.luck.picture.lib.permissions;
/*     */ 
/*     */ import android.annotation.TargetApi;
/*     */ import android.app.Activity;
/*     */ import android.app.Fragment;
/*     */ import android.content.pm.PackageManager;
/*     */ import android.os.Bundle;
/*     */ import android.support.annotation.NonNull;
/*     */ import android.util.Log;
/*     */ import io.reactivex.subjects.PublishSubject;
/*     */ import java.util.HashMap;
/*     */ import java.util.Map;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RxPermissionsFragment
/*     */   extends Fragment
/*     */ {
/*     */   private static final int PERMISSIONS_REQUEST_CODE = 42;
/*  30 */   private Map<String, PublishSubject<Permission>> mSubjects = new HashMap();
/*     */   
/*     */ 
/*     */   private boolean mLogging;
/*     */   
/*     */ 
/*     */   public void onCreate(Bundle savedInstanceState)
/*     */   {
/*  38 */     super.onCreate(savedInstanceState);
/*  39 */     setRetainInstance(true);
/*     */   }
/*     */   
/*     */   @TargetApi(23)
/*     */   void requestPermissions(@NonNull String[] permissions) {
/*  44 */     requestPermissions(permissions, 42);
/*     */   }
/*     */   
/*     */   @TargetApi(23)
/*     */   public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
/*  49 */     super.onRequestPermissionsResult(requestCode, permissions, grantResults);
/*     */     
/*  51 */     if (requestCode != 42) { return;
/*     */     }
/*  53 */     boolean[] shouldShowRequestPermissionRationale = new boolean[permissions.length];
/*     */     
/*  55 */     for (int i = 0; i < permissions.length; i++) {
/*  56 */       shouldShowRequestPermissionRationale[i] = shouldShowRequestPermissionRationale(permissions[i]);
/*     */     }
/*     */     
/*  59 */     onRequestPermissionsResult(permissions, grantResults, shouldShowRequestPermissionRationale);
/*     */   }
/*     */   
/*     */   void onRequestPermissionsResult(String[] permissions, int[] grantResults, boolean[] shouldShowRequestPermissionRationale) {
/*  63 */     int i = 0; for (int size = permissions.length; i < size; i++) {
/*  64 */       log("onRequestPermissionsResult  " + permissions[i]);
/*     */       
/*  66 */       PublishSubject<Permission> subject = (PublishSubject)this.mSubjects.get(permissions[i]);
/*  67 */       if (subject == null)
/*     */       {
/*  69 */         Log.e("RxPermissions", "RxPermissions.onRequestPermissionsResult invoked but didn't find the corresponding permission request.");
/*  70 */         return;
/*     */       }
/*  72 */       this.mSubjects.remove(permissions[i]);
/*  73 */       boolean granted = grantResults[i] == 0;
/*  74 */       subject.onNext(new Permission(permissions[i], granted, shouldShowRequestPermissionRationale[i]));
/*  75 */       subject.onComplete();
/*     */     }
/*     */   }
/*     */   
/*     */   @TargetApi(23)
/*     */   boolean isGranted(String permission) {
/*  81 */     return getActivity().checkSelfPermission(permission) == 0;
/*     */   }
/*     */   
/*     */   @TargetApi(23)
/*     */   boolean isRevoked(String permission) {
/*  86 */     return getActivity().getPackageManager().isPermissionRevokedByPolicy(permission, getActivity().getPackageName());
/*     */   }
/*     */   
/*     */   public void setLogging(boolean logging) {
/*  90 */     this.mLogging = logging;
/*     */   }
/*     */   
/*     */   public PublishSubject<Permission> getSubjectByPermission(@NonNull String permission) {
/*  94 */     return (PublishSubject)this.mSubjects.get(permission);
/*     */   }
/*     */   
/*     */   public boolean containsByPermission(@NonNull String permission) {
/*  98 */     return this.mSubjects.containsKey(permission);
/*     */   }
/*     */   
/*     */   public PublishSubject<Permission> setSubjectForPermission(@NonNull String permission, @NonNull PublishSubject<Permission> subject) {
/* 102 */     return (PublishSubject)this.mSubjects.put(permission, subject);
/*     */   }
/*     */   
/*     */   void log(String message) {
/* 106 */     if (this.mLogging) {
/* 107 */       Log.d("RxPermissions", message);
/*     */     }
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\permissions\RxPermissionsFragment.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */