/*     */ package com.luck.picture.lib.config;
/*     */ 
/*     */ import android.os.Parcel;
/*     */ import android.os.Parcelable;
/*     */ import android.os.Parcelable.Creator;
/*     */ import android.support.annotation.StyleRes;
/*     */ import com.luck.picture.lib.R.style;
/*     */ import com.luck.picture.lib.entity.LocalMedia;
/*     */ import com.luck.picture.lib.tools.DebugUtil;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class PictureSelectionConfig
/*     */   implements Parcelable
/*     */ {
/*     */   public int mimeType;
/*     */   public boolean camera;
/*     */   public String outputCameraPath;
/*     */   @StyleRes
/*     */   public int themeStyleId;
/*     */   public int selectionMode;
/*     */   public int maxSelectNum;
/*     */   public int minSelectNum;
/*     */   public int videoQuality;
/*     */   public int cropCompressQuality;
/*     */   public int videoMaxSecond;
/*     */   public int videoMinSecond;
/*     */   public int recordVideoSecond;
/*     */   public int compressMaxkB;
/*     */   public int compressGrade;
/*     */   public int imageSpanCount;
/*     */   public int compressMode;
/*     */   public int compressWidth;
/*     */   public int compressHeight;
/*     */   public int overrideWidth;
/*     */   public int overrideHeight;
/*     */   public int aspect_ratio_x;
/*     */   public int aspect_ratio_y;
/*     */   public float sizeMultiplier;
/*     */   public int cropWidth;
/*     */   public int cropHeight;
/*     */   public boolean zoomAnim;
/*     */   public boolean isCompress;
/*     */   public boolean isCamera;
/*     */   public boolean isGif;
/*     */   public boolean enablePreview;
/*     */   public boolean enPreviewVideo;
/*     */   public boolean enablePreviewAudio;
/*     */   public boolean checkNumMode;
/*     */   public boolean openClickSound;
/*     */   public boolean enableCrop;
/*     */   public boolean freeStyleCropEnabled;
/*     */   public boolean circleDimmedLayer;
/*     */   public boolean showCropFrame;
/*     */   public boolean showCropGrid;
/*     */   public boolean hideBottomControls;
/*     */   public boolean rotateEnabled;
/*     */   public boolean scaleEnabled;
/*     */   public boolean previewEggs;
/*     */   public List<LocalMedia> selectionMedias;
/*     */   
/*     */   private void reset()
/*     */   {
/*  72 */     this.mimeType = 1;
/*  73 */     this.camera = false;
/*  74 */     this.themeStyleId = R.style.picture_default_style;
/*  75 */     this.selectionMode = 2;
/*  76 */     this.maxSelectNum = 9;
/*  77 */     this.minSelectNum = 0;
/*  78 */     this.videoQuality = 1;
/*  79 */     this.cropCompressQuality = 90;
/*  80 */     this.videoMaxSecond = 0;
/*  81 */     this.videoMinSecond = 0;
/*  82 */     this.recordVideoSecond = 60;
/*  83 */     this.compressMaxkB = 102400;
/*  84 */     this.compressGrade = 3;
/*  85 */     this.imageSpanCount = 4;
/*  86 */     this.compressMode = 1;
/*  87 */     this.compressWidth = 0;
/*  88 */     this.compressHeight = 0;
/*  89 */     this.overrideWidth = 0;
/*  90 */     this.overrideHeight = 0;
/*  91 */     this.isCompress = false;
/*  92 */     this.aspect_ratio_x = 0;
/*  93 */     this.aspect_ratio_y = 0;
/*  94 */     this.cropWidth = 0;
/*  95 */     this.cropHeight = 0;
/*  96 */     this.isCamera = true;
/*  97 */     this.isGif = false;
/*  98 */     this.enablePreview = true;
/*  99 */     this.enPreviewVideo = true;
/* 100 */     this.enablePreviewAudio = true;
/* 101 */     this.checkNumMode = false;
/* 102 */     this.openClickSound = false;
/* 103 */     this.enableCrop = false;
/* 104 */     this.freeStyleCropEnabled = false;
/* 105 */     this.circleDimmedLayer = false;
/* 106 */     this.showCropFrame = true;
/* 107 */     this.showCropGrid = true;
/* 108 */     this.hideBottomControls = true;
/* 109 */     this.rotateEnabled = true;
/* 110 */     this.scaleEnabled = true;
/* 111 */     this.previewEggs = false;
/* 112 */     this.zoomAnim = true;
/* 113 */     this.outputCameraPath = "";
/* 114 */     this.sizeMultiplier = 0.5F;
/* 115 */     this.selectionMedias = new ArrayList();
/* 116 */     DebugUtil.i("*******", "reset PictureSelectionConfig");
/*     */   }
/*     */   
/*     */   public static PictureSelectionConfig getInstance() {
/* 120 */     return InstanceHolder.INSTANCE;
/*     */   }
/*     */   
/*     */   public static PictureSelectionConfig getCleanInstance() {
/* 124 */     PictureSelectionConfig selectionSpec = getInstance();
/* 125 */     selectionSpec.reset();
/* 126 */     return selectionSpec;
/*     */   }
/*     */   
/*     */   private static final class InstanceHolder {
/* 130 */     private static final PictureSelectionConfig INSTANCE = new PictureSelectionConfig();
/*     */   }
/*     */   
/*     */   public int describeContents()
/*     */   {
/* 135 */     return 0;
/*     */   }
/*     */   
/*     */   public void writeToParcel(Parcel dest, int flags)
/*     */   {
/* 140 */     dest.writeInt(this.mimeType);
/* 141 */     dest.writeByte((byte)(this.camera ? 1 : 0));
/* 142 */     dest.writeString(this.outputCameraPath);
/* 143 */     dest.writeInt(this.themeStyleId);
/* 144 */     dest.writeInt(this.selectionMode);
/* 145 */     dest.writeInt(this.maxSelectNum);
/* 146 */     dest.writeInt(this.minSelectNum);
/* 147 */     dest.writeInt(this.videoQuality);
/* 148 */     dest.writeInt(this.cropCompressQuality);
/* 149 */     dest.writeInt(this.videoMaxSecond);
/* 150 */     dest.writeInt(this.videoMinSecond);
/* 151 */     dest.writeInt(this.recordVideoSecond);
/* 152 */     dest.writeInt(this.compressMaxkB);
/* 153 */     dest.writeInt(this.compressGrade);
/* 154 */     dest.writeInt(this.imageSpanCount);
/* 155 */     dest.writeInt(this.compressMode);
/* 156 */     dest.writeInt(this.compressWidth);
/* 157 */     dest.writeInt(this.compressHeight);
/* 158 */     dest.writeInt(this.overrideWidth);
/* 159 */     dest.writeInt(this.overrideHeight);
/* 160 */     dest.writeInt(this.aspect_ratio_x);
/* 161 */     dest.writeInt(this.aspect_ratio_y);
/* 162 */     dest.writeFloat(this.sizeMultiplier);
/* 163 */     dest.writeInt(this.cropWidth);
/* 164 */     dest.writeInt(this.cropHeight);
/* 165 */     dest.writeByte((byte)(this.zoomAnim ? 1 : 0));
/* 166 */     dest.writeByte((byte)(this.isCompress ? 1 : 0));
/* 167 */     dest.writeByte((byte)(this.isCamera ? 1 : 0));
/* 168 */     dest.writeByte((byte)(this.isGif ? 1 : 0));
/* 169 */     dest.writeByte((byte)(this.enablePreview ? 1 : 0));
/* 170 */     dest.writeByte((byte)(this.enPreviewVideo ? 1 : 0));
/* 171 */     dest.writeByte((byte)(this.enablePreviewAudio ? 1 : 0));
/* 172 */     dest.writeByte((byte)(this.checkNumMode ? 1 : 0));
/* 173 */     dest.writeByte((byte)(this.openClickSound ? 1 : 0));
/* 174 */     dest.writeByte((byte)(this.enableCrop ? 1 : 0));
/* 175 */     dest.writeByte((byte)(this.freeStyleCropEnabled ? 1 : 0));
/* 176 */     dest.writeByte((byte)(this.circleDimmedLayer ? 1 : 0));
/* 177 */     dest.writeByte((byte)(this.showCropFrame ? 1 : 0));
/* 178 */     dest.writeByte((byte)(this.showCropGrid ? 1 : 0));
/* 179 */     dest.writeByte((byte)(this.hideBottomControls ? 1 : 0));
/* 180 */     dest.writeByte((byte)(this.rotateEnabled ? 1 : 0));
/* 181 */     dest.writeByte((byte)(this.scaleEnabled ? 1 : 0));
/* 182 */     dest.writeByte((byte)(this.previewEggs ? 1 : 0));
/* 183 */     dest.writeTypedList(this.selectionMedias);
/*     */   }
/*     */   
/*     */   public PictureSelectionConfig() {}
/*     */   
/*     */   protected PictureSelectionConfig(Parcel in)
/*     */   {
/* 190 */     this.mimeType = in.readInt();
/* 191 */     this.camera = (in.readByte() != 0);
/* 192 */     this.outputCameraPath = in.readString();
/* 193 */     this.themeStyleId = in.readInt();
/* 194 */     this.selectionMode = in.readInt();
/* 195 */     this.maxSelectNum = in.readInt();
/* 196 */     this.minSelectNum = in.readInt();
/* 197 */     this.videoQuality = in.readInt();
/* 198 */     this.cropCompressQuality = in.readInt();
/* 199 */     this.videoMaxSecond = in.readInt();
/* 200 */     this.videoMinSecond = in.readInt();
/* 201 */     this.recordVideoSecond = in.readInt();
/* 202 */     this.compressMaxkB = in.readInt();
/* 203 */     this.compressGrade = in.readInt();
/* 204 */     this.imageSpanCount = in.readInt();
/* 205 */     this.compressMode = in.readInt();
/* 206 */     this.compressWidth = in.readInt();
/* 207 */     this.compressHeight = in.readInt();
/* 208 */     this.overrideWidth = in.readInt();
/* 209 */     this.overrideHeight = in.readInt();
/* 210 */     this.aspect_ratio_x = in.readInt();
/* 211 */     this.aspect_ratio_y = in.readInt();
/* 212 */     this.sizeMultiplier = in.readFloat();
/* 213 */     this.cropWidth = in.readInt();
/* 214 */     this.cropHeight = in.readInt();
/* 215 */     this.zoomAnim = (in.readByte() != 0);
/* 216 */     this.isCompress = (in.readByte() != 0);
/* 217 */     this.isCamera = (in.readByte() != 0);
/* 218 */     this.isGif = (in.readByte() != 0);
/* 219 */     this.enablePreview = (in.readByte() != 0);
/* 220 */     this.enPreviewVideo = (in.readByte() != 0);
/* 221 */     this.enablePreviewAudio = (in.readByte() != 0);
/* 222 */     this.checkNumMode = (in.readByte() != 0);
/* 223 */     this.openClickSound = (in.readByte() != 0);
/* 224 */     this.enableCrop = (in.readByte() != 0);
/* 225 */     this.freeStyleCropEnabled = (in.readByte() != 0);
/* 226 */     this.circleDimmedLayer = (in.readByte() != 0);
/* 227 */     this.showCropFrame = (in.readByte() != 0);
/* 228 */     this.showCropGrid = (in.readByte() != 0);
/* 229 */     this.hideBottomControls = (in.readByte() != 0);
/* 230 */     this.rotateEnabled = (in.readByte() != 0);
/* 231 */     this.scaleEnabled = (in.readByte() != 0);
/* 232 */     this.previewEggs = (in.readByte() != 0);
/* 233 */     this.selectionMedias = in.createTypedArrayList(LocalMedia.CREATOR);
/*     */   }
/*     */   
/* 236 */   public static final Parcelable.Creator<PictureSelectionConfig> CREATOR = new Parcelable.Creator()
/*     */   {
/*     */     public PictureSelectionConfig createFromParcel(Parcel source) {
/* 239 */       return new PictureSelectionConfig(source);
/*     */     }
/*     */     
/*     */     public PictureSelectionConfig[] newArray(int size)
/*     */     {
/* 244 */       return new PictureSelectionConfig[size];
/*     */     }
/*     */   };
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\config\PictureSelectionConfig.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */