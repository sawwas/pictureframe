/*    */ package com.luck.picture.lib.rxbus2;
/*    */ 
/*    */ public class BusData
/*    */ {
/*    */   String id;
/*    */   String status;
/*    */   
/*    */   public BusData() {}
/*    */   
/*    */   public BusData(String id, String status)
/*    */   {
/* 12 */     this.id = id;
/* 13 */     this.status = status;
/*    */   }
/*    */   
/*    */   public String getId() {
/* 17 */     return this.id;
/*    */   }
/*    */   
/*    */   public void setId(String id) {
/* 21 */     this.id = id;
/*    */   }
/*    */   
/*    */   public String getStatus() {
/* 25 */     return this.status;
/*    */   }
/*    */   
/*    */   public void setStatus(String status) {
/* 29 */     this.status = status;
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\rxbus2\BusData.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */