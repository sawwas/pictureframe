package com.luck.picture.lib.compress;

import android.content.Context;
import android.text.TextUtils;
import com.luck.picture.lib.entity.LocalMedia;
import java.io.File;
import java.util.List;

public class CompressImageOptions
        implements CompressInterface
{
    private CompressImageUtil compressImageUtil;
    private List<LocalMedia> images;
    private CompressInterface.CompressListener listener;

    public static CompressInterface compress(Context context, CompressConfig config, List<LocalMedia> images, CompressInterface.CompressListener listener)
    {
        if (config.getLubanOptions() != null) {
            return new LuBanCompress(context, config, images, listener);
        }
        return new CompressImageOptions(context, config, images, listener);
    }

    private CompressImageOptions(Context context, CompressConfig config, List<LocalMedia> images, CompressInterface.CompressListener listener)
    {
        this.compressImageUtil = new CompressImageUtil(context, config);
        this.images = images;
        this.listener = listener;
    }

    public void compress()
    {
        if ((this.images == null) || (this.images.isEmpty())) {
            this.listener.onCompressError(this.images, " images is null");
        }
        for (LocalMedia image : this.images) {
            if (image == null)
            {
                this.listener.onCompressError(this.images, " There are pictures of compress  is null.");
                return;
            }
        }
        compress((LocalMedia)this.images.get(0));
    }

    private void compress(final LocalMedia image)
    {
        String path = "";
        if (image.isCut()) {
            path = image.getCutPath();
        } else {
            path = image.getPath();
        }
        if (TextUtils.isEmpty(path))
        {
            continueCompress(image, false, new String[0]);
            return;
        }
        File file = new File(path);
        if ((file == null) || (!file.exists()) || (!file.isFile()))
        {
            continueCompress(image, false, new String[0]);
            return;
        }
        this.compressImageUtil.compress(path, new CompressImageUtil.CompressListener()
        {
            public void onCompressSuccess(String imgPath)
            {
                image.setCompressPath(imgPath);
                CompressImageOptions.this.continueCompress(image, true, new String[0]);
            }

            public void onCompressFailed(String imgPath, String msg)
            {
                CompressImageOptions.this.continueCompress(image, false, new String[] { msg });
            }
        });
    }

    private void continueCompress(LocalMedia image, boolean preSuccess, String... message)
    {
        image.setCompressed(preSuccess);
        int index = this.images.indexOf(image);
        boolean isLast = index == this.images.size() - 1;
        if (isLast) {
            handleCompressCallBack(message);
        } else {
            compress((LocalMedia)this.images.get(index + 1));
        }
    }

    private void handleCompressCallBack(String... message)
    {
        if (message.length > 0)
        {
            this.listener.onCompressError(this.images, message[0]);
            return;
        }
        for (LocalMedia image : this.images) {
            if (!image.isCompressed())
            {
                this.listener.onCompressError(this.images, image.getCompressPath() + " is compress failures");
                return;
            }
        }
        this.listener.onCompressSuccess(this.images);
    }
}
