/*    */ package com.luck.picture.lib.compress;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class LubanOptions
/*    */   implements Serializable
/*    */ {
/*    */   private int maxSize;
/*    */   private int maxHeight;
/*    */   private int maxWidth;
/*    */   private int grade;
/*    */   
/*    */   public int getGrade()
/*    */   {
/* 25 */     if (this.grade == 0) {
/* 26 */       return 3;
/*    */     }
/* 28 */     return this.grade;
/*    */   }
/*    */   
/*    */   public void setGrade(int grade) {
/* 32 */     this.grade = grade;
/*    */   }
/*    */   
/*    */   public int getMaxSize() {
/* 36 */     return this.maxSize;
/*    */   }
/*    */   
/*    */   public void setMaxSize(int maxSize) {
/* 40 */     this.maxSize = maxSize;
/*    */   }
/*    */   
/*    */   public int getMaxHeight() {
/* 44 */     return this.maxHeight;
/*    */   }
/*    */   
/*    */   public void setMaxHeight(int maxHeight) {
/* 48 */     this.maxHeight = maxHeight;
/*    */   }
/*    */   
/*    */   public int getMaxWidth() {
/* 52 */     return this.maxWidth;
/*    */   }
/*    */   
/*    */   public void setMaxWidth(int maxWidth) {
/* 56 */     this.maxWidth = maxWidth;
/*    */   }
/*    */   
/*    */   public static class Builder {
/*    */     private LubanOptions options;
/*    */     
/*    */     public Builder() {
/* 63 */       this.options = new LubanOptions(null);
/*    */     }
/*    */     
/*    */     public Builder setMaxSize(int maxSize) {
/* 67 */       this.options.setMaxSize(maxSize);
/* 68 */       return this;
/*    */     }
/*    */     
/*    */     public Builder setGrade(int grade) {
/* 72 */       this.options.setGrade(grade);
/* 73 */       return this;
/*    */     }
/*    */     
/*    */     public Builder setMaxHeight(int maxHeight) {
/* 77 */       this.options.setMaxHeight(maxHeight);
/* 78 */       return this;
/*    */     }
/*    */     
/*    */     public Builder setMaxWidth(int maxWidth) {
/* 82 */       this.options.setMaxWidth(maxWidth);
/* 83 */       return this;
/*    */     }
/*    */     
/*    */     public LubanOptions create() {
/* 87 */       return this.options;
/*    */     }
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\compress\LubanOptions.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */