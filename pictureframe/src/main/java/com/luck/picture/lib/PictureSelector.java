/*     */ package com.luck.picture.lib;
/*     */ 
/*     */ import android.app.Activity;
/*     */ import android.content.Intent;
/*     */ import android.os.Bundle;
/*     */ import android.support.annotation.Nullable;
/*     */ import android.support.v4.app.Fragment;
/*     */ import com.luck.picture.lib.entity.LocalMedia;
/*     */ import com.luck.picture.lib.tools.DoubleUtils;
/*     */ import java.io.Serializable;
/*     */ import java.lang.ref.WeakReference;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class PictureSelector
/*     */ {
/*     */   private final WeakReference<Activity> mActivity;
/*     */   private final WeakReference<Fragment> mFragment;
/*     */   
/*     */   private PictureSelector(Activity activity)
/*     */   {
/*  33 */     this(activity, null);
/*     */   }
/*     */   
/*     */   private PictureSelector(Fragment fragment) {
/*  37 */     this(fragment.getActivity(), fragment);
/*     */   }
/*     */   
/*     */   private PictureSelector(Activity activity, Fragment fragment) {
/*  41 */     this.mActivity = new WeakReference(activity);
/*  42 */     this.mFragment = new WeakReference(fragment);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static PictureSelector create(Activity activity)
/*     */   {
/*  52 */     return new PictureSelector(activity);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static PictureSelector create(Fragment fragment)
/*     */   {
/*  62 */     return new PictureSelector(fragment);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel openGallery(int mimeType)
/*     */   {
/*  70 */     return new PictureSelectionModel(this, mimeType);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel openCamera(int mimeType)
/*     */   {
/*  78 */     return new PictureSelectionModel(this, mimeType, true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static List<LocalMedia> obtainMultipleResult(Intent data)
/*     */   {
/*  86 */     List<LocalMedia> result = new ArrayList();
/*  87 */     if (data != null) {
/*  88 */       result = (List)data.getSerializableExtra("extra_result_media");
/*  89 */       if (result == null) {
/*  90 */         result = new ArrayList();
/*     */       }
/*  92 */       return result;
/*     */     }
/*  94 */     return result;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Intent putIntentResult(List<LocalMedia> data)
/*     */   {
/* 102 */     return new Intent().putExtra("extra_result_media", (Serializable)data);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static List<LocalMedia> obtainSelectorList(Bundle bundle)
/*     */   {
/* 111 */     if (bundle != null)
/*     */     {
/* 113 */       List<LocalMedia> selectionMedias = (List)bundle.getSerializable("selectList");
/* 114 */       return selectionMedias;
/*     */     }
/* 116 */     List<LocalMedia> selectionMedias = new ArrayList();
/* 117 */     return selectionMedias;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void saveSelectorList(Bundle outState, List<LocalMedia> selectedImages)
/*     */   {
/* 125 */     outState.putSerializable("selectList", (Serializable)selectedImages);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void externalPicturePreview(int position, List<LocalMedia> medias)
/*     */   {
/* 135 */     if (!DoubleUtils.isFastDoubleClick()) {
/* 136 */       Intent intent = new Intent(getActivity(), PictureExternalPreviewActivity.class);
/* 137 */       intent.putExtra("previewSelectList", (Serializable)medias);
/* 138 */       intent.putExtra("position", position);
/* 139 */       getActivity().startActivity(intent);
/* 140 */       getActivity().overridePendingTransition(R.anim.a5, 0);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void externalPicturePreview(int position, String directory_path, List<LocalMedia> medias)
/*     */   {
/* 152 */     if (!DoubleUtils.isFastDoubleClick()) {
/* 153 */       Intent intent = new Intent(getActivity(), PictureExternalPreviewActivity.class);
/* 154 */       intent.putExtra("previewSelectList", (Serializable)medias);
/* 155 */       intent.putExtra("position", position);
/* 156 */       intent.putExtra("directory_path", directory_path);
/* 157 */       getActivity().startActivity(intent);
/* 158 */       getActivity().overridePendingTransition(R.anim.a5, 0);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void externalPictureVideo(String path)
/*     */   {
/* 168 */     if (!DoubleUtils.isFastDoubleClick()) {
/* 169 */       Intent intent = new Intent(getActivity(), PictureVideoPlayActivity.class);
/* 170 */       intent.putExtra("video_path", path);
/* 171 */       getActivity().startActivity(intent);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void externalPictureAudio(String path)
/*     */   {
/* 181 */     if (!DoubleUtils.isFastDoubleClick()) {
/* 182 */       Intent intent = new Intent(getActivity(), PicturePlayAudioActivity.class);
/* 183 */       intent.putExtra("audio_path", path);
/* 184 */       getActivity().startActivity(intent);
/* 185 */       getActivity().overridePendingTransition(R.anim.a5, 0);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   @Nullable
/*     */   Activity getActivity()
/*     */   {
/* 194 */     return (Activity)this.mActivity.get();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   @Nullable
/*     */   Fragment getFragment()
/*     */   {
/* 202 */     return this.mFragment != null ? (Fragment)this.mFragment.get() : null;
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\PictureSelector.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */