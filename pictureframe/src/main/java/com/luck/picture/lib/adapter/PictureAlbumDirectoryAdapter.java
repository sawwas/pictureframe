 package com.luck.picture.lib.adapter;

/*     */ import android.content.Context;
/*     */ import android.graphics.Bitmap;
/*     */ import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
/*     */ import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
/*     */ import android.support.v7.widget.RecyclerView;
 import android.support.v7.widget.RecyclerView.Adapter;
/*     */ import android.support.v7.widget.RecyclerView.ViewHolder;
/*     */ import android.view.LayoutInflater;
/*     */ import android.view.View;
/*     */ import android.view.View.OnClickListener;
/*     */ import android.view.ViewGroup;
/*     */ import android.widget.ImageView;
/*     */ import android.widget.TextView;
/*     */ import com.bumptech.glide.Glide;
/*     */ import com.bumptech.glide.RequestBuilder;
/*     */ import com.bumptech.glide.RequestManager;
/*     */ import com.bumptech.glide.load.engine.DiskCacheStrategy;
/*     */ import com.bumptech.glide.request.RequestOptions;
/*     */ import com.bumptech.glide.request.target.BitmapImageViewTarget;
/*     */ import com.luck.picture.lib.R;
 import com.luck.picture.lib.R.drawable;
/*     */ import com.luck.picture.lib.R.id;
/*     */ import com.luck.picture.lib.R.layout;
/*     */ import com.luck.picture.lib.config.PictureMimeType;
/*     */ import com.luck.picture.lib.entity.LocalMedia;
/*     */ import com.luck.picture.lib.entity.LocalMediaFolder;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ public class PictureAlbumDirectoryAdapter
/*     */   extends RecyclerView.Adapter<ViewHolder>
/*     */ {
/*     */   private Context mContext;
/*  35 */   private List<LocalMediaFolder> folders = new ArrayList();
/*     */   private int mimeType;
/*     */   private OnItemClickListener onItemClickListener;
/*     */   
/*     */   public PictureAlbumDirectoryAdapter(Context mContext) {
/*  40 */     this.mContext = mContext;
/*     */   }
/*     */   
/*     */   public void bindFolderData(List<LocalMediaFolder> folders) {
/*  44 */     this.folders = folders;
/*  45 */     notifyDataSetChanged();
/*     */   }
/*     */   
/*     */   public void setMimeType(int mimeType) {
/*  49 */     this.mimeType = mimeType;
/*     */   }
/*     */   
/*     */   public List<LocalMediaFolder> getFolderData() {
/*  53 */     if (this.folders == null) {
/*  54 */       this.folders = new ArrayList();
/*     */     }
/*  56 */     return this.folders;
/*     */   }
/*     */   
/*     */   public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
/*     */   {
/*  61 */     View itemView = LayoutInflater.from(this.mContext).inflate(R.layout.picture_album_folder_item, parent, false);
/*  62 */     return new ViewHolder(itemView);
/*     */   }
/*     */   
/*     */   public void onBindViewHolder(final ViewHolder holder, int position)
/*     */   {
/*  67 */     final LocalMediaFolder folder = (LocalMediaFolder)this.folders.get(position);
/*  68 */     String name = folder.getName();
/*  69 */     int imageNum = folder.getImageNum();
/*  70 */     String imagePath = folder.getFirstImagePath();
/*  71 */     boolean isChecked = folder.isChecked();
/*  72 */     int checkedNum = folder.getCheckedNum();
/*  73 */     holder.tv_sign.setVisibility(checkedNum > 0 ? 0 : 4);
/*  74 */     holder.itemView.setSelected(isChecked);
/*  75 */     if (this.mimeType == PictureMimeType.ofAudio()) {
/*  76 */       holder.first_image.setImageResource(R.drawable.audio_placeholder);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*  83 */       RequestOptions options = new RequestOptions().placeholder(R.drawable.ic_placeholder).centerCrop().sizeMultiplier(0.5F).diskCacheStrategy(DiskCacheStrategy.ALL).override(160, 160);
/*  84 */       Glide.with(holder.itemView.getContext())
/*  85 */         .asBitmap()
/*  86 */         .load(imagePath)
/*  87 */         .apply(options)
/*  88 */         .into(new BitmapImageViewTarget(holder.first_image)
/*     */         {
/*     */ 
/*     */           protected void setResource(Bitmap resource)
/*     */           {
/*     */ 
/*  93 */             RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(PictureAlbumDirectoryAdapter.this.mContext.getResources(), resource);
/*  94 */             circularBitmapDrawable.setCornerRadius(8.0F);
/*  95 */             holder.first_image.setImageDrawable(circularBitmapDrawable);
/*     */           }
/*     */         });
/*     */     }
/*  99 */     holder.image_num.setText("(" + imageNum + ")");
/* 100 */     holder.tv_folder_name.setText(name);
/* 101 */     holder.itemView.setOnClickListener(new View.OnClickListener()
/*     */     {
/*     */       public void onClick(View view) {
/* 104 */         if (PictureAlbumDirectoryAdapter.this.onItemClickListener != null) {
/* 105 */           for (LocalMediaFolder mediaFolder : PictureAlbumDirectoryAdapter.this.folders) {
/* 106 */             mediaFolder.setChecked(false);
/*     */           }
/* 108 */           folder.setChecked(true);
/* 109 */           PictureAlbumDirectoryAdapter.this.notifyDataSetChanged();
/* 110 */           PictureAlbumDirectoryAdapter.this.onItemClickListener.onItemClick(folder.getName(), folder.getImages());
/*     */         }
/*     */       }
/*     */     });
/*     */   }
/*     */   
/*     */ 
/*     */ 
/* 118 */   public int getItemCount() { return this.folders.size(); }
/*     */   
/*     */   public static abstract interface OnItemClickListener { public abstract void onItemClick(String paramString, List<LocalMedia> paramList);
/*     */   }
/*     */   
/*     */   class ViewHolder extends RecyclerView.ViewHolder { ImageView first_image;
/*     */     TextView tv_folder_name;
/*     */     
/* 126 */     public ViewHolder(View itemView) { super();
/* 127 */       this.first_image = ((ImageView)itemView.findViewById(R.id.first_image));
/* 128 */       this.tv_folder_name = ((TextView)itemView.findViewById(R.id.tv_folder_name));
/* 129 */       this.image_num = ((TextView)itemView.findViewById(R.id.image_num));
/* 130 */       this.tv_sign = ((TextView)itemView.findViewById(R.id.tv_sign));
/*     */     }
/*     */     
/*     */     TextView image_num;
/*     */     TextView tv_sign;
/*     */   }
/*     */   
/* 137 */   public void setOnItemClickListener(OnItemClickListener onItemClickListener) { this.onItemClickListener = onItemClickListener; }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\adapter\PictureAlbumDirectoryAdapter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */