package com.luck.picture.lib.tools;

public class DebugUtil
{
  public static final String TAG = "com.luck.picture.lib";
  public static final boolean DEBUG = false;
  
  public static void debug(String tag, String msg) {}
  
  public static void debug(String msg) {}
  
  public static void v(String msg) {}
  
  public static void v(String tag, String msg) {}
  
  public static void log(String msg) {}
  
  public static void log(String tag, String msg) {}
  
  public static void i(String tag, String msg) {}
  
  public static void i(String msg) {}
  
  public static void error(String tag, String error) {}
  
  public static void error(String error) {}
}


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\tools\DebugUtil.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */