/*    */ package com.luck.picture.lib.dialog;
/*    */ 
/*    */ import android.app.Dialog;
/*    */ import android.content.Context;
/*    */ import android.os.Bundle;
/*    */ import android.view.Window;
/*    */ import com.luck.picture.lib.R.style;
/*    */ 
/*    */ public class PictureDialog extends Dialog
/*    */ {
/*    */   public Context context;
/*    */   
/*    */   public PictureDialog(Context context)
/*    */   {
/* 15 */     super(context, R.style.picture_alert_dialog);
/* 16 */     this.context = context;
/* 17 */     setCancelable(true);
/* 18 */     setCanceledOnTouchOutside(false);
/* 19 */     Window window = getWindow();
/* 20 */     window.setWindowAnimations(R.style.DialogWindowStyle);
/*    */   }
/*    */   
/*    */   protected void onCreate(Bundle savedInstanceState)
/*    */   {
/* 25 */     super.onCreate(savedInstanceState);
/* 26 */     setContentView(com.luck.picture.lib.R.layout.picture_alert_dialog);
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\dialog\PictureDialog.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */