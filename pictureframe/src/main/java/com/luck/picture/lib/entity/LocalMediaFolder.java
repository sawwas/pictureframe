/*     */ package com.luck.picture.lib.entity;
/*     */ 
/*     */ import android.os.Parcel;
/*     */ import android.os.Parcelable;
/*     */ import android.os.Parcelable.Creator;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LocalMediaFolder
/*     */   implements Parcelable
/*     */ {
/*     */   private String name;
/*     */   private String path;
/*     */   private String firstImagePath;
/*     */   private int imageNum;
/*     */   private int checkedNum;
/*     */   private boolean isChecked;
/*  23 */   private List<LocalMedia> images = new ArrayList();
/*     */   
/*     */   public boolean isChecked()
/*     */   {
/*  27 */     return this.isChecked;
/*     */   }
/*     */   
/*     */   public void setChecked(boolean checked) {
/*  31 */     this.isChecked = checked;
/*     */   }
/*     */   
/*     */   public String getName() {
/*  35 */     return this.name;
/*     */   }
/*     */   
/*     */   public void setName(String name) {
/*  39 */     this.name = name;
/*     */   }
/*     */   
/*     */   public String getPath() {
/*  43 */     return this.path;
/*     */   }
/*     */   
/*     */   public void setPath(String path) {
/*  47 */     this.path = path;
/*     */   }
/*     */   
/*     */   public String getFirstImagePath() {
/*  51 */     return this.firstImagePath;
/*     */   }
/*     */   
/*     */   public void setFirstImagePath(String firstImagePath) {
/*  55 */     this.firstImagePath = firstImagePath;
/*     */   }
/*     */   
/*     */   public int getImageNum() {
/*  59 */     return this.imageNum;
/*     */   }
/*     */   
/*     */   public void setImageNum(int imageNum) {
/*  63 */     this.imageNum = imageNum;
/*     */   }
/*     */   
/*     */   public List<LocalMedia> getImages() {
/*  67 */     if (this.images == null) {
/*  68 */       this.images = new ArrayList();
/*     */     }
/*  70 */     return this.images;
/*     */   }
/*     */   
/*     */   public void setImages(List<LocalMedia> images) {
/*  74 */     this.images = images;
/*     */   }
/*     */   
/*     */   public int getCheckedNum() {
/*  78 */     return this.checkedNum;
/*     */   }
/*     */   
/*     */   public void setCheckedNum(int checkedNum) {
/*  82 */     this.checkedNum = checkedNum;
/*     */   }
/*     */   
/*     */   public int describeContents()
/*     */   {
/*  87 */     return 0;
/*     */   }
/*     */   
/*     */   public void writeToParcel(Parcel dest, int flags)
/*     */   {
/*  92 */     dest.writeString(this.name);
/*  93 */     dest.writeString(this.path);
/*  94 */     dest.writeString(this.firstImagePath);
/*  95 */     dest.writeInt(this.imageNum);
/*  96 */     dest.writeInt(this.checkedNum);
/*  97 */     dest.writeByte((byte)(this.isChecked ? 1 : 0));
/*  98 */     dest.writeTypedList(this.images);
/*     */   }
/*     */   
/*     */   public LocalMediaFolder() {}
/*     */   
/*     */   protected LocalMediaFolder(Parcel in)
/*     */   {
/* 105 */     this.name = in.readString();
/* 106 */     this.path = in.readString();
/* 107 */     this.firstImagePath = in.readString();
/* 108 */     this.imageNum = in.readInt();
/* 109 */     this.checkedNum = in.readInt();
/* 110 */     this.isChecked = (in.readByte() != 0);
/* 111 */     this.images = in.createTypedArrayList(LocalMedia.CREATOR);
/*     */   }
/*     */   
/* 114 */   public static final Parcelable.Creator<LocalMediaFolder> CREATOR = new Parcelable.Creator()
/*     */   {
/*     */     public LocalMediaFolder createFromParcel(Parcel source) {
/* 117 */       return new LocalMediaFolder(source);
/*     */     }
/*     */     
/*     */     public LocalMediaFolder[] newArray(int size)
/*     */     {
/* 122 */       return new LocalMediaFolder[size];
/*     */     }
/*     */   };
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\entity\LocalMediaFolder.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */