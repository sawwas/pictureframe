/*     */ package com.luck.picture.lib;
/*     */ 
/*     */ import android.content.Intent;
/*     */ import android.os.Bundle;
/*     */ import android.support.annotation.Nullable;
/*     */ import android.support.v4.content.ContextCompat;
/*     */ import android.support.v4.view.PagerAdapter;
/*     */ import android.support.v4.view.ViewPager.OnPageChangeListener;
/*     */ import android.text.TextUtils;
/*     */ import android.view.LayoutInflater;
/*     */ import android.view.View;
/*     */ import android.view.View.OnClickListener;
/*     */ import android.view.ViewGroup;
/*     */ import android.view.animation.Animation;
/*     */ import android.view.animation.Animation.AnimationListener;
/*     */ import android.widget.ImageView;
/*     */ import android.widget.LinearLayout;
/*     */ import android.widget.TextView;
/*     */ import com.bumptech.glide.Glide;
/*     */ import com.bumptech.glide.Priority;
/*     */ import com.bumptech.glide.RequestBuilder;
/*     */ import com.bumptech.glide.RequestManager;
/*     */ import com.bumptech.glide.load.engine.DiskCacheStrategy;
/*     */ import com.bumptech.glide.request.RequestOptions;
/*     */ import com.luck.picture.lib.anim.OptAnimationLoader;
/*     */ import com.luck.picture.lib.config.PictureMimeType;
/*     */ import com.luck.picture.lib.entity.EventEntity;
/*     */ import com.luck.picture.lib.entity.LocalMedia;
/*     */ import com.luck.picture.lib.observable.ImagesObservable;
/*     */ import com.luck.picture.lib.rxbus2.RxBus;
/*     */ import com.luck.picture.lib.rxbus2.Subscribe;
/*     */ import com.luck.picture.lib.rxbus2.ThreadMode;
/*     */ import com.luck.picture.lib.tools.AttrsUtils;
/*     */ import com.luck.picture.lib.tools.DebugUtil;
/*     */ import com.luck.picture.lib.tools.LightStatusBarUtils;
/*     */ import com.luck.picture.lib.tools.ScreenUtils;
/*     */ import com.luck.picture.lib.tools.ToolbarUtil;
/*     */ import com.luck.picture.lib.tools.VoiceUtils;
/*     */ import com.luck.picture.lib.widget.PreviewViewPager;
/*     */ import com.yalantis.ucrop.UCropMulti;
/*     */ import com.yalantis.ucrop.model.CutInfo;
/*     */ import java.io.Serializable;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import uk.co.senab.photoview.PhotoView;
/*     */ import uk.co.senab.photoview.PhotoViewAttacher.OnViewTapListener;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class PicturePreviewActivity
/*     */   extends PictureBaseActivity
/*     */   implements View.OnClickListener, Animation.AnimationListener
/*     */ {
/*     */   private ImageView picture_left_back;
/*     */   private TextView tv_img_num;
/*     */   private TextView tv_title;
/*     */   private TextView tv_ok;
/*     */   private PreviewViewPager viewPager;
/*     */   private LinearLayout id_ll_ok;
/*     */   private int position;
/*     */   private LinearLayout ll_check;
/*  63 */   private List<LocalMedia> images = new ArrayList();
/*  64 */   private List<LocalMedia> selectImages = new ArrayList();
/*     */   private TextView check;
/*     */   private SimpleFragmentAdapter adapter;
/*     */   private Animation animation;
/*     */   private boolean refresh;
/*     */   private int index;
/*     */   private int preview_complete_textColor;
/*     */   private int screenWidth;
/*     */   private LayoutInflater inflater;
/*     */   
/*     */   @Subscribe(threadMode=ThreadMode.MAIN)
/*     */   public void eventBus(EventEntity obj)
/*     */   {
/*  77 */     switch (obj.what)
/*     */     {
/*     */     case 2770: 
/*  80 */       dismissDialog();
/*  81 */       finish();
/*  82 */       overridePendingTransition(0, R.anim.a3);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   protected void onCreate(@Nullable Bundle savedInstanceState)
/*     */   {
/*  89 */     super.onCreate(savedInstanceState);
/*  90 */     setContentView(R.layout.picture_preview);
/*  91 */     if (!RxBus.getDefault().isRegistered(this)) {
/*  92 */       RxBus.getDefault().register(this);
/*     */     }
/*  94 */     this.inflater = LayoutInflater.from(this);
/*  95 */     this.screenWidth = ScreenUtils.getScreenWidth(this);
/*  96 */     int status_color = AttrsUtils.getTypeValueColor(this, R.attr.picture_status_color);
/*  97 */     ToolbarUtil.setColorNoTranslucent(this, status_color);
/*  98 */     this.preview_complete_textColor = AttrsUtils.getTypeValueColor(this, R.attr.picture_preview_textColor);
/*  99 */     LightStatusBarUtils.setLightStatusBar(this, this.previewStatusFont);
/* 100 */     this.animation = OptAnimationLoader.loadAnimation(this, R.anim.modal_in);
/* 101 */     this.animation.setAnimationListener(this);
/* 102 */     this.picture_left_back = ((ImageView)findViewById(R.id.picture_left_back));
/* 103 */     this.viewPager = ((PreviewViewPager)findViewById(R.id.preview_pager));
/* 104 */     this.ll_check = ((LinearLayout)findViewById(R.id.ll_check));
/* 105 */     this.id_ll_ok = ((LinearLayout)findViewById(R.id.id_ll_ok));
/* 106 */     this.check = ((TextView)findViewById(R.id.check));
/* 107 */     this.picture_left_back.setOnClickListener(this);
/* 108 */     this.tv_ok = ((TextView)findViewById(R.id.tv_ok));
/* 109 */     this.id_ll_ok.setOnClickListener(this);
/* 110 */     this.tv_img_num = ((TextView)findViewById(R.id.tv_img_num));
/* 111 */     this.tv_title = ((TextView)findViewById(R.id.picture_title));
/* 112 */     this.position = getIntent().getIntExtra("position", 0);
/* 113 */     this.tv_ok.setText(this.numComplete ? getString(R.string.picture_done_front_num, new Object[] { Integer.valueOf(0), Integer.valueOf(this.maxSelectNum) }) : 
/* 114 */       getString(R.string.picture_please_select));
/*     */     
/* 116 */     this.tv_img_num.setSelected(this.checkNumMode);
/*     */     
/*     */ 
/* 119 */     this.selectImages = ((List)getIntent().getSerializableExtra("selectList"));
/*     */     
/* 121 */     boolean is_bottom_preview = getIntent().getBooleanExtra("bottom_preview", false);
/* 122 */     if (is_bottom_preview)
/*     */     {
/*     */ 
/* 125 */       this.images = ((List)getIntent().getSerializableExtra("previewSelectList"));
/*     */     } else {
/* 127 */       this.images = ImagesObservable.getInstance().readLocalMedias();
/*     */     }
/* 129 */     initViewPageAdapterData();
/* 130 */     this.ll_check.setOnClickListener(new View.OnClickListener()
/*     */     {
/*     */       public void onClick(View view) {
/* 133 */         if ((PicturePreviewActivity.this.images != null) && (PicturePreviewActivity.this.images.size() > 0)) {
/* 134 */           LocalMedia image = (LocalMedia)PicturePreviewActivity.this.images.get(PicturePreviewActivity.this.viewPager.getCurrentItem());
/*     */           
/* 136 */           String pictureType = PicturePreviewActivity.this.selectImages.size() > 0 ? ((LocalMedia)PicturePreviewActivity.this.selectImages.get(0)).getPictureType() : "";
/* 137 */           if (!TextUtils.isEmpty(pictureType))
/*     */           {
/* 139 */             boolean toEqual = PictureMimeType.mimeToEqual(pictureType, image.getPictureType());
/* 140 */             if (!toEqual) {
/* 141 */               PicturePreviewActivity.this.showToast(PicturePreviewActivity.this.getString(R.string.picture_rule)); return;
/*     */             }
/*     */           }
/*     */           
/*     */           boolean isChecked;
/*     */           
/* 147 */           if (!PicturePreviewActivity.this.check.isSelected()) {
/* 148 */             boolean isChecked = true;
/* 149 */             PicturePreviewActivity.this.check.setSelected(true);
/* 150 */             PicturePreviewActivity.this.check.startAnimation(PicturePreviewActivity.this.animation);
/*     */           } else {
/* 152 */             isChecked = false;
/* 153 */             PicturePreviewActivity.this.check.setSelected(false);
/*     */           }
/* 155 */           if ((PicturePreviewActivity.this.selectImages.size() >= PicturePreviewActivity.this.maxSelectNum) && (isChecked)) {
/* 156 */             PicturePreviewActivity.this.showToast(PicturePreviewActivity.this.getString(R.string.picture_message_max_num, new Object[] { Integer.valueOf(PicturePreviewActivity.this.maxSelectNum) }));
/* 157 */             PicturePreviewActivity.this.check.setSelected(false);
/* 158 */             return;
/*     */           }
/* 160 */           if (isChecked) {
/* 161 */             VoiceUtils.playVoice(PicturePreviewActivity.this.mContext, PicturePreviewActivity.this.openClickSound);
/* 162 */             PicturePreviewActivity.this.selectImages.add(image);
/* 163 */             image.setNum(PicturePreviewActivity.this.selectImages.size());
/* 164 */             if (PicturePreviewActivity.this.checkNumMode) {
/* 165 */               PicturePreviewActivity.this.check.setText(image.getNum() + "");
/*     */             }
/*     */           } else {
/* 168 */             for (LocalMedia media : PicturePreviewActivity.this.selectImages) {
/* 169 */               if (media.getPath().equals(image.getPath())) {
/* 170 */                 PicturePreviewActivity.this.selectImages.remove(media);
/* 171 */                 PicturePreviewActivity.this.subSelectPosition();
/* 172 */                 PicturePreviewActivity.this.notifyCheckChanged(media);
/* 173 */                 break;
/*     */               }
/*     */             }
/*     */           }
/* 177 */           PicturePreviewActivity.this.onSelectNumChange(true);
/*     */         }
/*     */       }
/* 180 */     });
/* 181 */     this.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
/*     */     {
/*     */       public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
/* 184 */         PicturePreviewActivity.this.isPreviewEggs(PicturePreviewActivity.this.previewEggs, position, positionOffsetPixels);
/*     */       }
/*     */       
/*     */       public void onPageSelected(int i)
/*     */       {
/* 189 */         PicturePreviewActivity.this.position = i;
/* 190 */         PicturePreviewActivity.this.tv_title.setText(PicturePreviewActivity.this.position + 1 + "/" + PicturePreviewActivity.this.images.size());
/* 191 */         LocalMedia media = (LocalMedia)PicturePreviewActivity.this.images.get(PicturePreviewActivity.this.position);
/* 192 */         PicturePreviewActivity.this.index = media.getPosition();
/* 193 */         if (!PicturePreviewActivity.this.previewEggs) {
/* 194 */           if (PicturePreviewActivity.this.checkNumMode) {
/* 195 */             PicturePreviewActivity.this.check.setText(media.getNum() + "");
/* 196 */             PicturePreviewActivity.this.notifyCheckChanged(media);
/*     */           }
/* 198 */           PicturePreviewActivity.this.onImageChecked(PicturePreviewActivity.this.position);
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       public void onPageScrollStateChanged(int state) {}
/*     */     });
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void isPreviewEggs(boolean previewEggs, int position, int positionOffsetPixels)
/*     */   {
/* 215 */     if ((previewEggs) && 
/* 216 */       (this.images.size() > 0) && (this.images != null))
/*     */     {
/*     */ 
/* 219 */       if (positionOffsetPixels < this.screenWidth / 2) {
/* 220 */         LocalMedia media = (LocalMedia)this.images.get(position);
/* 221 */         this.check.setSelected(isSelected(media));
/* 222 */         if (this.checkNumMode) {
/* 223 */           int num = media.getNum();
/* 224 */           this.check.setText(num + "");
/* 225 */           notifyCheckChanged(media);
/* 226 */           onImageChecked(position);
/*     */         }
/*     */       } else {
/* 229 */         LocalMedia media = (LocalMedia)this.images.get(position + 1);
/* 230 */         this.check.setSelected(isSelected(media));
/* 231 */         if (this.checkNumMode) {
/* 232 */           int num = media.getNum();
/* 233 */           this.check.setText(num + "");
/* 234 */           notifyCheckChanged(media);
/* 235 */           onImageChecked(position + 1);
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void initViewPageAdapterData()
/*     */   {
/* 243 */     this.tv_title.setText(this.position + 1 + "/" + this.images.size());
/* 244 */     this.adapter = new SimpleFragmentAdapter();
/* 245 */     this.viewPager.setAdapter(this.adapter);
/* 246 */     this.viewPager.setCurrentItem(this.position);
/* 247 */     onSelectNumChange(false);
/* 248 */     onImageChecked(this.position);
/* 249 */     if (this.images.size() > 0) {
/* 250 */       LocalMedia media = (LocalMedia)this.images.get(this.position);
/* 251 */       this.index = media.getPosition();
/* 252 */       if (this.checkNumMode) {
/* 253 */         this.tv_img_num.setSelected(true);
/* 254 */         this.check.setText(media.getNum() + "");
/* 255 */         notifyCheckChanged(media);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void notifyCheckChanged(LocalMedia imageBean)
/*     */   {
/* 264 */     if (this.checkNumMode) {
/* 265 */       this.check.setText("");
/* 266 */       for (LocalMedia media : this.selectImages) {
/* 267 */         if (media.getPath().equals(imageBean.getPath())) {
/* 268 */           imageBean.setNum(media.getNum());
/* 269 */           this.check.setText(String.valueOf(imageBean.getNum()));
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void subSelectPosition()
/*     */   {
/* 279 */     int index = 0; for (int len = this.selectImages.size(); index < len; index++) {
/* 280 */       LocalMedia media = (LocalMedia)this.selectImages.get(index);
/* 281 */       media.setNum(index + 1);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onImageChecked(int position)
/*     */   {
/* 291 */     if ((this.images != null) && (this.images.size() > 0)) {
/* 292 */       LocalMedia media = (LocalMedia)this.images.get(position);
/* 293 */       this.check.setSelected(isSelected(media));
/*     */     } else {
/* 295 */       this.check.setSelected(false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isSelected(LocalMedia image)
/*     */   {
/* 306 */     for (LocalMedia media : this.selectImages) {
/* 307 */       if (media.getPath().equals(image.getPath())) {
/* 308 */         return true;
/*     */       }
/*     */     }
/* 311 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onSelectNumChange(boolean isRefresh)
/*     */   {
/* 319 */     this.refresh = isRefresh;
/* 320 */     boolean enable = this.selectImages.size() != 0;
/* 321 */     if (enable) {
/* 322 */       this.tv_ok.setTextColor(this.preview_complete_textColor);
/* 323 */       this.id_ll_ok.setEnabled(true);
/* 324 */       if (this.numComplete) {
/* 325 */         this.tv_ok.setText(getString(R.string.picture_done_front_num, new Object[] { Integer.valueOf(this.selectImages.size()), Integer.valueOf(this.maxSelectNum) }));
/*     */       } else {
/* 327 */         if (this.refresh) {
/* 328 */           this.tv_img_num.startAnimation(this.animation);
/*     */         }
/* 330 */         this.tv_img_num.setVisibility(0);
/* 331 */         this.tv_img_num.setText(this.selectImages.size() + "");
/* 332 */         this.tv_ok.setText(getString(R.string.picture_completed));
/*     */       }
/*     */     } else {
/* 335 */       this.id_ll_ok.setEnabled(false);
/* 336 */       this.tv_ok.setTextColor(ContextCompat.getColor(this, R.color.tab_color_false));
/* 337 */       if (this.numComplete) {
/* 338 */         this.tv_ok.setText(getString(R.string.picture_done_front_num, new Object[] { Integer.valueOf(0), Integer.valueOf(this.maxSelectNum) }));
/*     */       } else {
/* 340 */         this.tv_img_num.setVisibility(4);
/* 341 */         this.tv_ok.setText(getString(R.string.picture_please_select));
/*     */       }
/*     */     }
/* 344 */     updateSelector(this.refresh);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void updateSelector(boolean isRefresh)
/*     */   {
/* 353 */     if (isRefresh) {
/* 354 */       EventEntity obj = new EventEntity(2774, this.selectImages, this.index);
/* 355 */       RxBus.getDefault().post(obj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void onAnimationStart(Animation animation) {}
/*     */   
/*     */ 
/*     */   public void onAnimationEnd(Animation animation)
/*     */   {
/* 365 */     updateSelector(this.refresh);
/*     */   }
/*     */   
/*     */   public void onAnimationRepeat(Animation animation) {}
/*     */   
/*     */   public class SimpleFragmentAdapter
/*     */     extends PagerAdapter
/*     */   {
/*     */     public SimpleFragmentAdapter() {}
/*     */     
/*     */     public int getCount()
/*     */     {
/* 377 */       return PicturePreviewActivity.this.images.size();
/*     */     }
/*     */     
/*     */     public void destroyItem(ViewGroup container, int position, Object object)
/*     */     {
/* 382 */       container.removeView((View)object);
/*     */     }
/*     */     
/*     */     public boolean isViewFromObject(View view, Object object)
/*     */     {
/* 387 */       return view == object;
/*     */     }
/*     */     
/*     */     public Object instantiateItem(ViewGroup container, int position)
/*     */     {
/* 392 */       View contentView = PicturePreviewActivity.this.inflater.inflate(R.layout.picture_image_preview, container, false);
/* 393 */       PhotoView imageView = (PhotoView)contentView.findViewById(R.id.preview_image);
/* 394 */       ImageView iv_play = (ImageView)contentView.findViewById(R.id.iv_play);
/* 395 */       LocalMedia media = (LocalMedia)PicturePreviewActivity.this.images.get(position);
/* 396 */       if (media != null) {
/* 397 */         String pictureType = media.getPictureType();
/* 398 */         boolean eqVideo = pictureType.startsWith("video");
/* 399 */         iv_play.setVisibility(eqVideo ? 0 : 8);
/*     */         String path;
/* 401 */         final String path; if ((media.isCut()) && (!media.isCompressed()))
/*     */         {
/* 403 */           path = media.getCutPath(); } else { String path;
/* 404 */           if ((media.isCompressed()) || ((media.isCut()) && (media.isCompressed())))
/*     */           {
/* 406 */             path = media.getCompressPath();
/*     */           } else
/* 408 */             path = media.getPath();
/*     */         }
/* 410 */         boolean isGif = PictureMimeType.isGif(pictureType);
/*     */         
/* 412 */         if ((isGif) && (!media.isCompressed()))
/*     */         {
/*     */ 
/*     */ 
/* 416 */           RequestOptions gifOptions = new RequestOptions().override(480, 800).priority(Priority.HIGH).diskCacheStrategy(DiskCacheStrategy.NONE);
/* 417 */           Glide.with(PicturePreviewActivity.this)
/* 418 */             .asGif()
/* 419 */             .load(path)
/* 420 */             .apply(gifOptions)
/* 421 */             .into(imageView);
/*     */         }
/*     */         else
/*     */         {
/* 425 */           RequestOptions options = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).override(480, 800);
/* 426 */           Glide.with(PicturePreviewActivity.this)
/* 427 */             .asBitmap()
/* 428 */             .load(path)
/* 429 */             .apply(options)
/* 430 */             .into(imageView);
/*     */         }
/* 432 */         imageView.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener()
/*     */         {
/*     */           public void onViewTap(View view, float x, float y) {
/* 435 */             PicturePreviewActivity.this.finish();
/* 436 */             PicturePreviewActivity.this.overridePendingTransition(0, R.anim.a3);
/*     */           }
/* 438 */         });
/* 439 */         iv_play.setOnClickListener(new View.OnClickListener()
/*     */         {
/*     */           public void onClick(View v) {
/* 442 */             Bundle bundle = new Bundle();
/* 443 */             bundle.putString("video_path", path);
/* 444 */             PicturePreviewActivity.this.startActivity(PictureVideoPlayActivity.class, bundle);
/*     */           }
/*     */         });
/*     */       }
/* 448 */       container.addView(contentView, 0);
/* 449 */       return contentView;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void onClick(View view)
/*     */   {
/* 456 */     int id = view.getId();
/* 457 */     if (id == R.id.picture_left_back) {
/* 458 */       finish();
/* 459 */       overridePendingTransition(0, R.anim.a3);
/*     */     }
/* 461 */     if (id == R.id.id_ll_ok)
/*     */     {
/* 463 */       int size = this.selectImages.size();
/* 464 */       String pictureType = this.selectImages.size() > 0 ? ((LocalMedia)this.selectImages.get(0)).getPictureType() : "";
/* 465 */       String str; if ((this.minSelectNum > 0) && 
/* 466 */         (size < this.minSelectNum) && (this.selectionMode == 2)) {
/* 467 */         boolean eqImg = pictureType.startsWith("image");
/*     */         
/* 469 */         str = eqImg ? getString(R.string.picture_min_img_num, new Object[] { Integer.valueOf(this.minSelectNum) }) : getString(R.string.picture_min_video_num, new Object[] {Integer.valueOf(this.minSelectNum) });
/* 470 */         showToast(str);
/* 471 */         return;
/*     */       }
/*     */       
/* 474 */       if ((this.enableCrop) && (pictureType.startsWith("image")) && (this.selectionMode == 2))
/*     */       {
/*     */ 
/* 477 */         ArrayList<String> cuts = new ArrayList();
/* 478 */         for (LocalMedia media : this.selectImages) {
/* 479 */           cuts.add(media.getPath());
/*     */         }
/* 481 */         startCrop(cuts);
/*     */       } else {
/* 483 */         onResult(this.selectImages);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void onResult(List<LocalMedia> images) {
/* 489 */     RxBus.getDefault().post(new EventEntity(2771, images));
/*     */     
/* 491 */     if (!this.isCompress) {
/* 492 */       DebugUtil.i("**** not compress finish");
/* 493 */       finish();
/* 494 */       overridePendingTransition(0, R.anim.a3);
/*     */     } else {
/* 496 */       DebugUtil.i("**** loading compress");
/* 497 */       showPleaseDialog();
/*     */     }
/*     */   }
/*     */   
/*     */   protected void onActivityResult(int requestCode, int resultCode, Intent data)
/*     */   {
/* 503 */     if (resultCode == -1) {
/* 504 */       switch (requestCode) {
/*     */       case 609: 
/* 506 */         List<CutInfo> list = UCropMulti.getOutput(data);
/* 507 */         setResult(-1, new Intent().putExtra("com.yalantis.ucrop.OutputUriList", (Serializable)list));
/*     */         
/* 509 */         finish();
/*     */       }
/*     */     }
/* 512 */     else if (resultCode == 96) {
/* 513 */       Throwable throwable = (Throwable)data.getSerializableExtra("com.yalantis.ucrop.Error");
/* 514 */       showToast(throwable.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void onBackPressed()
/*     */   {
/* 521 */     super.onBackPressed();
/* 522 */     finish();
/* 523 */     overridePendingTransition(0, R.anim.a3);
/*     */   }
/*     */   
/*     */   protected void onDestroy()
/*     */   {
/* 528 */     super.onDestroy();
/* 529 */     if (RxBus.getDefault().isRegistered(this)) {
/* 530 */       RxBus.getDefault().unregister(this);
/*     */     }
/* 532 */     if (this.animation != null) {
/* 533 */       this.animation.cancel();
/* 534 */       this.animation = null;
/*     */     }
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\PicturePreviewActivity.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */