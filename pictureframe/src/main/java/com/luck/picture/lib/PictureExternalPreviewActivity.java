package com.luck.picture.lib;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.dialog.CustomDialog;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.permissions.RxPermissions;
import com.luck.picture.lib.tools.DebugUtil;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.luck.picture.lib.tools.ScreenUtils;
import com.luck.picture.lib.widget.PreviewViewPager;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class PictureExternalPreviewActivity
        extends PictureBaseActivity
        implements View.OnClickListener {
    private ImageButton left_back;
    private TextView tv_title;
    private PreviewViewPager viewPager;
    private List<LocalMedia> images = new ArrayList();
    private int position = 0;
    private String directory_path;
    private SimpleFragmentAdapter adapter;
    private LayoutInflater inflater;
    private RxPermissions rxPermissions;
    private loadDataThread loadDataThread;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.picture_activity_external_preview);
        this.inflater = LayoutInflater.from(this);
        this.tv_title = ((TextView) findViewById(R.id.picture_title));
        this.left_back = ((ImageButton) findViewById(R.id.left_back));
        this.viewPager = ((PreviewViewPager) findViewById(R.id.preview_pager));
        this.position = getIntent().getIntExtra("position", 0);
        this.directory_path = getIntent().getStringExtra("directory_path");
        this.images = ((List) getIntent().getSerializableExtra("previewSelectList"));
        this.left_back.setOnClickListener(this);
        initViewPageAdapterData();
    }

    private void initViewPageAdapterData() {
        this.tv_title.setText(this.position + 1 + "/" + this.images.size());
        this.adapter = new SimpleFragmentAdapter();
        this.viewPager.setAdapter(this.adapter);
        this.viewPager.setCurrentItem(this.position);
        this.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                PictureExternalPreviewActivity.this.tv_title.setText(position + 1 + "/" + PictureExternalPreviewActivity.this.images.size());
            }

            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    public void onClick(View v) {
        finish();
        overridePendingTransition(0, R.anim.a3);
    }

    public class SimpleFragmentAdapter
            extends PagerAdapter {
        public SimpleFragmentAdapter() {
        }

        public int getCount() {
            return PictureExternalPreviewActivity.this.images.size();
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public Object instantiateItem(ViewGroup container, int position) {
            View contentView = PictureExternalPreviewActivity.this.inflater.inflate(R.layout.picture_image_preview, container, false);
            PhotoView imageView = (PhotoView) contentView.findViewById(R.id.preview_image);
            LocalMedia media = (LocalMedia) PictureExternalPreviewActivity.this.images.get(position);
            if (media != null) {
                String pictureType = media.getPictureType();
                final String path;
                if ((media.isCut()) && (!media.isCompressed())) {
                    path = media.getCutPath();
                } else {
                    if ((media.isCompressed()) || ((media.isCut()) && (media.isCompressed()))) {
                        path = media.getCompressPath();
                    } else {
                        path = media.getPath();
                    }
                }
                boolean isHttp = PictureMimeType.isHttp(path);
                if (isHttp) {
                    PictureExternalPreviewActivity.this.showPleaseDialog();
                }
                boolean isGif = PictureMimeType.isGif(pictureType);
                if ((isGif) && (!media.isCompressed())) {
                    RequestOptions gifOptions = new RequestOptions().override(480, 800).priority(Priority.HIGH).diskCacheStrategy(DiskCacheStrategy.NONE);
                    Glide.with(PictureExternalPreviewActivity.this)
                            .asGif()
                            .apply(gifOptions)
                            .load(path)
                            .listener(new RequestListener() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                                    PictureExternalPreviewActivity.this.dismissDialog();
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                                    PictureExternalPreviewActivity.this.dismissDialog();
                                    return false;


                                }
                            })

                            .into(imageView);
                } else {
                    RequestOptions options = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).override(480, 800);
                    Glide.with(PictureExternalPreviewActivity.this)
                            .load(path)
                            .apply(options)
                            .listener(new RequestListener() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                                    PictureExternalPreviewActivity.this.dismissDialog();
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                                    PictureExternalPreviewActivity.this.dismissDialog();
                                    return false;
                                }


                            })

                            .into(imageView);
                }
                imageView.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {
                    public void onViewTap(View view, float x, float y) {
                        PictureExternalPreviewActivity.this.finish();
                        PictureExternalPreviewActivity.this.overridePendingTransition(0, R.anim.a3);
                    }
                });
                imageView.setOnLongClickListener(new View.OnLongClickListener() {
                    public boolean onLongClick(View v) {
                        if (PictureExternalPreviewActivity.this.rxPermissions == null) {
                            PictureExternalPreviewActivity.this.rxPermissions = new RxPermissions(PictureExternalPreviewActivity.this);
                        }
                        PictureExternalPreviewActivity.this.rxPermissions.request(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}).subscribe(new Observer() {
                            public void onSubscribe(Disposable d) {
                            }

                            @Override
                            public void onNext(Object o) {
                                if (o != null) {
                                    PictureExternalPreviewActivity.this.showDownLoadDialog(path);
                                } else {
                                    PictureExternalPreviewActivity.this.showToast(PictureExternalPreviewActivity.this.getString(R.string.picture_jurisdiction));
                                }
                            }


                            public void onError(Throwable e) {
                            }

                            public void onComplete() {
                            }
                        });
                        return true;
                    }
                });
            }
            container.addView(contentView, 0);
            return contentView;
        }
    }

    private void showDownLoadDialog(final String path) {
        final CustomDialog dialog = new CustomDialog(this, ScreenUtils.getScreenWidth(this) * 3 / 4, ScreenUtils.getScreenHeight(this) / 4, R.layout.picture_wind_base_dialog_xml, R.style.Theme_dialog);

        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        Button btn_commit = (Button) dialog.findViewById(R.id.btn_commit);
        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        TextView tv_content = (TextView) dialog.findViewById(R.id.tv_content);
        tv_title.setText(getString(R.string.picture_prompt));
        tv_content.setText(getString(R.string.picture_prompt_content));
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btn_commit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PictureExternalPreviewActivity.this.showPleaseDialog();
                boolean isHttp = PictureMimeType.isHttp(path);
                if (isHttp) {
                    PictureExternalPreviewActivity.this.loadDataThread = new PictureExternalPreviewActivity.loadDataThread(path);
                    PictureExternalPreviewActivity.this.loadDataThread.start();
                } else {
                    try {
                        String dirPath = PictureFileUtils.createDir(PictureExternalPreviewActivity.this,
                                System.currentTimeMillis() + ".png", PictureExternalPreviewActivity.this.directory_path);
                        PictureFileUtils.copyFile(path, dirPath);
                        PictureExternalPreviewActivity.this.showToast(PictureExternalPreviewActivity.this.getString(R.string.picture_save_success) + "\n" + dirPath);
                        PictureExternalPreviewActivity.this.dismissDialog();
                    } catch (IOException e) {
                        PictureExternalPreviewActivity.this.showToast(PictureExternalPreviewActivity.this.getString(R.string.picture_save_error) + "\n" + e.getMessage());
                        PictureExternalPreviewActivity.this.dismissDialog();
                        e.printStackTrace();
                    }
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public class loadDataThread
            extends Thread {
        private String path;

        public loadDataThread(String path) {
            this.path = path;
        }

        public void run() {
            try {
                PictureExternalPreviewActivity.this.showLoadingImage(this.path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void showLoadingImage(String urlPath) {
        try {
            URL u = new URL(urlPath);
            String path = PictureFileUtils.createDir(this,
                    System.currentTimeMillis() + ".png", this.directory_path);
            byte[] buffer = new byte['?'];

            int ava = 0;
            long start = System.currentTimeMillis();

            BufferedInputStream bin = new BufferedInputStream(u.openStream());
            BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(path));
            int read;
            while ((read = bin.read(buffer)) > -1) {
                bout.write(buffer, 0, read);
                ava += read;
                long speed = ava / (System.currentTimeMillis() - start);
                DebugUtil.i("Download: " + ava + " byte(s)" + "    avg speed: " + speed + "  (kb/s)");
            }
            bout.flush();
            bout.close();
            Message message = this.handler.obtainMessage();
            message.what = 200;
            message.obj = path;
            this.handler.sendMessage(message);
        } catch (IOException e) {
            showToast(getString(R.string.picture_save_error) + "\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 200:
                    String path = (String) msg.obj;
                    PictureExternalPreviewActivity.this.showToast(PictureExternalPreviewActivity.this.getString(R.string.picture_save_success) + "\n" + path);
                    PictureExternalPreviewActivity.this.dismissDialog();
            }
        }
    };

    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0, R.anim.a3);
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.loadDataThread != null) {
            this.handler.removeCallbacks(this.loadDataThread);
            this.loadDataThread = null;
        }
    }
}
