/*    */ package com.luck.picture.lib.rxbus2;
/*    */ 
/*    */ import java.lang.reflect.InvocationTargetException;
/*    */ import java.lang.reflect.Method;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SubscriberMethod
/*    */ {
/*    */   public Method method;
/*    */   public ThreadMode threadMode;
/*    */   public Class<?> eventType;
/*    */   public Object subscriber;
/*    */   public int code;
/*    */   
/*    */   public SubscriberMethod(Object subscriber, Method method, Class<?> eventType, int code, ThreadMode threadMode)
/*    */   {
/* 18 */     this.method = method;
/* 19 */     this.threadMode = threadMode;
/* 20 */     this.eventType = eventType;
/* 21 */     this.subscriber = subscriber;
/* 22 */     this.code = code;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void invoke(Object o)
/*    */   {
/*    */     try
/*    */     {
/* 32 */       Class[] parameterType = this.method.getParameterTypes();
/* 33 */       if ((parameterType != null) && (parameterType.length == 1)) {
/* 34 */         this.method.invoke(this.subscriber, new Object[] { o });
/* 35 */       } else if ((parameterType == null) || (parameterType.length == 0)) {
/* 36 */         this.method.invoke(this.subscriber, new Object[0]);
/*    */       }
/*    */     } catch (IllegalAccessException e) {
/* 39 */       e.printStackTrace();
/*    */     } catch (InvocationTargetException e) {
/* 41 */       e.printStackTrace();
/*    */     }
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\rxbus2\SubscriberMethod.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */