package com.luck.picture.lib.compress;

import java.io.File;
import java.util.List;

public abstract interface OnMultiCompressListener
{
  public abstract void onStart();
  
  public abstract void onSuccess(List<File> paramList);
  
  public abstract void onError(Throwable paramThrowable);
}


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\compress\OnMultiCompressListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */