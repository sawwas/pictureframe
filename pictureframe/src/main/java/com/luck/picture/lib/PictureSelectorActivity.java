/*      */ package com.luck.picture.lib;
/*      */ 
/*      */ import android.content.DialogInterface;
/*      */ import android.content.DialogInterface.OnDismissListener;
/*      */ import android.content.Intent;
/*      */ import android.media.MediaPlayer;
/*      */ import android.net.Uri;
/*      */ import android.os.Build;
import android.os.Build.VERSION;
/*      */ import android.os.Bundle;
/*      */ import android.os.Handler;
/*      */ import android.support.v4.content.ContextCompat;
/*      */ import android.support.v4.content.FileProvider;
/*      */ import android.support.v7.widget.GridLayoutManager;
/*      */ import android.support.v7.widget.RecyclerView;
/*      */ import android.support.v7.widget.SimpleItemAnimator;
/*      */ import android.view.View;
/*      */ import android.view.View.OnClickListener;
/*      */ import android.view.Window;
/*      */ import android.view.animation.Animation;
/*      */ import android.view.animation.AnimationUtils;
/*      */ import android.widget.ImageView;
/*      */ import android.widget.LinearLayout;
/*      */ import android.widget.RelativeLayout;
/*      */ import android.widget.SeekBar;
/*      */ import android.widget.SeekBar.OnSeekBarChangeListener;
/*      */ import android.widget.TextView;
/*      */ import com.luck.picture.lib.adapter.PictureAlbumDirectoryAdapter;
import com.luck.picture.lib.adapter.PictureAlbumDirectoryAdapter.OnItemClickListener;
/*      */ import com.luck.picture.lib.adapter.PictureImageGridAdapter;
/*      */ import com.luck.picture.lib.adapter.PictureImageGridAdapter.OnPhotoSelectChangedListener;
/*      */ import com.luck.picture.lib.config.PictureMimeType;
/*      */ import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
/*      */ import com.luck.picture.lib.dialog.CustomDialog;
/*      */ import com.luck.picture.lib.entity.EventEntity;
/*      */ import com.luck.picture.lib.entity.LocalMedia;
/*      */ import com.luck.picture.lib.entity.LocalMediaFolder;
/*      */ import com.luck.picture.lib.model.LocalMediaLoader;
/*      */ import com.luck.picture.lib.model.LocalMediaLoader.LocalMediaLoadListener;
/*      */ import com.luck.picture.lib.observable.ImagesObservable;
/*      */ import com.luck.picture.lib.permissions.RxPermissions;
/*      */ import com.luck.picture.lib.rxbus2.RxBus;
/*      */ import com.luck.picture.lib.rxbus2.Subscribe;
/*      */ import com.luck.picture.lib.rxbus2.ThreadMode;
/*      */ import com.luck.picture.lib.tools.AttrsUtils;
/*      */ import com.luck.picture.lib.tools.DateUtils;
/*      */ import com.luck.picture.lib.tools.DebugUtil;
/*      */ import com.luck.picture.lib.tools.DoubleUtils;
/*      */ import com.luck.picture.lib.tools.LightStatusBarUtils;
/*      */ import com.luck.picture.lib.tools.PictureFileUtils;
/*      */ import com.luck.picture.lib.tools.ScreenUtils;
/*      */ import com.luck.picture.lib.tools.StringUtils;
/*      */ import com.luck.picture.lib.widget.FolderPopWindow;
/*      */ import com.luck.picture.lib.widget.PhotoPopupWindow;
/*      */ import com.luck.picture.lib.widget.PhotoPopupWindow.OnItemClickListener;
/*      */ import com.yalantis.ucrop.UCrop;
/*      */ import com.yalantis.ucrop.UCropMulti;
/*      */ import com.yalantis.ucrop.model.CutInfo;
/*      */ import io.reactivex.Observable;
/*      */ import io.reactivex.Observer;
/*      */ import io.reactivex.disposables.Disposable;
/*      */ import java.io.File;
/*      */ import java.io.Serializable;
/*      */ import java.util.ArrayList;
/*      */ import java.util.List;
/*      */ 
/*      */ public class PictureSelectorActivity extends PictureBaseActivity implements View.OnClickListener, PictureAlbumDirectoryAdapter.OnItemClickListener, PictureImageGridAdapter.OnPhotoSelectChangedListener, PhotoPopupWindow.OnItemClickListener
/*      */ {
/*   67 */   private static final String TAG = PictureSelectorActivity.class.getSimpleName();
/*      */   private ImageView picture_left_back;
/*      */   private TextView picture_title;
/*      */   private TextView picture_right;
/*      */   private TextView picture_tv_ok;
/*      */   private TextView tv_empty;
/*      */   private TextView picture_tv_img_num;
/*      */   private TextView picture_id_preview;
/*      */   private TextView tv_PlayPause;
/*   76 */   private TextView tv_Stop; private TextView tv_Quit; private TextView tv_musicStatus; private TextView tv_musicTotal; private TextView tv_musicTime; private RelativeLayout rl_picture_title; private RelativeLayout rl_bottom; private LinearLayout id_ll_ok; private RecyclerView picture_recycler; private PictureImageGridAdapter adapter; private List<LocalMedia> images = new ArrayList();
/*   77 */   private List<LocalMediaFolder> foldersList = new ArrayList();
/*      */   private FolderPopWindow folderWindow;
/*   79 */   private Animation animation = null;
/*   80 */   private boolean anim = false;
/*      */   private int preview_textColor;
/*      */   private int complete_textColor;
/*      */   private RxPermissions rxPermissions;
/*      */   private PhotoPopupWindow popupWindow;
/*      */   private LocalMediaLoader mediaLoader;
/*      */   private MediaPlayer mediaPlayer;
/*   87 */   private SeekBar musicSeekBar; private boolean isPlayAudio = false;
/*      */   private CustomDialog audioDialog;
/*      */   private int audioH;
/*      */   
/*      */   @Subscribe(threadMode=ThreadMode.MAIN)
/*      */   public void eventBus(EventEntity obj)
/*      */   {
/*   94 */     switch (obj.what)
/*      */     {
/*      */     case 2774: 
/*   97 */       List<LocalMedia> selectImages = obj.medias;
/*   98 */       this.anim = (selectImages.size() > 0);
/*   99 */       int position = obj.position;
/*  100 */       DebugUtil.i(TAG, "刷新下标::" + position);
/*  101 */       this.adapter.bindSelectImages(selectImages);
/*  102 */       this.adapter.notifyItemChanged(position);
/*  103 */       break;
/*      */     case 2771: 
/*  105 */       List<LocalMedia> medias = obj.medias;
/*  106 */       if (medias.size() > 0)
/*      */       {
/*  108 */         String pictureType = ((LocalMedia)medias.get(0)).getPictureType();
/*  109 */         if ((this.isCompress) && (pictureType.startsWith("image"))) {
/*  110 */           compressImage(medias);
/*      */         } else {
/*  112 */           onResult(medias);
/*      */         }
/*      */       }
/*      */       break;
/*      */     }
/*      */   }
/*      */   
/*      */   protected void onCreate(Bundle savedInstanceState)
/*      */   {
/*  121 */     super.onCreate(savedInstanceState);
/*  122 */     if (!RxBus.getDefault().isRegistered(this)) {
/*  123 */       RxBus.getDefault().register(this);
/*      */     }
/*  125 */     this.rxPermissions = new RxPermissions(this);
/*  126 */     LightStatusBarUtils.setLightStatusBar(this, this.statusFont);
/*  127 */     if (this.camera) {
/*  128 */       if (savedInstanceState == null)
/*      */       {
/*  130 */         this.rxPermissions.request(new String[] { "android.permission.READ_EXTERNAL_STORAGE" }).subscribe(new Observer()
/*      */         {
/*      */           public void onSubscribe(Disposable d) {}
/*      */           
/*      */ 
/*      */           public void onNext(Boolean aBoolean)
/*      */           {
/*  137 */             if (aBoolean.booleanValue()) {
/*  138 */               PictureSelectorActivity.this.onTakePhoto();
/*      */             } else {
/*  140 */               PictureSelectorActivity.this.showToast(PictureSelectorActivity.this.getString(R.string.picture_camera));
/*  141 */               PictureSelectorActivity.this.closeActivity();
/*      */             }
/*      */           }
/*      */           
/*      */ 
/*      */ 
/*      */           public void onError(Throwable e) {}
/*      */           
/*      */ 
/*      */           public void onComplete() {}
/*      */         });
/*      */       }
/*      */       
/*  154 */       getWindow().setFlags(1024, 1024);
/*      */       
/*  156 */       setContentView(R.layout.picture_empty);
/*      */     } else {
/*  158 */       setContentView(R.layout.picture_selector);
/*  159 */       initView(savedInstanceState);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void initView(Bundle savedInstanceState)
/*      */   {
/*  169 */     this.rl_picture_title = ((RelativeLayout)findViewById(R.id.rl_picture_title));
/*  170 */     this.picture_left_back = ((ImageView)findViewById(R.id.picture_left_back));
/*  171 */     this.picture_title = ((TextView)findViewById(R.id.picture_title));
/*  172 */     this.picture_right = ((TextView)findViewById(R.id.picture_right));
/*  173 */     this.picture_tv_ok = ((TextView)findViewById(R.id.picture_tv_ok));
/*  174 */     this.picture_id_preview = ((TextView)findViewById(R.id.picture_id_preview));
/*  175 */     this.picture_tv_img_num = ((TextView)findViewById(R.id.picture_tv_img_num));
/*  176 */     this.picture_recycler = ((RecyclerView)findViewById(R.id.picture_recycler));
/*  177 */     this.rl_bottom = ((RelativeLayout)findViewById(R.id.rl_bottom));
/*  178 */     this.id_ll_ok = ((LinearLayout)findViewById(R.id.id_ll_ok));
/*  179 */     this.tv_empty = ((TextView)findViewById(R.id.tv_empty));
/*  180 */     this.rl_bottom.setVisibility(this.selectionMode == 1 ? 8 : 0);
/*  181 */     isNumComplete(this.numComplete);
/*  182 */     if (this.mimeType == PictureMimeType.ofAll()) {
/*  183 */       this.popupWindow = new PhotoPopupWindow(this);
/*  184 */       this.popupWindow.setOnItemClickListener(this);
/*      */     }
/*  186 */     this.picture_id_preview.setOnClickListener(this);
/*  187 */     if (this.mimeType == PictureMimeType.ofAudio()) {
/*  188 */       this.picture_id_preview.setVisibility(8);
/*      */       
/*  190 */       this.audioH = (ScreenUtils.getScreenHeight(this.mContext) + ScreenUtils.getStatusBarHeight(this.mContext));
/*      */     } else {
/*  192 */       this.picture_id_preview.setVisibility(this.mimeType == 2 ? 8 : 0);
/*      */     }
/*      */     
/*  195 */     this.picture_left_back.setOnClickListener(this);
/*  196 */     this.picture_right.setOnClickListener(this);
/*  197 */     this.id_ll_ok.setOnClickListener(this);
/*  198 */     this.picture_title.setOnClickListener(this);
/*      */     
/*      */ 
/*  201 */     String title = this.mimeType == PictureMimeType.ofAudio() ? getString(R.string.picture_all_audio) : getString(R.string.picture_camera_roll);
/*  202 */     this.picture_title.setText(title);
/*  203 */     this.folderWindow = new FolderPopWindow(this, this.mimeType);
/*  204 */     this.folderWindow.setPictureTitleView(this.picture_title);
/*  205 */     this.folderWindow.setOnItemClickListener(this);
/*  206 */     this.picture_recycler.setHasFixedSize(true);
/*  207 */     this.picture_recycler.addItemDecoration(new GridSpacingItemDecoration(this.spanCount, 
/*  208 */       ScreenUtils.dip2px(this, 2.0F), false));
/*  209 */     this.picture_recycler.setLayoutManager(new GridLayoutManager(this, this.spanCount));
/*      */     
/*  211 */     ((SimpleItemAnimator)this.picture_recycler.getItemAnimator())
/*  212 */       .setSupportsChangeAnimations(false);
/*  213 */     this.mediaLoader = new LocalMediaLoader(this, this.mimeType, this.isGif, this.videoMaxSecond, this.videoMinSecond);
/*  214 */     this.rxPermissions.request(new String[] { "android.permission.READ_EXTERNAL_STORAGE" })
/*  215 */       .subscribe(new Observer()
/*      */       {
/*      */         public void onSubscribe(Disposable d) {}
/*      */         
/*      */ 
/*      */ 
/*      */         public void onNext(Boolean aBoolean)
/*      */         {
/*  222 */           PictureSelectorActivity.this.showPleaseDialog();
/*  223 */           if (aBoolean.booleanValue()) {
/*  224 */             PictureSelectorActivity.this.readLocalMedia();
/*      */           } else {
/*  226 */             PictureSelectorActivity.this.showToast(PictureSelectorActivity.this.getString(R.string.picture_jurisdiction));
/*  227 */             PictureSelectorActivity.this.dismissDialog();
/*      */           }
/*      */         }
/*      */         
/*      */ 
/*      */ 
/*      */         public void onError(Throwable e) {}
/*      */         
/*      */ 
/*      */ 
/*      */         public void onComplete() {}
/*  238 */       });
/*  239 */     this.tv_empty.setText(this.mimeType == PictureMimeType.ofAudio() ? 
/*  240 */       getString(R.string.picture_audio_empty) : 
/*  241 */       getString(R.string.picture_empty));
/*  242 */     StringUtils.tempTextFont(this.tv_empty, this.mimeType);
/*  243 */     if (savedInstanceState != null)
/*      */     {
/*  245 */       this.selectionMedias = PictureSelector.obtainSelectorList(savedInstanceState);
/*  246 */       this.preview_textColor = savedInstanceState.getInt("preview_textColor");
/*  247 */       this.complete_textColor = savedInstanceState.getInt("complete_textColor");
/*      */     } else {
/*  249 */       this.preview_textColor = AttrsUtils.getTypeValueColor(this, R.attr.picture_preview_textColor);
/*  250 */       this.complete_textColor = AttrsUtils.getTypeValueColor(this, R.attr.picture_complete_textColor);
/*      */     }
/*  252 */     this.adapter = new PictureImageGridAdapter(this.mContext, this.config);
/*  253 */     this.adapter.setOnPhotoSelectChangedListener(this);
/*  254 */     this.adapter.bindSelectImages(this.selectionMedias);
/*  255 */     this.picture_recycler.setAdapter(this.adapter);
/*  256 */     String titleText = this.picture_title.getText().toString().trim();
/*  257 */     if (this.isCamera) {
/*  258 */       this.isCamera = StringUtils.isCamera(titleText);
/*      */     }
/*      */   }
/*      */   
/*      */   protected void onSaveInstanceState(Bundle outState)
/*      */   {
/*  264 */     super.onSaveInstanceState(outState);
/*  265 */     if (this.adapter != null) {
/*  266 */       outState.putInt("preview_textColor", this.preview_textColor);
/*  267 */       outState.putInt("complete_textColor", this.complete_textColor);
/*  268 */       List<LocalMedia> selectedImages = this.adapter.getSelectedImages();
/*  269 */       PictureSelector.saveSelectorList(outState, selectedImages);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private void isNumComplete(boolean numComplete)
/*      */   {
/*  277 */     this.picture_tv_ok.setText(numComplete ? getString(R.string.picture_done_front_num, new Object[] { Integer.valueOf(0), Integer.valueOf(this.maxSelectNum) }) : 
/*  278 */       getString(R.string.picture_please_select));
/*  279 */     if (!numComplete) {
/*  280 */       this.animation = AnimationUtils.loadAnimation(this, R.anim.modal_in);
/*      */     }
/*  282 */     this.animation = (numComplete ? null : AnimationUtils.loadAnimation(this, R.anim.modal_in));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   protected void readLocalMedia()
/*      */   {
/*  289 */     this.mediaLoader.loadAllMedia(new LocalMediaLoader.LocalMediaLoadListener()
/*      */     {
/*      */       public void loadComplete(List<LocalMediaFolder> folders) {
/*  292 */         DebugUtil.i("loadComplete:" + folders.size());
/*  293 */         if (folders.size() > 0) {
/*  294 */           PictureSelectorActivity.this.foldersList = folders;
/*  295 */           LocalMediaFolder folder = (LocalMediaFolder)folders.get(0);
/*  296 */           folder.setChecked(true);
/*  297 */           List<LocalMedia> localImg = folder.getImages();
/*      */           
/*      */ 
/*      */ 
/*  301 */           if (localImg.size() >= PictureSelectorActivity.this.images.size()) {
/*  302 */             PictureSelectorActivity.this.images = localImg;
/*  303 */             PictureSelectorActivity.this.folderWindow.bindFolder(folders);
/*      */           }
/*      */         }
/*  306 */         if (PictureSelectorActivity.this.adapter != null) {
/*  307 */           if (PictureSelectorActivity.this.images == null) {
/*  308 */             PictureSelectorActivity.this.images = new ArrayList();
/*      */           }
/*  310 */           PictureSelectorActivity.this.adapter.bindImagesData(PictureSelectorActivity.this.images);
/*  311 */           PictureSelectorActivity.this.tv_empty.setVisibility(PictureSelectorActivity.this.images.size() > 0 ? 4 : 0);
/*      */         }
/*      */         
/*  314 */         PictureSelectorActivity.this.dismissDialog();
/*      */       }
/*      */     });
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void startCamera()
/*      */   {
/*  324 */     if ((!DoubleUtils.isFastDoubleClick()) || (this.camera)) {
/*  325 */       switch (this.mimeType)
/*      */       {
/*      */       case 0: 
/*  328 */         if (this.popupWindow != null) {
/*  329 */           if (this.popupWindow.isShowing()) {
/*  330 */             this.popupWindow.dismiss();
/*      */           }
/*  332 */           this.popupWindow.showAsDropDown(this.rl_picture_title);
/*      */         } else {
/*  334 */           startOpenCamera();
/*      */         }
/*  336 */         break;
/*      */       
/*      */       case 1: 
/*  339 */         startOpenCamera();
/*  340 */         break;
/*      */       
/*      */       case 2: 
/*  343 */         startOpenCameraVideo();
/*  344 */         break;
/*      */       
/*      */       case 3: 
/*  347 */         startOpenCameraAudio();
/*      */       }
/*      */       
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void startOpenCamera()
/*      */   {
/*  357 */     Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");
/*  358 */     if (cameraIntent.resolveActivity(getPackageManager()) != null) {
/*  359 */       File cameraFile = PictureFileUtils.createCameraFile(this, this.mimeType == 0 ? 1 : this.mimeType, this.outputCameraPath);
/*      */       
/*      */ 
/*  362 */       this.cameraPath = cameraFile.getAbsolutePath();
/*  363 */       Uri imageUri = parUri(cameraFile);
/*  364 */       cameraIntent.putExtra("output", imageUri);
/*  365 */       startActivityForResult(cameraIntent, 909);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void startOpenCameraVideo()
/*      */   {
/*  373 */     Intent cameraIntent = new Intent("android.media.action.VIDEO_CAPTURE");
/*  374 */     if (cameraIntent.resolveActivity(getPackageManager()) != null) {
/*  375 */       File cameraFile = PictureFileUtils.createCameraFile(this, this.mimeType == 0 ? 2 : this.mimeType, this.outputCameraPath);
/*      */       
/*      */ 
/*  378 */       this.cameraPath = cameraFile.getAbsolutePath();
/*  379 */       Uri imageUri = parUri(cameraFile);
/*  380 */       DebugUtil.i(TAG, "video second:" + this.recordVideoSecond);
/*  381 */       cameraIntent.putExtra("output", imageUri);
/*  382 */       cameraIntent.putExtra("android.intent.extra.durationLimit", this.recordVideoSecond);
/*  383 */       cameraIntent.putExtra("android.intent.extra.videoQuality", this.videoQuality);
/*  384 */       startActivityForResult(cameraIntent, 909);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void startOpenCameraAudio()
/*      */   {
/*  392 */     this.rxPermissions.request(new String[] { "android.permission.RECORD_AUDIO" }).subscribe(new Observer()
/*      */     {
/*      */       public void onSubscribe(Disposable d) {}
/*      */       
/*      */ 
/*      */       public void onNext(Boolean aBoolean)
/*      */       {
/*  399 */         if (aBoolean.booleanValue()) {
/*  400 */           Intent cameraIntent = new Intent("android.provider.MediaStore.RECORD_SOUND");
/*  401 */           if (cameraIntent.resolveActivity(PictureSelectorActivity.this.getPackageManager()) != null)
/*      */           {
/*  403 */             File cameraFile = PictureFileUtils.createCameraFile(PictureSelectorActivity.this, PictureSelectorActivity.this.mimeType, PictureSelectorActivity.this.outputCameraPath);
/*      */             
/*  405 */             PictureSelectorActivity.this.cameraPath = cameraFile.getAbsolutePath();
/*  406 */             Uri imageUri = PictureSelectorActivity.this.parUri(cameraFile);
/*  407 */             cameraIntent.putExtra("output", imageUri);
/*  408 */             PictureSelectorActivity.this.startActivityForResult(cameraIntent, 909);
/*      */           }
/*      */         } else {
/*  411 */           PictureSelectorActivity.this.showToast(PictureSelectorActivity.this.getString(R.string.picture_audio));
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       public void onError(Throwable e) {}
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */       public void onComplete() {}
/*      */     });
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private Uri parUri(File cameraFile)
/*      */   {
/*  433 */     String authority = getPackageName() + ".provider";
/*  434 */     Uri imageUri; Uri imageUri; if (Build.VERSION.SDK_INT >= 23)
/*      */     {
/*  436 */       imageUri = FileProvider.getUriForFile(this.mContext, authority, cameraFile);
/*      */     } else {
/*  438 */       imageUri = Uri.fromFile(cameraFile);
/*      */     }
/*  440 */     return imageUri;
/*      */   }
/*      */   
/*      */ 
/*      */   public void onClick(View v)
/*      */   {
/*  446 */     int id = v.getId();
/*  447 */     if ((id == R.id.picture_left_back) || (id == R.id.picture_right)) {
/*  448 */       if (this.folderWindow.isShowing()) {
/*  449 */         this.folderWindow.dismiss();
/*      */       } else {
/*  451 */         closeActivity();
/*      */       }
/*      */     }
/*  454 */     if (id == R.id.picture_title) {
/*  455 */       if (this.folderWindow.isShowing()) {
/*  456 */         this.folderWindow.dismiss();
/*      */       }
/*  458 */       else if ((this.images != null) && (this.images.size() > 0)) {
/*  459 */         this.folderWindow.showAsDropDown(this.rl_picture_title);
/*  460 */         List<LocalMedia> selectedImages = this.adapter.getSelectedImages();
/*  461 */         this.folderWindow.notifyDataCheckedStatus(selectedImages);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  466 */     if (id == R.id.picture_id_preview) {
/*  467 */       List<LocalMedia> selectedImages = this.adapter.getSelectedImages();
/*      */       
/*  469 */       List<LocalMedia> medias = new ArrayList();
/*  470 */       for (LocalMedia media : selectedImages) {
/*  471 */         medias.add(media);
/*      */       }
/*  473 */       Bundle bundle = new Bundle();
/*  474 */       bundle.putSerializable("previewSelectList", (Serializable)medias);
/*  475 */       bundle.putSerializable("selectList", (Serializable)selectedImages);
/*  476 */       bundle.putBoolean("bottom_preview", true);
/*  477 */       startActivity(PicturePreviewActivity.class, bundle, 609);
/*  478 */       overridePendingTransition(R.anim.a5, 0);
/*      */     }
/*      */     
/*  481 */     if (id == R.id.id_ll_ok) {
/*  482 */       List<LocalMedia> images = this.adapter.getSelectedImages();
/*  483 */       String pictureType = images.size() > 0 ? ((LocalMedia)images.get(0)).getPictureType() : "";
/*      */       
/*  485 */       int size = images.size();
/*  486 */       boolean eqImg = pictureType.startsWith("image");
/*  487 */       if ((this.minSelectNum > 0) && (this.selectionMode == 2) && 
/*  488 */         (size < this.minSelectNum))
/*      */       {
/*  490 */         String str = eqImg ? getString(R.string.picture_min_img_num, new Object[] { Integer.valueOf(this.minSelectNum) }) : getString(R.string.picture_min_video_num, new Object[] { Integer.valueOf(this.minSelectNum) });
/*  491 */         showToast(str);
/*  492 */         return;
/*      */       }
/*      */       
/*  495 */       if ((this.enableCrop) && (eqImg))
/*      */       {
/*  497 */         ArrayList<String> medias = new ArrayList();
/*  498 */         for (LocalMedia media : images) {
/*  499 */           medias.add(media.getPath());
/*      */         }
/*  501 */         startCrop(medias);
/*  502 */       } else if ((this.isCompress) && (eqImg))
/*      */       {
/*  504 */         compressImage(images);
/*      */       } else {
/*  506 */         onResult(images);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void audioDialog(final String path)
/*      */   {
/*  517 */     this.audioDialog = new CustomDialog(this.mContext, -1, this.audioH, R.layout.picture_audio_dialog, R.style.Theme_dialog);
/*      */     
/*      */ 
/*      */ 
/*  521 */     this.audioDialog.getWindow().setWindowAnimations(R.style.Dialog_Audio_StyleAnim);
/*  522 */     this.tv_musicStatus = ((TextView)this.audioDialog.findViewById(R.id.tv_musicStatus));
/*  523 */     this.tv_musicTime = ((TextView)this.audioDialog.findViewById(R.id.tv_musicTime));
/*  524 */     this.musicSeekBar = ((SeekBar)this.audioDialog.findViewById(R.id.musicSeekBar));
/*  525 */     this.tv_musicTotal = ((TextView)this.audioDialog.findViewById(R.id.tv_musicTotal));
/*  526 */     this.tv_PlayPause = ((TextView)this.audioDialog.findViewById(R.id.tv_PlayPause));
/*  527 */     this.tv_Stop = ((TextView)this.audioDialog.findViewById(R.id.tv_Stop));
/*  528 */     this.tv_Quit = ((TextView)this.audioDialog.findViewById(R.id.tv_Quit));
/*  529 */     this.handler.postDelayed(new Runnable()
/*      */     {
/*      */ 
/*  532 */       public void run() { PictureSelectorActivity.this.initPlayer(path); } }, 30L);
/*      */     
/*      */ 
/*  535 */     this.tv_PlayPause.setOnClickListener(new audioOnClick(path));
/*  536 */     this.tv_Stop.setOnClickListener(new audioOnClick(path));
/*  537 */     this.tv_Quit.setOnClickListener(new audioOnClick(path));
/*  538 */     this.musicSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
/*      */     {
/*      */       public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
/*  541 */         if (fromUser == true) {
/*  542 */           PictureSelectorActivity.this.mediaPlayer.seekTo(progress);
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */       public void onStartTrackingTouch(SeekBar seekBar) {}
/*      */       
/*      */ 
/*      */ 
/*      */       public void onStopTrackingTouch(SeekBar seekBar) {}
/*  553 */     });
/*  554 */     this.audioDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
/*      */     {
/*      */       public void onDismiss(DialogInterface dialog) {
/*  557 */         PictureSelectorActivity.this.handler.removeCallbacks(PictureSelectorActivity.this.runnable);
/*  558 */         new Handler().postDelayed(new Runnable()
/*      */         {
/*      */ 
/*  561 */           public void run() { PictureSelectorActivity.this.stop(path); } }, 30L);
/*      */         
/*      */         try
/*      */         {
/*  565 */           if ((PictureSelectorActivity.this.audioDialog != null) && 
/*  566 */             (PictureSelectorActivity.this.audioDialog.isShowing())) {
/*  567 */             PictureSelectorActivity.this.audioDialog.dismiss();
/*      */           }
/*      */         } catch (Exception e) {
/*  570 */           e.printStackTrace();
/*      */         }
/*      */       }
/*  573 */     });
/*  574 */     this.handler.post(this.runnable);
/*  575 */     this.audioDialog.show();
/*      */   }
/*      */   
/*      */ 
/*  579 */   public Handler handler = new Handler();
/*  580 */   public Runnable runnable = new Runnable()
/*      */   {
/*      */     public void run() {
/*      */       try {
/*  584 */         if (PictureSelectorActivity.this.mediaPlayer != null) {
/*  585 */           PictureSelectorActivity.this.tv_musicTime.setText(DateUtils.timeParse(PictureSelectorActivity.this.mediaPlayer.getCurrentPosition()));
/*  586 */           PictureSelectorActivity.this.musicSeekBar.setProgress(PictureSelectorActivity.this.mediaPlayer.getCurrentPosition());
/*  587 */           PictureSelectorActivity.this.musicSeekBar.setMax(PictureSelectorActivity.this.mediaPlayer.getDuration());
/*  588 */           PictureSelectorActivity.this.tv_musicTotal.setText(DateUtils.timeParse(PictureSelectorActivity.this.mediaPlayer.getDuration()));
/*  589 */           PictureSelectorActivity.this.handler.postDelayed(PictureSelectorActivity.this.runnable, 200L);
/*      */         }
/*      */       } catch (Exception e) {
/*  592 */         e.printStackTrace();
/*      */       }
/*      */     }
/*      */   };
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void initPlayer(String path)
/*      */   {
/*  603 */     this.mediaPlayer = new MediaPlayer();
/*      */     try {
/*  605 */       this.mediaPlayer.setDataSource(path);
/*  606 */       this.mediaPlayer.prepare();
/*  607 */       this.mediaPlayer.setLooping(true);
/*  608 */       playAudio();
/*      */     } catch (Exception e) {
/*  610 */       e.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public class audioOnClick
/*      */     implements View.OnClickListener
/*      */   {
/*      */     private String path;
/*      */     
/*      */     public audioOnClick(String path)
/*      */     {
/*  622 */       this.path = path;
/*      */     }
/*      */     
/*      */     public void onClick(View v)
/*      */     {
/*  627 */       int id = v.getId();
/*  628 */       if (id == R.id.tv_PlayPause) {
/*  629 */         PictureSelectorActivity.this.playAudio();
/*      */       }
/*  631 */       if (id == R.id.tv_Stop) {
/*  632 */         PictureSelectorActivity.this.tv_musicStatus.setText(PictureSelectorActivity.this.getString(R.string.picture_stop_audio));
/*  633 */         PictureSelectorActivity.this.tv_PlayPause.setText(PictureSelectorActivity.this.getString(R.string.picture_play_audio));
/*  634 */         PictureSelectorActivity.this.stop(this.path);
/*      */       }
/*  636 */       if (id == R.id.tv_Quit) {
/*  637 */         PictureSelectorActivity.this.handler.removeCallbacks(PictureSelectorActivity.this.runnable);
/*  638 */         new Handler().postDelayed(new Runnable()
/*      */         {
/*      */ 
/*  641 */           public void run() { PictureSelectorActivity.this.stop(PictureSelectorActivity.audioOnClick.this.path); } }, 30L);
/*      */         
/*      */         try
/*      */         {
/*  645 */           if ((PictureSelectorActivity.this.audioDialog != null) && 
/*  646 */             (PictureSelectorActivity.this.audioDialog.isShowing())) {
/*  647 */             PictureSelectorActivity.this.audioDialog.dismiss();
/*      */           }
/*      */         } catch (Exception e) {
/*  650 */           e.printStackTrace();
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private void playAudio()
/*      */   {
/*  660 */     if (this.mediaPlayer != null) {
/*  661 */       this.musicSeekBar.setProgress(this.mediaPlayer.getCurrentPosition());
/*  662 */       this.musicSeekBar.setMax(this.mediaPlayer.getDuration());
/*      */     }
/*  664 */     String ppStr = this.tv_PlayPause.getText().toString();
/*  665 */     if (ppStr.equals(getString(R.string.picture_play_audio))) {
/*  666 */       this.tv_PlayPause.setText(getString(R.string.picture_pause_audio));
/*  667 */       this.tv_musicStatus.setText(getString(R.string.picture_play_audio));
/*  668 */       playOrPause();
/*      */     } else {
/*  670 */       this.tv_PlayPause.setText(getString(R.string.picture_play_audio));
/*  671 */       this.tv_musicStatus.setText(getString(R.string.picture_pause_audio));
/*  672 */       playOrPause();
/*      */     }
/*  674 */     if (!this.isPlayAudio) {
/*  675 */       this.handler.post(this.runnable);
/*  676 */       this.isPlayAudio = true;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void stop(String path)
/*      */   {
/*  686 */     if (this.mediaPlayer != null) {
/*      */       try {
/*  688 */         this.mediaPlayer.stop();
/*  689 */         this.mediaPlayer.reset();
/*  690 */         this.mediaPlayer.setDataSource(path);
/*  691 */         this.mediaPlayer.prepare();
/*  692 */         this.mediaPlayer.seekTo(0);
/*      */       } catch (Exception e) {
/*  694 */         e.printStackTrace();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public void playOrPause()
/*      */   {
/*      */     try
/*      */     {
/*  704 */       if (this.mediaPlayer != null) {
/*  705 */         if (this.mediaPlayer.isPlaying()) {
/*  706 */           this.mediaPlayer.pause();
/*      */         } else {
/*  708 */           this.mediaPlayer.start();
/*      */         }
/*      */       }
/*      */     } catch (Exception e) {
/*  712 */       e.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   public void onItemClick(String folderName, List<LocalMedia> images)
/*      */   {
/*  718 */     boolean camera = StringUtils.isCamera(folderName);
/*  719 */     camera = this.isCamera ? camera : false;
/*  720 */     this.adapter.setShowCamera(camera);
/*  721 */     this.picture_title.setText(folderName);
/*  722 */     this.adapter.bindImagesData(images);
/*  723 */     this.folderWindow.dismiss();
/*      */   }
/*      */   
/*      */ 
/*      */   public void onTakePhoto()
/*      */   {
/*  729 */     this.rxPermissions.request(new String[] { "android.permission.CAMERA" }).subscribe(new Observer()
/*      */     {
/*      */       public void onSubscribe(Disposable d) {}
/*      */       
/*      */ 
/*      */ 
/*      */       public void onNext(Boolean aBoolean)
/*      */       {
/*  737 */         if (aBoolean.booleanValue()) {
/*  738 */           PictureSelectorActivity.this.startCamera();
/*      */         } else {
/*  740 */           PictureSelectorActivity.this.showToast(PictureSelectorActivity.this.getString(R.string.picture_camera));
/*  741 */           if (PictureSelectorActivity.this.camera) {
/*  742 */             PictureSelectorActivity.this.closeActivity();
/*      */           }
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */       public void onError(Throwable e) {}
/*      */       
/*      */ 
/*      */ 
/*      */       public void onComplete() {}
/*      */     });
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void onChange(List<LocalMedia> selectImages)
/*      */   {
/*  761 */     changeImageNumber(selectImages);
/*      */   }
/*      */   
/*      */   public void onPictureClick(LocalMedia media, int position)
/*      */   {
/*  766 */     List<LocalMedia> images = this.adapter.getImages();
/*  767 */     startPreview(images, position);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void startPreview(List<LocalMedia> previewImages, int position)
/*      */   {
/*  777 */     LocalMedia media = (LocalMedia)previewImages.get(position);
/*  778 */     String pictureType = media.getPictureType();
/*  779 */     Bundle bundle = new Bundle();
/*  780 */     List<LocalMedia> result = new ArrayList();
/*  781 */     int mediaType = PictureMimeType.isPictureType(pictureType);
/*  782 */     DebugUtil.i(TAG, "mediaType:" + mediaType);
/*  783 */     switch (mediaType)
/*      */     {
/*      */     case 1: 
/*  786 */       if (this.selectionMode == 1) {
/*  787 */         if (this.enableCrop) {
/*  788 */           this.originalPath = media.getPath();
/*  789 */           boolean gif = PictureMimeType.isGif(pictureType);
/*  790 */           if (gif) {
/*  791 */             result.add(media);
/*  792 */             handlerResult(result);
/*      */           } else {
/*  794 */             startCrop(this.originalPath);
/*      */           }
/*      */         } else {
/*  797 */           result.add(media);
/*  798 */           handlerResult(result);
/*      */         }
/*      */       } else {
/*  801 */         List<LocalMedia> selectedImages = this.adapter.getSelectedImages();
/*  802 */         ImagesObservable.getInstance().saveLocalMedia(previewImages);
/*  803 */         bundle.putSerializable("selectList", (Serializable)selectedImages);
/*  804 */         bundle.putInt("position", position);
/*  805 */         startActivity(PicturePreviewActivity.class, bundle, 609);
/*  806 */         overridePendingTransition(R.anim.a5, 0);
/*      */       }
/*  808 */       break;
/*      */     
/*      */     case 2: 
/*  811 */       if (this.selectionMode == 1) {
/*  812 */         result.add(media);
/*  813 */         onResult(result);
/*      */       } else {
/*  815 */         bundle.putString("video_path", media.getPath());
/*  816 */         startActivity(PictureVideoPlayActivity.class, bundle);
/*      */       }
/*  818 */       break;
/*      */     
/*      */     case 3: 
/*  821 */       if (this.selectionMode == 1) {
/*  822 */         result.add(media);
/*  823 */         onResult(result);
/*      */       } else {
/*  825 */         audioDialog(media.getPath());
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */       break;
/*      */     }
/*      */     
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void changeImageNumber(List<LocalMedia> selectImages)
/*      */   {
/*  840 */     String pictureType = selectImages.size() > 0 ? ((LocalMedia)selectImages.get(0)).getPictureType() : "";
/*  841 */     if (this.mimeType == PictureMimeType.ofAudio()) {
/*  842 */       this.picture_id_preview.setVisibility(8);
/*      */     } else {
/*  844 */       boolean isVideo = PictureMimeType.isVideo(pictureType);
/*  845 */       this.picture_id_preview.setVisibility(isVideo ? 8 : 0);
/*      */     }
/*  847 */     boolean enable = selectImages.size() != 0;
/*  848 */     if (enable) {
/*  849 */       this.id_ll_ok.setEnabled(true);
/*  850 */       this.picture_id_preview.setEnabled(true);
/*  851 */       this.picture_id_preview.setTextColor(this.preview_textColor);
/*  852 */       this.picture_tv_ok.setTextColor(this.complete_textColor);
/*  853 */       if (this.numComplete) {
/*  854 */         this.picture_tv_ok.setText(
/*  855 */           getString(R.string.picture_done_front_num, new Object[] {Integer.valueOf(selectImages.size()), Integer.valueOf(this.maxSelectNum) }));
/*      */       } else {
/*  857 */         if (!this.anim) {
/*  858 */           this.picture_tv_img_num.startAnimation(this.animation);
/*      */         }
/*  860 */         this.picture_tv_img_num.setVisibility(0);
/*  861 */         this.picture_tv_img_num.setText(selectImages.size() + "");
/*  862 */         this.picture_tv_ok.setText(getString(R.string.picture_completed));
/*  863 */         this.anim = false;
/*      */       }
/*      */     } else {
/*  866 */       this.id_ll_ok.setEnabled(false);
/*  867 */       this.picture_id_preview.setEnabled(false);
/*  868 */       this.picture_tv_ok.setTextColor(ContextCompat.getColor(this.mContext, R.color.tab_color_false));
/*  869 */       this.picture_id_preview
/*  870 */         .setTextColor(ContextCompat.getColor(this.mContext, R.color.tab_color_false));
/*  871 */       if (this.numComplete) {
/*  872 */         this.picture_tv_ok.setText(getString(R.string.picture_done_front_num, new Object[] { Integer.valueOf(0), Integer.valueOf(this.maxSelectNum) }));
/*      */       } else {
/*  874 */         this.picture_tv_img_num.setVisibility(4);
/*  875 */         this.picture_tv_ok.setText(getString(R.string.picture_please_select));
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   protected void onActivityResult(int requestCode, int resultCode, Intent data)
/*      */   {
/*  883 */     if (resultCode == -1) {
/*  884 */       List<LocalMedia> medias = new ArrayList();
/*      */       
/*      */ 
/*  887 */       switch (requestCode) {
/*      */       case 69: 
/*  889 */         Uri resultUri = UCrop.getOutput(data);
/*  890 */         String cutPath = resultUri.getPath();
/*  891 */         LocalMedia media = new LocalMedia(this.originalPath, 0L, false, 0, 0, this.mimeType);
/*  892 */         media.setCutPath(cutPath);
/*  893 */         media.setCut(true);
/*  894 */         String imageType = PictureMimeType.createImageType(cutPath);
/*  895 */         media.setPictureType(imageType);
/*  896 */         medias.add(media);
/*  897 */         DebugUtil.i(TAG, "cut createImageType:" + imageType);
/*  898 */         handlerResult(medias);
/*  899 */         break;
/*      */       case 609: 
/*  901 */         List<CutInfo> cuts = UCropMulti.getOutput(data);
/*  902 */         for (CutInfo c : cuts) {
/*  903 */           LocalMedia media = new LocalMedia();
/*  904 */           String imageType = PictureMimeType.createImageType(c.getPath());
/*  905 */           media.setCut(true);
/*  906 */           media.setPath(c.getPath());
/*  907 */           media.setCutPath(c.getCutPath());
/*  908 */           media.setPictureType(imageType);
/*  909 */           media.setMimeType(this.mimeType);
/*  910 */           medias.add(media);
/*      */         }
/*  912 */         handlerResult(medias);
/*  913 */         break;
/*      */       case 909: 
/*  915 */         isAudio(data);
/*      */         
/*  917 */         File file = new File(this.cameraPath);
/*  918 */         sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));
/*  919 */         String toType = PictureMimeType.fileToType(file);
/*  920 */         DebugUtil.i(TAG, "camera result:" + toType);
/*  921 */         if (this.mimeType != PictureMimeType.ofAudio()) {
/*  922 */           int degree = PictureFileUtils.readPictureDegree(file.getAbsolutePath());
/*  923 */           rotateImage(degree, file);
/*      */         }
/*      */         
/*  926 */         LocalMedia media = new LocalMedia();
/*  927 */         media.setPath(this.cameraPath);
/*      */         
/*  929 */         boolean eqVideo = toType.startsWith("video");
/*  930 */         int duration = eqVideo ? PictureMimeType.getLocalVideoDuration(this.cameraPath) : 0;
/*  931 */         String pictureType = "";
/*  932 */         if (this.mimeType == PictureMimeType.ofAudio()) {
/*  933 */           pictureType = "audio/mpeg";
/*  934 */           duration = PictureMimeType.getLocalVideoDuration(this.cameraPath);
/*      */         }
/*      */         else {
/*  937 */           pictureType = eqVideo ? PictureMimeType.createVideoType(this.cameraPath) : PictureMimeType.createImageType(this.cameraPath);
/*      */         }
/*  939 */         media.setPictureType(pictureType);
/*  940 */         media.setDuration(duration);
/*  941 */         media.setMimeType(this.mimeType);
/*      */         
/*      */ 
/*  944 */         if ((this.selectionMode == 1) || (this.camera))
/*      */         {
/*  946 */           boolean eqImg = toType.startsWith("image");
/*  947 */           if ((this.enableCrop) && (eqImg))
/*      */           {
/*  949 */             this.originalPath = this.cameraPath;
/*  950 */             startCrop(this.cameraPath);
/*  951 */           } else if ((this.isCompress) && (eqImg))
/*      */           {
/*  953 */             medias.add(media);
/*  954 */             compressImage(medias);
/*  955 */             if (this.adapter != null) {
/*  956 */               this.images.add(0, media);
/*  957 */               this.adapter.notifyDataSetChanged();
/*      */             }
/*      */           }
/*      */           else {
/*  961 */             medias.add(media);
/*  962 */             onResult(medias);
/*      */           }
/*      */         }
/*      */         else {
/*  966 */           this.images.add(0, media);
/*  967 */           if (this.adapter != null) {
/*  968 */             List<LocalMedia> selectedImages = this.adapter.getSelectedImages();
/*      */             
/*  970 */             if (selectedImages.size() < this.maxSelectNum) {
/*  971 */               pictureType = selectedImages.size() > 0 ? ((LocalMedia)selectedImages.get(0)).getPictureType() : "";
/*  972 */               boolean toEqual = PictureMimeType.mimeToEqual(pictureType, media.getPictureType());
/*      */               
/*  974 */               if (((toEqual) || (selectedImages.size() == 0)) && 
/*  975 */                 (selectedImages.size() < this.maxSelectNum)) {
/*  976 */                 selectedImages.add(media);
/*  977 */                 this.adapter.bindSelectImages(selectedImages);
/*      */               }
/*      */               
/*  980 */               this.adapter.notifyDataSetChanged();
/*      */             }
/*      */           }
/*      */         }
/*  984 */         if (this.adapter != null)
/*      */         {
/*      */ 
/*  987 */           manualSaveFolder(media);
/*  988 */           this.tv_empty.setVisibility(this.images.size() > 0 ? 4 : 0);
/*      */         }
/*      */         
/*      */ 
/*  992 */         if (this.mimeType != PictureMimeType.ofAudio()) {
/*  993 */           int lastImageId = getLastImageId(eqVideo);
/*  994 */           if (lastImageId != -1) {
/*  995 */             removeImage(lastImageId, eqVideo);
/*      */           }
/*      */         }
/*      */         break;
/*      */       }
/* 1000 */     } else if (resultCode == 0) {
/* 1001 */       if (this.camera) {
/* 1002 */         closeActivity();
/*      */       }
/* 1004 */     } else if (resultCode == 96) {
/* 1005 */       Throwable throwable = (Throwable)data.getSerializableExtra("com.yalantis.ucrop.Error");
/* 1006 */       showToast(throwable.getMessage());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void manualSaveFolder(LocalMedia media)
/*      */   {
/*      */     try
/*      */     {
/* 1018 */       createNewFolder(this.foldersList);
/* 1019 */       LocalMediaFolder folder = getImageFolder(media.getPath(), this.foldersList);
/* 1020 */       LocalMediaFolder cameraFolder = this.foldersList.size() > 0 ? (LocalMediaFolder)this.foldersList.get(0) : null;
/* 1021 */       if ((cameraFolder != null) && (folder != null))
/*      */       {
/* 1023 */         cameraFolder.setFirstImagePath(media.getPath());
/* 1024 */         cameraFolder.setImages(this.images);
/* 1025 */         cameraFolder.setImageNum(cameraFolder.getImageNum() + 1);
/*      */         
/* 1027 */         int num = folder.getImageNum() + 1;
/* 1028 */         folder.setImageNum(num);
/* 1029 */         folder.getImages().add(0, media);
/* 1030 */         folder.setFirstImagePath(this.cameraPath);
/* 1031 */         this.folderWindow.bindFolder(this.foldersList);
/*      */       }
/*      */     } catch (Exception e) {
/* 1034 */       e.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public void onBackPressed()
/*      */   {
/* 1041 */     super.onBackPressed();
/* 1042 */     closeActivity();
/*      */   }
/*      */   
/*      */   protected void onDestroy()
/*      */   {
/* 1047 */     super.onDestroy();
/* 1048 */     if (RxBus.getDefault().isRegistered(this)) {
/* 1049 */       RxBus.getDefault().unregister(this);
/*      */     }
/* 1051 */     ImagesObservable.getInstance().clearLocalMedia();
/* 1052 */     if (this.animation != null) {
/* 1053 */       this.animation.cancel();
/* 1054 */       this.animation = null;
/*      */     }
/* 1056 */     if ((this.mediaPlayer != null) && (this.handler != null)) {
/* 1057 */       this.handler.removeCallbacks(this.runnable);
/* 1058 */       this.mediaPlayer.release();
/* 1059 */       this.mediaPlayer = null;
/*      */     }
/*      */   }
/*      */   
/*      */   public void onItemClick(int position)
/*      */   {
/* 1065 */     switch (position)
/*      */     {
/*      */     case 0: 
/* 1068 */       startOpenCamera();
/* 1069 */       break;
/*      */     
/*      */     case 1: 
/* 1072 */       startOpenCameraVideo();
/*      */     }
/*      */   }
/*      */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\PictureSelectorActivity.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */