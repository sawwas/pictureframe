/*     */ package com.luck.picture.lib.widget;
/*     */ 
/*     */ import android.content.Context;
/*     */ import android.graphics.drawable.ColorDrawable;
/*     */ import android.os.Build.VERSION;
/*     */ import android.os.Handler;
/*     */ import android.view.LayoutInflater;
/*     */ import android.view.View;
/*     */ import android.view.View.OnClickListener;
/*     */ import android.view.animation.Animation;
/*     */ import android.view.animation.Animation.AnimationListener;
/*     */ import android.view.animation.AnimationUtils;
/*     */ import android.widget.FrameLayout;
/*     */ import android.widget.LinearLayout;
/*     */ import android.widget.PopupWindow;
/*     */ import android.widget.TextView;
/*     */ import com.luck.picture.lib.R.anim;
/*     */ import com.luck.picture.lib.R.id;
/*     */ import com.luck.picture.lib.R.layout;
/*     */ 
/*     */ public class PhotoPopupWindow
/*     */   extends PopupWindow
/*     */   implements View.OnClickListener
/*     */ {
/*     */   private TextView picture_tv_photo;
/*     */   private TextView picture_tv_video;
/*     */   private TextView picture_tv_cancel;
/*     */   private LinearLayout ll_root;
/*     */   private FrameLayout fl_content;
/*     */   private Animation animationIn;
/*     */   private Animation animationOut;
/*  32 */   private boolean isDismiss = false;
/*     */   private OnItemClickListener onItemClickListener;
/*     */   
/*  35 */   public PhotoPopupWindow(Context context) { super(context);
/*  36 */     View inflate = LayoutInflater.from(context).inflate(R.layout.picture_camera_pop_layout, null);
/*  37 */     setWidth(-1);
/*  38 */     setHeight(-1);
/*  39 */     setBackgroundDrawable(new ColorDrawable());
/*  40 */     setFocusable(true);
/*  41 */     setOutsideTouchable(true);
/*  42 */     update();
/*  43 */     setBackgroundDrawable(new ColorDrawable());
/*  44 */     setContentView(inflate);
/*  45 */     this.animationIn = AnimationUtils.loadAnimation(context, R.anim.up_in);
/*  46 */     this.animationOut = AnimationUtils.loadAnimation(context, R.anim.down_out);
/*  47 */     this.ll_root = ((LinearLayout)inflate.findViewById(R.id.ll_root));
/*  48 */     this.fl_content = ((FrameLayout)inflate.findViewById(R.id.fl_content));
/*  49 */     this.picture_tv_photo = ((TextView)inflate.findViewById(R.id.picture_tv_photo));
/*  50 */     this.picture_tv_cancel = ((TextView)inflate.findViewById(R.id.picture_tv_cancel));
/*  51 */     this.picture_tv_video = ((TextView)inflate.findViewById(R.id.picture_tv_video));
/*  52 */     this.picture_tv_video.setOnClickListener(this);
/*  53 */     this.picture_tv_cancel.setOnClickListener(this);
/*  54 */     this.picture_tv_photo.setOnClickListener(this);
/*  55 */     this.fl_content.setOnClickListener(this);
/*     */   }
/*     */   
/*     */   public void showAsDropDown(View parent)
/*     */   {
/*     */     try {
/*  61 */       if (Build.VERSION.SDK_INT >= 24) {
/*  62 */         int[] location = new int[2];
/*  63 */         parent.getLocationOnScreen(location);
/*  64 */         int x = location[0];
/*  65 */         int y = location[1] + parent.getHeight();
/*  66 */         showAtLocation(parent, 80, x, y);
/*     */       } else {
/*  68 */         showAtLocation(parent, 80, 0, 0);
/*     */       }
/*     */       
/*  71 */       this.isDismiss = false;
/*  72 */       this.ll_root.startAnimation(this.animationIn);
/*     */     } catch (Exception e) {
/*  74 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */   public void dismiss()
/*     */   {
/*  80 */     if (this.isDismiss) {
/*  81 */       return;
/*     */     }
/*  83 */     this.isDismiss = true;
/*  84 */     this.ll_root.startAnimation(this.animationOut);
/*  85 */     dismiss();
/*  86 */     this.animationOut.setAnimationListener(new Animation.AnimationListener()
/*     */     {
/*     */       public void onAnimationStart(Animation animation) {}
/*     */       
/*     */ 
/*     */       public void onAnimationEnd(Animation animation)
/*     */       {
/*  93 */         PhotoPopupWindow.this.isDismiss = false;
/*  94 */         if (Build.VERSION.SDK_INT <= 16) {
/*  95 */           PhotoPopupWindow.this.dismiss4Pop();
/*     */         } else {
/*  97 */           PhotoPopupWindow.this.dismiss();
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */       public void onAnimationRepeat(Animation animation) {}
/*     */     });
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void dismiss4Pop()
/*     */   {
/* 111 */     new Handler().post(new Runnable()
/*     */     {
/*     */       public void run() {
/* 114 */         PhotoPopupWindow.this.dismiss();
/*     */       }
/*     */     });
/*     */   }
/*     */   
/*     */   public void onClick(View v)
/*     */   {
/* 121 */     int id = v.getId();
/* 122 */     if ((id == R.id.picture_tv_photo) && 
/* 123 */       (this.onItemClickListener != null)) {
/* 124 */       this.onItemClickListener.onItemClick(0);
/* 125 */       super.dismiss();
/*     */     }
/*     */     
/* 128 */     if ((id == R.id.picture_tv_video) && 
/* 129 */       (this.onItemClickListener != null)) {
/* 130 */       this.onItemClickListener.onItemClick(1);
/* 131 */       super.dismiss();
/*     */     }
/*     */     
/* 134 */     dismiss();
/*     */   }
/*     */   
/*     */ 
/*     */   public void setOnItemClickListener(OnItemClickListener onItemClickListener)
/*     */   {
/* 140 */     this.onItemClickListener = onItemClickListener;
/*     */   }
/*     */   
/*     */   public static abstract interface OnItemClickListener
/*     */   {
/*     */     public abstract void onItemClick(int paramInt);
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\widget\PhotoPopupWindow.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */