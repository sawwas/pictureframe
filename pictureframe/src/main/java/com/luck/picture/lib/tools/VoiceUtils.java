/*    */ package com.luck.picture.lib.tools;
/*    */ 
/*    */ import android.content.Context;
/*    */ import android.media.SoundPool;
/*    */ import com.luck.picture.lib.R.raw;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class VoiceUtils
/*    */ {
/*    */   private static SoundPool soundPool;
/*    */   private static int soundID;
/*    */   
/*    */   public static void playVoice(Context mContext, boolean enableVoice)
/*    */   {
/* 25 */     if (soundPool == null) {
/* 26 */       soundPool = new SoundPool(1, 4, 0);
/* 27 */       soundID = soundPool.load(mContext, R.raw.music, 1);
/*    */     }
/* 29 */     if (enableVoice) {
/* 30 */       soundPool.play(soundID, 0.1F, 0.5F, 0, 1, 1.0F);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static void release()
/*    */   {
/* 45 */     if (soundPool != null) {
/* 46 */       soundPool.stop(soundID);
/*    */     }
/* 48 */     soundPool = null;
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\tools\VoiceUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */