package com.luck.picture.lib.compress;

import com.luck.picture.lib.entity.LocalMedia;
import java.util.List;

public abstract interface CompressInterface
{
  public abstract void compress();

  public static abstract interface CompressListener
  {
    public abstract void onCompressSuccess(List<LocalMedia> paramList);

    public abstract void onCompressError(List<LocalMedia> paramList, String paramString);
  }
}
