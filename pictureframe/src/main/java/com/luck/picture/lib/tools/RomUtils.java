/*     */ package com.luck.picture.lib.tools;
/*     */ 
/*     */ import android.os.Build;
/*     */ import android.os.Build.VERSION;
/*     */ import android.text.TextUtils;
/*     */ import java.io.BufferedReader;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStreamReader;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RomUtils
/*     */ {
/*     */   public static boolean isLightStatusBarAvailable()
/*     */   {
/*  27 */     if ((isMIUIV6OrAbove()) || (isFlymeV4OrAbove()) || (isAndroidMOrAbove())) {
/*  28 */       return true;
/*     */     }
/*  30 */     return false;
/*     */   }
/*     */   
/*     */   public static int getLightStatausBarAvailableRomType() {
/*  34 */     if (isMIUIV6OrAbove()) {
/*  35 */       return 1;
/*     */     }
/*     */     
/*  38 */     if (isFlymeV4OrAbove()) {
/*  39 */       return 2;
/*     */     }
/*     */     
/*  42 */     if (isAndroidMOrAbove()) {
/*  43 */       return 3;
/*     */     }
/*     */     
/*  46 */     return 4;
/*     */   }
/*     */   
/*     */ 
/*     */   private static boolean isFlymeV4OrAbove()
/*     */   {
/*  52 */     String displayId = Build.DISPLAY;
/*  53 */     if ((!TextUtils.isEmpty(displayId)) && (displayId.contains("Flyme"))) {
/*  54 */       String[] displayIdArray = displayId.split(" ");
/*  55 */       for (String temp : displayIdArray)
/*     */       {
/*  57 */         if (temp.matches("^[4-9]\\.(\\d+\\.)+\\S*")) {
/*  58 */           return true;
/*     */         }
/*     */       }
/*     */     }
/*  62 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */   private static boolean isMIUIV6OrAbove()
/*     */   {
/*  68 */     String miuiVersionCodeStr = getSystemProperty("ro.miui.ui.version.code");
/*  69 */     if (!TextUtils.isEmpty(miuiVersionCodeStr)) {
/*     */       try {
/*  71 */         int miuiVersionCode = Integer.parseInt(miuiVersionCodeStr);
/*  72 */         if (miuiVersionCode >= 4) {
/*  73 */           return true;
/*     */         }
/*     */       } catch (Exception localException) {}
/*     */     }
/*  77 */     return false;
/*     */   }
/*     */   
/*     */   private static boolean isAndroidMOrAbove()
/*     */   {
/*  82 */     if (Build.VERSION.SDK_INT >= 23) {
/*  83 */       return true;
/*     */     }
/*  85 */     return false;
/*     */   }
/*     */   
/*     */   private static String getSystemProperty(String propName)
/*     */   {
/*  90 */     BufferedReader input = null;
/*     */     try {
/*  92 */       Process p = Runtime.getRuntime().exec("getprop " + propName);
/*  93 */       input = new BufferedReader(new InputStreamReader(p.getInputStream()), 1024);
/*  94 */       String line = input.readLine();
/*  95 */       input.close();
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       String line;
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 106 */       return line;
/*     */     }
/*     */     catch (IOException ex)
/*     */     {
/*  97 */       return null;
/*     */     } finally {
/*  99 */       if (input != null) {
/*     */         try {
/* 101 */           input.close();
/*     */         }
/*     */         catch (IOException localIOException3) {}
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   class AvailableRomType
/*     */   {
/*     */     public static final int MIUI = 1;
/*     */     public static final int FLYME = 2;
/*     */     public static final int ANDROID_NATIVE = 3;
/*     */     public static final int NA = 4;
/*     */     
/*     */     AvailableRomType() {}
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\tools\RomUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */