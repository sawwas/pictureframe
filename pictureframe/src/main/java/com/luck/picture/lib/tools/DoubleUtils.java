/*    */ package com.luck.picture.lib.tools;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class DoubleUtils
/*    */ {
/*    */   private static long lastClickTime;
/*    */   
/*    */ 
/*    */ 
/*    */   private static final long TIME = 800L;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public static boolean isFastDoubleClick()
/*    */   {
/* 19 */     long time = System.currentTimeMillis();
/* 20 */     if (time - lastClickTime < 800L) {
/* 21 */       return true;
/*    */     }
/* 23 */     lastClickTime = time;
/* 24 */     return false;
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\tools\DoubleUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */