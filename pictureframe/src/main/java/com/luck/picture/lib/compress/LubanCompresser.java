/*     */ package com.luck.picture.lib.compress;
/*     */ 
/*     */ import android.graphics.Bitmap;
/*     */ import android.graphics.Bitmap.CompressFormat;
/*     */ import android.graphics.BitmapFactory;
/*     */ import android.graphics.BitmapFactory.Options;
/*     */ import android.graphics.Matrix;
/*     */ import android.media.ExifInterface;
/*     */ import android.support.annotation.NonNull;
/*     */ import com.luck.picture.lib.config.PictureMimeType;
/*     */ import io.reactivex.Observable;
/*     */ import io.reactivex.functions.Function;
/*     */ import io.reactivex.schedulers.Schedulers;
/*     */ import java.io.ByteArrayOutputStream;
/*     */ import java.io.File;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.IOException;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import java.util.concurrent.Callable;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ class LubanCompresser
/*     */ {
/*     */   private static final String TAG = "Luban Compress";
/*     */   private final LubanBuilder mLuban;
/*     */   private ByteArrayOutputStream mByteArrayOutputStream;
/*     */   
/*     */   LubanCompresser(LubanBuilder luban)
/*     */   {
/*  41 */     this.mLuban = luban;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   Observable<File> singleAction(final File file)
/*     */   {
/*  50 */     Observable.fromCallable(new Callable()
/*     */     {
/*     */       public File call()
/*     */         throws Exception
/*     */       {
/*  48 */         return LubanCompresser.this.compressImage(LubanCompresser.this.mLuban.gear, file);
/*     */       }
/*  50 */     }).subscribeOn(Schedulers.computation());
/*     */   }
/*     */   
/*     */   Observable<List<File>> multiAction(List<File> files) {
/*  54 */     List<Observable<File>> observables = new ArrayList(files.size());
/*  55 */     for (final File file : files) {
/*  56 */       observables.add(Observable.fromCallable(new Callable()
/*     */       {
/*     */         public File call() throws Exception {
/*  59 */           return LubanCompresser.this.compressImage(LubanCompresser.this.mLuban.gear, file);
/*     */         }
/*     */       }));
/*     */     }
/*     */     
/*  64 */     Observable.zip(observables, new Function()
/*     */     {
/*     */       public List<File> apply(Object[] objects) throws Exception {
/*  67 */         List<File> files = new ArrayList(objects.length);
/*  68 */         for (Object o : objects) {
/*  69 */           files.add((File)o);
/*     */         }
/*  71 */         return files;
/*     */       }
/*     */     });
/*     */   }
/*     */   
/*     */   private File compressImage(int gear, File file) throws IOException
/*     */   {
/*  78 */     boolean eqHttp = PictureMimeType.isHttp(file.getPath());
/*  79 */     if ((file != null) && (eqHttp)) {
/*  80 */       return file;
/*     */     }
/*  82 */     switch (gear) {
/*     */     case 3: 
/*  84 */       return thirdCompress(file);
/*     */     case 4: 
/*  86 */       return customCompress(file);
/*     */     case 1: 
/*  88 */       return firstCompress(file);
/*     */     }
/*  90 */     return file;
/*     */   }
/*     */   
/*     */   private File thirdCompress(@NonNull File file) throws IOException
/*     */   {
/*  95 */     String file_name = file.getName();
/*  96 */     String thumb = getCacheFilePath(file_name);
/*     */     
/*  98 */     String filePath = file.getAbsolutePath();
/*  99 */     int angle = getImageSpinAngle(filePath);
/* 100 */     int width = getImageSize(filePath)[0];
/* 101 */     int height = getImageSize(filePath)[1];
/* 102 */     boolean flip = width > height;
/* 103 */     int thumbW = width % 2 == 1 ? width + 1 : width;
/* 104 */     int thumbH = height % 2 == 1 ? height + 1 : height;
/*     */     
/* 106 */     width = thumbW > thumbH ? thumbH : thumbW;
/* 107 */     height = thumbW > thumbH ? thumbW : thumbH;
/*     */     
/* 109 */     double scale = width / height;
/*     */     double size;
/* 111 */     if ((scale <= 1.0D) && (scale > 0.5625D)) {
/* 112 */       if (height < 1664) {
/* 113 */         if (file.length() / 1024L < 150L) {
/* 114 */           return file;
/*     */         }
/*     */         
/* 117 */         double size = width * height / Math.pow(1664.0D, 2.0D) * 150.0D;
/* 118 */         size = size < 60.0D ? 60.0D : size;
/* 119 */       } else if ((height >= 1664) && (height < 4990)) {
/* 120 */         thumbW = width / 2;
/* 121 */         thumbH = height / 2;
/* 122 */         double size = thumbW * thumbH / Math.pow(2495.0D, 2.0D) * 300.0D;
/* 123 */         size = size < 60.0D ? 60.0D : size;
/* 124 */       } else if ((height >= 4990) && (height < 10240)) {
/* 125 */         thumbW = width / 4;
/* 126 */         thumbH = height / 4;
/* 127 */         double size = thumbW * thumbH / Math.pow(2560.0D, 2.0D) * 300.0D;
/* 128 */         size = size < 100.0D ? 100.0D : size;
/*     */       } else {
/* 130 */         int multiple = height / 1280 == 0 ? 1 : height / 1280;
/* 131 */         thumbW = width / multiple;
/* 132 */         thumbH = height / multiple;
/* 133 */         double size = thumbW * thumbH / Math.pow(2560.0D, 2.0D) * 300.0D;
/* 134 */         size = size < 100.0D ? 100.0D : size;
/*     */       }
/* 136 */     } else if ((scale <= 0.5625D) && (scale > 0.5D)) {
/* 137 */       if ((height < 1280) && (file.length() / 1024L < 200L)) {
/* 138 */         return file;
/*     */       }
/*     */       
/* 141 */       int multiple = height / 1280 == 0 ? 1 : height / 1280;
/* 142 */       thumbW = width / multiple;
/* 143 */       thumbH = height / multiple;
/* 144 */       double size = thumbW * thumbH / 3686400.0D * 400.0D;
/* 145 */       size = size < 100.0D ? 100.0D : size;
/*     */     } else {
/* 147 */       int multiple = (int)Math.ceil(height / (1280.0D / scale));
/* 148 */       thumbW = width / multiple;
/* 149 */       thumbH = height / multiple;
/* 150 */       size = thumbW * thumbH / (1280.0D * (1280.0D / scale)) * 500.0D;
/* 151 */       size = size < 100.0D ? 100.0D : size;
/*     */     }
/*     */     
/* 154 */     return compress(filePath, thumb, flip ? thumbH : thumbW, flip ? thumbW : thumbH, angle, size);
/*     */   }
/*     */   
/*     */   private File firstCompress(@NonNull File file) throws IOException
/*     */   {
/* 159 */     int minSize = 60;
/* 160 */     int longSide = 720;
/* 161 */     int shortSide = 1280;
/* 162 */     String file_name = file.getName();
/* 163 */     String thumbFilePath = getCacheFilePath(file_name);
/* 164 */     String filePath = file.getAbsolutePath();
/*     */     
/* 166 */     long size = 0L;
/* 167 */     long maxSize = file.length() / 5L;
/*     */     
/* 169 */     int angle = getImageSpinAngle(filePath);
/* 170 */     int[] imgSize = getImageSize(filePath);
/* 171 */     int width = 0;int height = 0;
/* 172 */     if (imgSize[0] <= imgSize[1]) {
/* 173 */       double scale = imgSize[0] / imgSize[1];
/* 174 */       if ((scale <= 1.0D) && (scale > 0.5625D)) {
/* 175 */         width = imgSize[0] > shortSide ? shortSide : imgSize[0];
/* 176 */         height = width * imgSize[1] / imgSize[0];
/* 177 */         size = minSize;
/* 178 */       } else if (scale <= 0.5625D) {
/* 179 */         height = imgSize[1] > longSide ? longSide : imgSize[1];
/* 180 */         width = height * imgSize[0] / imgSize[1];
/* 181 */         size = maxSize;
/*     */       }
/*     */     } else {
/* 184 */       double scale = imgSize[1] / imgSize[0];
/* 185 */       if ((scale <= 1.0D) && (scale > 0.5625D)) {
/* 186 */         height = imgSize[1] > shortSide ? shortSide : imgSize[1];
/* 187 */         width = height * imgSize[0] / imgSize[1];
/* 188 */         size = minSize;
/* 189 */       } else if (scale <= 0.5625D) {
/* 190 */         width = imgSize[0] > longSide ? longSide : imgSize[0];
/* 191 */         height = width * imgSize[1] / imgSize[0];
/* 192 */         size = maxSize;
/*     */       }
/*     */     }
/*     */     
/* 196 */     return compress(filePath, thumbFilePath, width, height, angle, size);
/*     */   }
/*     */   
/*     */   private File customCompress(@NonNull File file) throws IOException {
/* 200 */     String fileName = file.getName();
/* 201 */     String thumbFilePath = getCacheFilePath(fileName);
/* 202 */     String filePath = file.getAbsolutePath();
/*     */     
/* 204 */     int angle = getImageSpinAngle(filePath);
/*     */     
/* 206 */     long fileSize = (this.mLuban.maxSize > 0) && (this.mLuban.maxSize < file.length() / 1024L) ? this.mLuban.maxSize : file.length() / 1024L;
/*     */     
/* 208 */     int[] size = getImageSize(filePath);
/* 209 */     int width = size[0];
/* 210 */     int height = size[1];
/*     */     
/* 212 */     if ((this.mLuban.maxSize > 0) && (this.mLuban.maxSize < (float)file.length() / 1024.0F))
/*     */     {
/* 214 */       float scale = (float)Math.sqrt((float)file.length() / 1024.0F / this.mLuban.maxSize);
/* 215 */       width = (int)(width / scale);
/* 216 */       height = (int)(height / scale);
/*     */     }
/*     */     
/*     */ 
/* 220 */     if (this.mLuban.maxWidth > 0) {
/* 221 */       width = Math.min(width, this.mLuban.maxWidth);
/*     */     }
/* 223 */     if (this.mLuban.maxHeight > 0) {
/* 224 */       height = Math.min(height, this.mLuban.maxHeight);
/*     */     }
/* 226 */     float scale = Math.min(width / size[0], height / size[1]);
/* 227 */     width = (int)(size[0] * scale);
/* 228 */     height = (int)(size[1] * scale);
/*     */     
/*     */ 
/* 231 */     if ((this.mLuban.maxSize > (float)file.length() / 1024.0F) && (scale == 1.0F)) {
/* 232 */       return file;
/*     */     }
/*     */     
/* 235 */     return compress(filePath, thumbFilePath, width, height, angle, fileSize);
/*     */   }
/*     */   
/*     */   private String getCacheFilePath(String fileName) {
/* 239 */     StringBuilder name = new StringBuilder("Luban_" + System.currentTimeMillis());
/* 240 */     if (this.mLuban.compressFormat == Bitmap.CompressFormat.WEBP) {
/* 241 */       name.append(".webp");
/* 242 */     } else if (fileName.endsWith(".webp")) {
/* 243 */       name.append(".webp");
/* 244 */     } else if (fileName.endsWith(".png")) {
/* 245 */       name.append(".png");
/*     */     } else {
/* 247 */       name.append(".jpg");
/*     */     }
/* 249 */     return this.mLuban.cacheDir.getAbsolutePath() + File.separator + name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int[] getImageSize(String imagePath)
/*     */   {
/* 258 */     int[] res = new int[2];
/*     */     
/* 260 */     BitmapFactory.Options options = new BitmapFactory.Options();
/* 261 */     options.inJustDecodeBounds = true;
/* 262 */     options.inSampleSize = 1;
/* 263 */     BitmapFactory.decodeFile(imagePath, options);
/*     */     
/* 265 */     res[0] = options.outWidth;
/* 266 */     res[1] = options.outHeight;
/*     */     
/* 268 */     return res;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private Bitmap compress(String imagePath, int width, int height)
/*     */   {
/* 280 */     BitmapFactory.Options options = new BitmapFactory.Options();
/* 281 */     options.inJustDecodeBounds = true;
/* 282 */     BitmapFactory.decodeFile(imagePath, options);
/*     */     
/* 284 */     int outH = options.outHeight;
/* 285 */     int outW = options.outWidth;
/* 286 */     int inSampleSize = 1;
/*     */     
/* 288 */     while ((outH / inSampleSize > height) || (outW / inSampleSize > width)) {
/* 289 */       inSampleSize *= 2;
/*     */     }
/*     */     
/* 292 */     options.inSampleSize = inSampleSize;
/* 293 */     options.inJustDecodeBounds = false;
/*     */     
/* 295 */     return BitmapFactory.decodeFile(imagePath, options);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private int getImageSpinAngle(String path)
/*     */   {
/* 304 */     int degree = 0;
/*     */     try {
/* 306 */       ExifInterface exifInterface = new ExifInterface(path);
/* 307 */       int orientation = exifInterface.getAttributeInt("Orientation", 1);
/*     */       
/* 309 */       switch (orientation) {
/*     */       case 6: 
/* 311 */         degree = 90;
/* 312 */         break;
/*     */       case 3: 
/* 314 */         degree = 180;
/* 315 */         break;
/*     */       case 8: 
/* 317 */         degree = 270;
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 321 */       e.printStackTrace();
/* 322 */       return degree;
/*     */     }
/* 324 */     return degree;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private File compress(String largeImagePath, String thumbFilePath, int width, int height, int angle, long size)
/*     */     throws IOException
/*     */   {
/* 340 */     Bitmap thbBitmap = compress(largeImagePath, width, height);
/*     */     
/* 342 */     thbBitmap = rotatingImage(angle, thbBitmap);
/*     */     
/* 344 */     return saveImage(thumbFilePath, thbBitmap, size);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static Bitmap rotatingImage(int angle, Bitmap bitmap)
/*     */   {
/* 356 */     Matrix matrix = new Matrix();
/* 357 */     matrix.postRotate(angle);
/*     */     
/*     */ 
/* 360 */     return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private File saveImage(String filePath, Bitmap bitmap, long size)
/*     */     throws IOException
/*     */   {
/* 373 */     Preconditions.checkNotNull(bitmap, "Luban Compressbitmap cannot be null");
/*     */     
/* 375 */     File result = new File(filePath.substring(0, filePath.lastIndexOf("/")));
/*     */     
/* 377 */     if ((!result.exists()) && (!result.mkdirs())) {
/* 378 */       return null;
/*     */     }
/*     */     
/* 381 */     if (this.mByteArrayOutputStream == null)
/*     */     {
/* 383 */       this.mByteArrayOutputStream = new ByteArrayOutputStream(bitmap.getWidth() * bitmap.getHeight());
/*     */     } else {
/* 385 */       this.mByteArrayOutputStream.reset();
/*     */     }
/*     */     
/* 388 */     int options = 100;
/* 389 */     bitmap.compress(this.mLuban.compressFormat, options, this.mByteArrayOutputStream);
/*     */     
/* 391 */     while ((this.mByteArrayOutputStream.size() / 1024 > size) && (options > 6)) {
/* 392 */       this.mByteArrayOutputStream.reset();
/* 393 */       options -= 6;
/* 394 */       bitmap.compress(this.mLuban.compressFormat, options, this.mByteArrayOutputStream);
/*     */     }
/* 396 */     bitmap.recycle();
/*     */     
/* 398 */     FileOutputStream fos = new FileOutputStream(filePath);
/* 399 */     this.mByteArrayOutputStream.writeTo(fos);
/* 400 */     fos.close();
/*     */     
/* 402 */     return new File(filePath);
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\compress\LubanCompresser.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */