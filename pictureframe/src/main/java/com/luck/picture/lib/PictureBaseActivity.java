/*     */ package com.luck.picture.lib;
/*     */ 
/*     */ import com.luck.picture.lib.config.PictureSelectionConfig;
/*     */ import com.luck.picture.lib.entity.LocalMediaFolder;
/*     */ import java.util.List;
/*     */ 
/*     */ public class PictureBaseActivity extends android.support.v4.app.FragmentActivity
/*     */ {
/*     */   protected android.content.Context mContext;
/*     */   protected PictureSelectionConfig config;
/*     */   protected int spanCount;
/*     */   protected int maxSelectNum;
/*     */   protected int minSelectNum;
/*     */   protected int compressQuality;
/*     */   protected int selectionMode;
/*     */   protected int mimeType;
/*     */   protected int videoMaxSecond;
/*     */   protected int videoMinSecond;
/*     */   protected int compressMaxKB;
/*     */   protected int compressMode;
/*     */   protected int compressGrade;
/*     */   protected int compressWidth;
/*     */   protected int compressHeight;
/*     */   protected int aspect_ratio_x;
/*     */   protected int aspect_ratio_y;
/*     */   protected int recordVideoSecond;
/*     */   protected int videoQuality;
/*     */   protected int cropWidth;
/*     */   protected int cropHeight;
/*     */   protected boolean isGif;
/*     */   protected boolean isCamera;
/*     */   protected boolean enablePreview;
/*     */   protected boolean enableCrop;
/*     */   protected boolean isCompress;
/*     */   protected boolean enPreviewVideo;
/*     */   protected boolean checkNumMode;
/*     */   protected boolean openClickSound;
/*     */   protected boolean numComplete;
/*     */   protected boolean camera;
/*     */   protected boolean freeStyleCropEnabled;
/*     */   protected boolean circleDimmedLayer;
/*     */   protected boolean hideBottomControls;
/*     */   protected boolean rotateEnabled;
/*     */   protected boolean scaleEnabled;
/*     */   protected boolean previewEggs;
/*     */   protected boolean statusFont;
/*     */   protected boolean showCropFrame;
/*     */   protected boolean showCropGrid;
/*     */   protected boolean previewStatusFont;
/*     */   protected String cameraPath;
/*     */   protected String outputCameraPath;
/*     */   protected String originalPath;
/*     */   protected com.luck.picture.lib.dialog.PictureDialog dialog;
/*     */   protected com.luck.picture.lib.dialog.PictureDialog compressDialog;
/*     */   protected List<com.luck.picture.lib.entity.LocalMedia> selectionMedias;
/*     */   
/*     */   protected void onCreate(android.os.Bundle savedInstanceState)
/*     */   {
/*  59 */     if (savedInstanceState != null) {
/*  60 */       this.config = ((PictureSelectionConfig)savedInstanceState.getParcelable("PictureSelectorConfig"));
/*  61 */       this.cameraPath = savedInstanceState.getString("CameraPath");
/*  62 */       this.originalPath = savedInstanceState.getString("OriginalPath");
/*     */     }
/*     */     else {
/*  65 */       this.config = PictureSelectionConfig.getInstance();
/*     */     }
/*  67 */     int themeStyleId = this.config.themeStyleId;
/*  68 */     setTheme(themeStyleId);
/*  69 */     super.onCreate(savedInstanceState);
/*  70 */     this.mContext = this;
/*  71 */     initConfig();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void initConfig()
/*     */   {
/*  78 */     this.camera = this.config.camera;
/*  79 */     this.outputCameraPath = this.config.outputCameraPath;
/*     */     
/*  81 */     this.statusFont = com.luck.picture.lib.tools.AttrsUtils.getTypeValueBoolean(this, R.attr.picture_statusFontColor);
/*     */     
/*  83 */     this.previewStatusFont = com.luck.picture.lib.tools.AttrsUtils.getTypeValueBoolean(this, R.attr.picture_preview_statusFontColor);
/*  84 */     this.mimeType = this.config.mimeType;
/*  85 */     this.selectionMedias = this.config.selectionMedias;
/*  86 */     if (this.selectionMedias == null) {
/*  87 */       this.selectionMedias = new java.util.ArrayList();
/*     */     }
/*  89 */     this.selectionMode = this.config.selectionMode;
/*  90 */     if (this.selectionMode == 1) {
/*  91 */       this.selectionMedias = new java.util.ArrayList();
/*     */     }
/*  93 */     this.spanCount = this.config.imageSpanCount;
/*  94 */     this.isGif = this.config.isGif;
/*  95 */     this.isCamera = this.config.isCamera;
/*  96 */     this.freeStyleCropEnabled = this.config.freeStyleCropEnabled;
/*  97 */     this.maxSelectNum = this.config.maxSelectNum;
/*  98 */     this.minSelectNum = this.config.minSelectNum;
/*  99 */     this.enablePreview = this.config.enablePreview;
/* 100 */     this.enPreviewVideo = this.config.enPreviewVideo;
/*     */     
/* 102 */     this.checkNumMode = (this.config.checkNumMode = com.luck.picture.lib.tools.AttrsUtils.getTypeValueBoolean(this, R.attr.picture_style_checkNumMode));
/* 103 */     this.openClickSound = this.config.openClickSound;
/* 104 */     this.videoMaxSecond = this.config.videoMaxSecond;
/* 105 */     this.videoMinSecond = this.config.videoMinSecond;
/* 106 */     this.enableCrop = this.config.enableCrop;
/* 107 */     this.isCompress = this.config.isCompress;
/* 108 */     this.compressQuality = this.config.cropCompressQuality;
/* 109 */     this.numComplete = com.luck.picture.lib.tools.AttrsUtils.getTypeValueBoolean(this, R.attr.picture_style_numComplete);
/* 110 */     this.compressMaxKB = this.config.compressMaxkB;
/* 111 */     this.compressMode = this.config.compressMode;
/* 112 */     this.compressGrade = this.config.compressGrade;
/* 113 */     this.compressWidth = this.config.compressWidth;
/* 114 */     this.compressHeight = this.config.compressHeight;
/* 115 */     this.recordVideoSecond = this.config.recordVideoSecond;
/* 116 */     this.videoQuality = this.config.videoQuality;
/* 117 */     this.cropWidth = this.config.cropWidth;
/* 118 */     this.cropHeight = this.config.cropHeight;
/* 119 */     this.aspect_ratio_x = this.config.aspect_ratio_x;
/* 120 */     this.aspect_ratio_y = this.config.aspect_ratio_y;
/* 121 */     this.circleDimmedLayer = this.config.circleDimmedLayer;
/* 122 */     this.showCropFrame = this.config.showCropFrame;
/* 123 */     this.showCropGrid = this.config.showCropGrid;
/* 124 */     this.rotateEnabled = this.config.rotateEnabled;
/* 125 */     this.scaleEnabled = this.config.scaleEnabled;
/* 126 */     this.previewEggs = this.config.previewEggs;
/* 127 */     this.hideBottomControls = this.config.hideBottomControls;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected void onSaveInstanceState(android.os.Bundle outState)
/*     */   {
/* 134 */     outState.putString("CameraPath", this.cameraPath);
/* 135 */     outState.putString("OriginalPath", this.originalPath);
/* 136 */     outState.putParcelable("PictureSelectorConfig", this.config);
/*     */   }
/*     */   
/*     */   protected void startActivity(Class clz, android.os.Bundle bundle) {
/* 140 */     if (!com.luck.picture.lib.tools.DoubleUtils.isFastDoubleClick()) {
/* 141 */       android.content.Intent intent = new android.content.Intent();
/* 142 */       intent.setClass(this, clz);
/* 143 */       intent.putExtras(bundle);
/* 144 */       startActivity(intent);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void startActivity(Class clz, android.os.Bundle bundle, int requestCode) {
/* 149 */     if (!com.luck.picture.lib.tools.DoubleUtils.isFastDoubleClick()) {
/* 150 */       android.content.Intent intent = new android.content.Intent();
/* 151 */       intent.setClass(this, clz);
/* 152 */       intent.putExtras(bundle);
/* 153 */       startActivityForResult(intent, requestCode);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void showToast(String msg) {
/* 158 */     android.widget.Toast.makeText(this.mContext, msg, 1).show();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected void showPleaseDialog()
/*     */   {
/* 165 */     if (!isFinishing()) {
/* 166 */       dismissDialog();
/* 167 */       this.dialog = new com.luck.picture.lib.dialog.PictureDialog(this);
/* 168 */       this.dialog.show();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void dismissDialog()
/*     */   {
/*     */     try
/*     */     {
/* 177 */       if ((this.dialog != null) && (this.dialog.isShowing())) {
/* 178 */         this.dialog.dismiss();
/*     */       }
/*     */     } catch (Exception e) {
/* 181 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void showCompressDialog()
/*     */   {
/* 190 */     if (!isFinishing()) {
/* 191 */       dismissCompressDialog();
/* 192 */       this.compressDialog = new com.luck.picture.lib.dialog.PictureDialog(this);
/* 193 */       this.compressDialog.show();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void dismissCompressDialog()
/*     */   {
/*     */     try
/*     */     {
/* 202 */       if ((!isFinishing()) && (this.compressDialog != null))
/*     */       {
/* 204 */         if (this.compressDialog.isShowing())
/* 205 */           this.compressDialog.dismiss();
/*     */       }
/*     */     } catch (Exception e) {
/* 208 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void compressImage(final List<com.luck.picture.lib.entity.LocalMedia> result)
/*     */   {
/* 217 */     showCompressDialog();
/* 218 */     com.luck.picture.lib.compress.CompressConfig compress_config = com.luck.picture.lib.compress.CompressConfig.ofDefaultConfig();
/* 219 */     switch (this.compressMode)
/*     */     {
/*     */     case 2: 
/* 222 */       compress_config.enablePixelCompress(true);
/* 223 */       compress_config.enableQualityCompress(true);
/* 224 */       compress_config.setMaxSize(this.compressMaxKB);
/* 225 */       break;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     case 1: 
/* 233 */       com.luck.picture.lib.compress.LubanOptions option = new com.luck.picture.lib.compress.LubanOptions.Builder().setMaxHeight(this.compressHeight).setMaxWidth(this.compressWidth).setMaxSize(this.compressMaxKB).setGrade(this.compressGrade).create();
/* 234 */       compress_config = com.luck.picture.lib.compress.CompressConfig.ofLuban(option);
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 251 */     com.luck.picture.lib.compress.CompressImageOptions.compress(this, compress_config, result, new com.luck.picture.lib.compress.CompressInterface.CompressListener()
/*     */     {
/*     */       public void onCompressSuccess(List<com.luck.picture.lib.entity.LocalMedia> images)
/*     */       {
/* 242 */         com.luck.picture.lib.rxbus2.RxBus.getDefault().post(new com.luck.picture.lib.entity.EventEntity(2770));
/* 243 */         PictureBaseActivity.this.onResult(images);
/*     */       }
/*     */       
/*     */       public void onCompressError(List<com.luck.picture.lib.entity.LocalMedia> images, String msg)
/*     */       {
/* 248 */         com.luck.picture.lib.rxbus2.RxBus.getDefault().post(new com.luck.picture.lib.entity.EventEntity(2770));
/* 249 */         PictureBaseActivity.this.onResult(result);
/*     */       }
/*     */     })
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 251 */       .compress();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void startCrop(String originalPath)
/*     */   {
/* 260 */     com.yalantis.ucrop.UCrop.Options options = new com.yalantis.ucrop.UCrop.Options();
/* 261 */     int toolbarColor = com.luck.picture.lib.tools.AttrsUtils.getTypeValueColor(this, R.attr.picture_crop_toolbar_bg);
/* 262 */     int statusColor = com.luck.picture.lib.tools.AttrsUtils.getTypeValueColor(this, R.attr.picture_crop_status_color);
/* 263 */     int titleColor = com.luck.picture.lib.tools.AttrsUtils.getTypeValueColor(this, R.attr.picture_crop_title_color);
/* 264 */     options.setToolbarColor(toolbarColor);
/* 265 */     options.setStatusBarColor(statusColor);
/* 266 */     options.setToolbarWidgetColor(titleColor);
/* 267 */     options.setCircleDimmedLayer(this.circleDimmedLayer);
/* 268 */     options.setShowCropFrame(this.showCropFrame);
/* 269 */     options.setShowCropGrid(this.showCropGrid);
/* 270 */     options.setCompressionQuality(this.compressQuality);
/* 271 */     options.setHideBottomControls(this.hideBottomControls);
/* 272 */     options.setFreeStyleCropEnabled(this.freeStyleCropEnabled);
/* 273 */     boolean isHttp = com.luck.picture.lib.config.PictureMimeType.isHttp(originalPath);
/* 274 */     android.net.Uri uri = isHttp ? android.net.Uri.parse(originalPath) : android.net.Uri.fromFile(new java.io.File(originalPath));
/* 275 */     com.yalantis.ucrop.UCrop.of(uri, android.net.Uri.fromFile(new java.io.File(getCacheDir(), System.currentTimeMillis() + ".jpg")))
/* 276 */       .withAspectRatio(this.aspect_ratio_x, this.aspect_ratio_y)
/* 277 */       .withMaxResultSize(this.cropWidth, this.cropHeight)
/* 278 */       .withOptions(options)
/* 279 */       .start(this);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void startCrop(java.util.ArrayList<String> list)
/*     */   {
/* 288 */     com.yalantis.ucrop.UCropMulti.Options options = new com.yalantis.ucrop.UCropMulti.Options();
/* 289 */     int toolbarColor = com.luck.picture.lib.tools.AttrsUtils.getTypeValueColor(this, R.attr.picture_crop_toolbar_bg);
/* 290 */     int statusColor = com.luck.picture.lib.tools.AttrsUtils.getTypeValueColor(this, R.attr.picture_crop_status_color);
/* 291 */     int titleColor = com.luck.picture.lib.tools.AttrsUtils.getTypeValueColor(this, R.attr.picture_crop_title_color);
/* 292 */     options.setToolbarColor(toolbarColor);
/* 293 */     options.setStatusBarColor(statusColor);
/* 294 */     options.setToolbarWidgetColor(titleColor);
/* 295 */     options.setCircleDimmedLayer(this.circleDimmedLayer);
/* 296 */     options.setShowCropFrame(this.showCropFrame);
/* 297 */     options.setShowCropGrid(this.showCropGrid);
/* 298 */     options.setScaleEnabled(this.scaleEnabled);
/* 299 */     options.setRotateEnabled(this.rotateEnabled);
/* 300 */     options.setHideBottomControls(true);
/* 301 */     options.setCompressionQuality(this.compressQuality);
/* 302 */     options.setCutListData(list);
/* 303 */     options.setFreeStyleCropEnabled(this.freeStyleCropEnabled);
/* 304 */     String path = list.size() > 0 ? (String)list.get(0) : "";
/* 305 */     boolean isHttp = com.luck.picture.lib.config.PictureMimeType.isHttp(path);
/* 306 */     android.net.Uri uri = isHttp ? android.net.Uri.parse(path) : android.net.Uri.fromFile(new java.io.File(path));
/* 307 */     com.yalantis.ucrop.UCropMulti.of(uri, android.net.Uri.fromFile(new java.io.File(getCacheDir(), System.currentTimeMillis() + ".jpg")))
/* 308 */       .withAspectRatio(this.aspect_ratio_x, this.aspect_ratio_y)
/* 309 */       .withMaxResultSize(this.cropWidth, this.cropHeight)
/* 310 */       .withOptions(options)
/* 311 */       .start(this);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void rotateImage(int degree, java.io.File file)
/*     */   {
/* 321 */     if (degree > 0) {
/*     */       try
/*     */       {
/* 324 */         android.graphics.BitmapFactory.Options opts = new android.graphics.BitmapFactory.Options();
/* 325 */         opts.inSampleSize = 2;
/* 326 */         android.graphics.Bitmap bitmap = android.graphics.BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
/* 327 */         android.graphics.Bitmap bmp = com.luck.picture.lib.tools.PictureFileUtils.rotaingImageView(degree, bitmap);
/* 328 */         com.luck.picture.lib.tools.PictureFileUtils.saveBitmapFile(bmp, file);
/*     */       } catch (Exception e) {
/* 330 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void handlerResult(List<com.luck.picture.lib.entity.LocalMedia> result)
/*     */   {
/* 342 */     if (this.isCompress) {
/* 343 */       compressImage(result);
/*     */     } else {
/* 345 */       onResult(result);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void createNewFolder(List<LocalMediaFolder> folders)
/*     */   {
/* 356 */     if (folders.size() == 0)
/*     */     {
/* 358 */       LocalMediaFolder newFolder = new LocalMediaFolder();
/*     */       
/* 360 */       String folderName = this.mimeType == com.luck.picture.lib.config.PictureMimeType.ofAudio() ? getString(R.string.picture_all_audio) : getString(R.string.picture_camera_roll);
/* 361 */       newFolder.setName(folderName);
/* 362 */       newFolder.setPath("");
/* 363 */       newFolder.setFirstImagePath("");
/* 364 */       folders.add(newFolder);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected LocalMediaFolder getImageFolder(String path, List<LocalMediaFolder> imageFolders)
/*     */   {
/* 376 */     java.io.File imageFile = new java.io.File(path);
/* 377 */     java.io.File folderFile = imageFile.getParentFile();
/*     */     
/* 379 */     for (LocalMediaFolder folder : imageFolders) {
/* 380 */       if (folder.getName().equals(folderFile.getName())) {
/* 381 */         return folder;
/*     */       }
/*     */     }
/* 384 */     LocalMediaFolder newFolder = new LocalMediaFolder();
/* 385 */     newFolder.setName(folderFile.getName());
/* 386 */     newFolder.setPath(folderFile.getAbsolutePath());
/* 387 */     newFolder.setFirstImagePath(path);
/* 388 */     imageFolders.add(newFolder);
/* 389 */     return newFolder;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void onResult(List<com.luck.picture.lib.entity.LocalMedia> images)
/*     */   {
/* 398 */     dismissCompressDialog();
/* 399 */     if ((this.camera) && (this.selectionMode == 2) && (this.selectionMedias != null))
/*     */     {
/*     */ 
/* 402 */       images.addAll(this.selectionMedias);
/*     */     }
/* 404 */     android.content.Intent intent = PictureSelector.putIntentResult(images);
/* 405 */     setResult(-1, intent);
/* 406 */     closeActivity();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected void closeActivity()
/*     */   {
/* 413 */     finish();
/* 414 */     overridePendingTransition(0, R.anim.a3);
/*     */   }
/*     */   
/*     */   protected void onDestroy()
/*     */   {
/* 419 */     super.onDestroy();
/* 420 */     dismissCompressDialog();
/* 421 */     dismissDialog();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected int getLastImageId(boolean eqVideo)
/*     */   {
/*     */     try
/*     */     {
/* 433 */       String absolutePath = com.luck.picture.lib.tools.PictureFileUtils.getDCIMCameraPath();
/* 434 */       String ORDER_BY = "_id DESC";
/* 435 */       String selection = eqVideo ? "_data like ?" : "_data like ?";
/*     */       
/*     */ 
/* 438 */       String[] selectionArgs = { absolutePath + "%" };
/* 439 */       android.database.Cursor imageCursor = getContentResolver().query(eqVideo ? android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI : android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, selection, selectionArgs, ORDER_BY);
/*     */       
/*     */ 
/*     */ 
/* 443 */       if (imageCursor.moveToFirst()) {
/* 444 */         int id = imageCursor.getInt(eqVideo ? imageCursor
/* 445 */           .getColumnIndex("_id") : imageCursor
/* 446 */           .getColumnIndex("_id"));
/* 447 */         long date = imageCursor.getLong(eqVideo ? imageCursor
/* 448 */           .getColumnIndex("duration") : imageCursor
/* 449 */           .getColumnIndex("date_added"));
/* 450 */         int duration = com.luck.picture.lib.tools.DateUtils.dateDiffer(date);
/* 451 */         imageCursor.close();
/*     */         
/* 453 */         return duration <= 30 ? id : -1;
/*     */       }
/* 455 */       return -1;
/*     */     }
/*     */     catch (Exception e) {
/* 458 */       e.printStackTrace(); }
/* 459 */     return -1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void removeImage(int id, boolean eqVideo)
/*     */   {
/*     */     try
/*     */     {
/* 471 */       android.content.ContentResolver cr = getContentResolver();
/* 472 */       android.net.Uri uri = eqVideo ? android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI : android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
/*     */       
/* 474 */       String selection = eqVideo ? "_id=?" : "_id=?";
/*     */       
/* 476 */       cr.delete(uri, selection, new String[] {
/*     */       
/* 478 */         Long.toString(id) });
/*     */     } catch (Exception e) {
/* 480 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void isAudio(android.content.Intent data)
/*     */   {
/* 490 */     if ((data != null) && (this.mimeType == com.luck.picture.lib.config.PictureMimeType.ofAudio())) {
/*     */       try {
/* 492 */         android.net.Uri uri = data.getData();
/*     */         String audioPath;
/* 494 */         String audioPath; if (android.os.Build.VERSION.SDK_INT <= 19) {
/* 495 */           audioPath = uri.getPath();
/*     */         } else {
/* 497 */           audioPath = getAudioFilePathFromUri(uri);
/*     */         }
/* 499 */         com.luck.picture.lib.tools.PictureFileUtils.copyAudioFile(audioPath, this.cameraPath);
/*     */       } catch (Exception e) {
/* 501 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected String getAudioFilePathFromUri(android.net.Uri uri)
/*     */   {
/* 513 */     String path = "";
/*     */     try
/*     */     {
/* 516 */       android.database.Cursor cursor = getContentResolver().query(uri, null, null, null, null);
/* 517 */       cursor.moveToFirst();
/* 518 */       int index = cursor.getColumnIndex("_data");
/* 519 */       path = cursor.getString(index);
/*     */     } catch (Exception e) {
/* 521 */       e.printStackTrace();
/*     */     }
/* 523 */     return path;
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\PictureBaseActivity.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */