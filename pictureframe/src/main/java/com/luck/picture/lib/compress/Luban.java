package com.luck.picture.lib.compress;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Log;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.File;
import java.util.Collections;
import java.util.List;

public class Luban
{
    public static final int FIRST_GEAR = 1;
    public static final int THIRD_GEAR = 3;
    public static final int CUSTOM_GEAR = 4;
    private static final String TAG = "Luban";
    private static String DEFAULT_DISK_CACHE_DIR = "luban_disk_cache";
    private File mFile;
    private List<File> mFileList;
    private LubanBuilder mBuilder;

    private Luban(File cacheDir)
    {
        this.mBuilder = new LubanBuilder(cacheDir);
    }

    public static Luban compress(Context context, File file)
    {
        Luban luban = new Luban(getPhotoCacheDir(context));
        luban.mFile = file;
        luban.mFileList = Collections.singletonList(file);

        return luban;
    }

    public static Luban compress(Context context, List<File> files)
    {
        Luban luban = new Luban(getPhotoCacheDir(context));
        luban.mFileList = files;
        luban.mFile = ((File)files.get(0));
        return luban;
    }

    public Luban putGear(int gear)
    {
        this.mBuilder.gear = gear;
        return this;
    }

    public Luban setCompressFormat(Bitmap.CompressFormat compressFormat)
    {
        this.mBuilder.compressFormat = compressFormat;
        return this;
    }

    public Luban setMaxSize(int size)
    {
        this.mBuilder.maxSize = size;
        return this;
    }

    public Luban setMaxWidth(int width)
    {
        this.mBuilder.maxWidth = width;
        return this;
    }

    public Luban setMaxHeight(int height)
    {
        this.mBuilder.maxHeight = height;
        return this;
    }

    public void launch(final OnCompressListener listener)
    {
        asObservable().subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer()
        {
            public void onSubscribe(Disposable d)
            {
                listener.onStart();
            }

            @Override
            public void onNext(Object o) {

            }

            public void onNext(File file)
            {
                listener.onSuccess(file);
            }

            public void onError(Throwable e)
            {
                listener.onError(e);
            }

            public void onComplete() {}
        });
    }

    public void launch(final OnMultiCompressListener listener)
    {
        asListObservable().subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer()
        {
            public void onSubscribe(Disposable d)
            {
                listener.onStart();
            }

            public void onNext(List<File> files)
            {
                listener.onSuccess(files);
            }

            public void onError(Throwable e)
            {
                listener.onError(e);
            }

            public void onComplete() {}
        });
    }

    public Observable<File> asObservable()
    {
        LubanCompresser compresser = new LubanCompresser(this.mBuilder);
        return compresser.singleAction(this.mFile);
    }

    public Observable<List<File>> asListObservable()
    {
        LubanCompresser compresser = new LubanCompresser(this.mBuilder);
        return compresser.multiAction(this.mFileList);
    }

    private static File getPhotoCacheDir(Context context)
    {
        return getPhotoCacheDir(context, DEFAULT_DISK_CACHE_DIR);
    }

    private static File getPhotoCacheDir(Context context, String cacheName)
    {
        File cacheDir = context.getCacheDir();
        if (cacheDir != null)
        {
            File result = new File(cacheDir, cacheName);
            if ((!result.mkdirs()) && ((!result.exists()) || (!result.isDirectory()))) {
                return null;
            }
            return result;
        }
        if (Log.isLoggable("Luban", 6)) {
            Log.e("Luban", "default disk cache dir is null");
        }
        return null;
    }

    public Luban clearCache()
    {
        if (this.mBuilder.cacheDir.exists()) {
            deleteFile(this.mBuilder.cacheDir);
        }
        return this;
    }

    private void deleteFile(File fileOrDirectory)
    {
        if (fileOrDirectory.isDirectory()) {
            for (File file : fileOrDirectory.listFiles()) {
                deleteFile(file);
            }
        }
        fileOrDirectory.delete();
    }
}
