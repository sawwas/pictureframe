/*     */ package com.luck.picture.lib.config;
/*     */ 
/*     */ import android.media.MediaMetadataRetriever;
/*     */ import android.text.TextUtils;
/*     */ import com.luck.picture.lib.tools.DebugUtil;
/*     */ import java.io.File;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class PictureMimeType
/*     */ {
/*     */   public static int ofAll()
/*     */   {
/*  21 */     return 0;
/*     */   }
/*     */   
/*     */   public static int ofImage() {
/*  25 */     return 1;
/*     */   }
/*     */   
/*     */   public static int ofVideo() {
/*  29 */     return 2;
/*     */   }
/*     */   
/*     */   public static int ofAudio() {
/*  33 */     return 3;
/*     */   }
/*     */   
/*     */   public static int isPictureType(String pictureType) {
/*  37 */     switch (pictureType) {
/*     */     case "image/png": 
/*     */     case "image/PNG": 
/*     */     case "image/jpeg": 
/*     */     case "image/JPEG": 
/*     */     case "image/webp": 
/*     */     case "image/WEBP": 
/*     */     case "image/gif": 
/*     */     case "image/GIF": 
/*     */     case "imagex-ms-bmp": 
/*  47 */       return 1;
/*     */     case "video/3gp": 
/*     */     case "video/3gpp": 
/*     */     case "video/3gpp2": 
/*     */     case "video/avi": 
/*     */     case "video/mp4": 
/*     */     case "video/quicktime": 
/*     */     case "video/x-msvideo": 
/*     */     case "video/x-matroska": 
/*     */     case "video/mpeg": 
/*     */     case "video/webm": 
/*     */     case "video/mp2ts": 
/*  59 */       return 2;
/*     */     case "audio/mpeg": 
/*     */     case "audio/x-ms-wma": 
/*     */     case "audio/x-wav": 
/*     */     case "audio/amr": 
/*     */     case "audio/wav": 
/*     */     case "audio/aac": 
/*     */     case "audio/mp4": 
/*     */     case "audio/quicktime": 
/*     */     case "audio/3gpp": 
/*  69 */       return 3;
/*     */     }
/*  71 */     return 1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isGif(String pictureType)
/*     */   {
/*  81 */     switch (pictureType) {
/*     */     case "image/gif": 
/*     */     case "image/GIF": 
/*  84 */       return true;
/*     */     }
/*  86 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isImageGif(String path)
/*     */   {
/*  96 */     if (!TextUtils.isEmpty(path)) {
/*  97 */       int lastIndex = path.lastIndexOf(".");
/*  98 */       String pictureType = path.substring(lastIndex, path.length());
/*     */       
/* 100 */       return (pictureType.startsWith(".gif")) || (pictureType.startsWith(".GIF"));
/*     */     }
/* 102 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isVideo(String pictureType)
/*     */   {
/* 112 */     switch (pictureType) {
/*     */     case "video/3gp": 
/*     */     case "video/3gpp": 
/*     */     case "video/3gpp2": 
/*     */     case "video/avi": 
/*     */     case "video/mp4": 
/*     */     case "video/quicktime": 
/*     */     case "video/x-msvideo": 
/*     */     case "video/x-matroska": 
/*     */     case "video/mpeg": 
/*     */     case "video/webm": 
/*     */     case "video/mp2ts": 
/* 124 */       return true;
/*     */     }
/* 126 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isHttp(String path)
/*     */   {
/* 136 */     if ((!TextUtils.isEmpty(path)) && (
/* 137 */       (path.startsWith("http")) || 
/* 138 */       (path.startsWith("https")))) {
/* 139 */       return true;
/*     */     }
/*     */     
/* 142 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String fileToType(File file)
/*     */   {
/* 152 */     if (file != null) {
/* 153 */       String name = file.getName();
/* 154 */       DebugUtil.i("**** fileToType:", name);
/* 155 */       if ((name.endsWith(".mp4")) || (name.endsWith(".avi")) || 
/* 156 */         (name.endsWith(".3gpp")) || (name.endsWith(".3gp")) || (name.startsWith(".mov")))
/* 157 */         return "video/mp4";
/* 158 */       if ((name.endsWith(".PNG")) || (name.endsWith(".png")) || (name.endsWith(".jpeg")) || 
/* 159 */         (name.endsWith(".gif")) || (name.endsWith(".GIF")) || (name.endsWith(".jpg")) || 
/* 160 */         (name.endsWith(".webp")) || (name.endsWith(".WEBP")) || (name.endsWith(".JPEG")))
/* 161 */         return "image/jpeg";
/* 162 */       if ((name.endsWith(".mp3")) || (name.endsWith(".amr")) || 
/* 163 */         (name.endsWith(".aac")) || (name.endsWith(".war")) || 
/* 164 */         (name.endsWith(".flac"))) {
/* 165 */         return "audio/mpeg";
/*     */       }
/*     */     }
/* 168 */     return "image/jpeg";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean mimeToEqual(String p1, String p2)
/*     */   {
/* 179 */     return isPictureType(p1) == isPictureType(p2);
/*     */   }
/*     */   
/*     */   public static String createImageType(String path) {
/*     */     try {
/* 184 */       if (!TextUtils.isEmpty(path)) {
/* 185 */         File file = new File(path);
/* 186 */         String fileName = file.getName();
/* 187 */         int last = fileName.lastIndexOf(".") + 1;
/* 188 */         String temp = fileName.substring(last, fileName.length());
/* 189 */         return "image/" + temp;
/*     */       }
/*     */     } catch (Exception e) {
/* 192 */       e.printStackTrace();
/* 193 */       return "image/jpeg";
/*     */     }
/* 195 */     return "image/jpeg";
/*     */   }
/*     */   
/*     */   public static String createVideoType(String path) {
/*     */     try {
/* 200 */       if (!TextUtils.isEmpty(path)) {
/* 201 */         File file = new File(path);
/* 202 */         String fileName = file.getName();
/* 203 */         int last = fileName.lastIndexOf(".") + 1;
/* 204 */         String temp = fileName.substring(last, fileName.length());
/* 205 */         return "video/" + temp;
/*     */       }
/*     */     } catch (Exception e) {
/* 208 */       e.printStackTrace();
/* 209 */       return "video/mp4";
/*     */     }
/* 211 */     return "video/mp4";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int pictureToVideo(String pictureType)
/*     */   {
/* 220 */     if (!TextUtils.isEmpty(pictureType)) {
/* 221 */       if (pictureType.startsWith("video"))
/* 222 */         return 2;
/* 223 */       if (pictureType.startsWith("audio")) {
/* 224 */         return 3;
/*     */       }
/*     */     }
/* 227 */     return 1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int getLocalVideoDuration(String videoPath)
/*     */   {
/*     */     try
/*     */     {
/* 238 */       MediaMetadataRetriever mmr = new MediaMetadataRetriever();
/* 239 */       mmr.setDataSource(videoPath);
/* 240 */       duration = Integer.parseInt(mmr
/* 241 */         .extractMetadata(9));
/*     */     } catch (Exception e) { int duration;
/* 243 */       e.printStackTrace();
/* 244 */       return 0; }
/*     */     int duration;
/* 246 */     return duration;
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\config\PictureMimeType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */