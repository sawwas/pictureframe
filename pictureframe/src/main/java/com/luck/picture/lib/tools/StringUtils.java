/*    */ package com.luck.picture.lib.tools;
/*    */ 
/*    */ import android.content.Context;
/*    */ import android.graphics.drawable.Drawable;
/*    */ import android.text.Spannable;
/*    */ import android.text.SpannableString;
/*    */ import android.text.TextUtils;
/*    */ import android.text.style.RelativeSizeSpan;
/*    */ import android.widget.TextView;
/*    */ import com.luck.picture.lib.R.string;
/*    */ import com.luck.picture.lib.config.PictureMimeType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class StringUtils
/*    */ {
/*    */   public static boolean isCamera(String title)
/*    */   {
/* 23 */     if (((!TextUtils.isEmpty(title)) && (title.startsWith("相机胶卷"))) || 
/* 24 */       (title.startsWith("CameraRoll")) || 
/* 25 */       (title.startsWith("所有音频")) || 
/* 26 */       (title.startsWith("All audio"))) {
/* 27 */       return true;
/*    */     }
/*    */     
/* 30 */     return false;
/*    */   }
/*    */   
/*    */   public static void tempTextFont(TextView tv, int mimeType) {
/* 34 */     String text = tv.getText().toString().trim();
/*    */     
/*    */ 
/* 37 */     String str = mimeType == PictureMimeType.ofAudio() ? tv.getContext().getString(R.string.picture_empty_audio_title) : tv.getContext().getString(R.string.picture_empty_title);
/* 38 */     String sumText = str + text;
/* 39 */     Spannable placeSpan = new SpannableString(sumText);
/* 40 */     placeSpan.setSpan(new RelativeSizeSpan(0.8F), str.length(), sumText.length(), 33);
/*    */     
/* 42 */     tv.setText(placeSpan);
/*    */   }
/*    */   
/*    */   public static void modifyTextViewDrawable(TextView v, Drawable drawable, int index) {
/* 46 */     drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
/*    */     
/* 48 */     if (index == 0) {
/* 49 */       v.setCompoundDrawables(drawable, null, null, null);
/* 50 */     } else if (index == 1) {
/* 51 */       v.setCompoundDrawables(null, drawable, null, null);
/* 52 */     } else if (index == 2) {
/* 53 */       v.setCompoundDrawables(null, null, drawable, null);
/*    */     } else {
/* 55 */       v.setCompoundDrawables(null, null, null, drawable);
/*    */     }
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\tools\StringUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */