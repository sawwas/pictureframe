/*    */ package com.luck.picture.lib.tools;
/*    */ 
/*    */ import java.text.SimpleDateFormat;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class DateUtils
/*    */ {
/* 14 */   private static SimpleDateFormat msFormat = new SimpleDateFormat("mm:ss");
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static String timeParse(long duration)
/*    */   {
/* 23 */     String time = "";
/* 24 */     if (duration > 1000L) {
/* 25 */       time = timeParseMinute(duration);
/*    */     } else {
/* 27 */       long minute = duration / 60000L;
/* 28 */       long seconds = duration % 60000L;
/* 29 */       long second = Math.round((float)seconds / 1000.0F);
/* 30 */       if (minute < 10L) {
/* 31 */         time = time + "0";
/*    */       }
/* 33 */       time = time + minute + ":";
/* 34 */       if (second < 10L) {
/* 35 */         time = time + "0";
/*    */       }
/* 37 */       time = time + second;
/*    */     }
/* 39 */     return time;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static String timeParseMinute(long duration)
/*    */   {
/*    */     try
/*    */     {
/* 50 */       return msFormat.format(Long.valueOf(duration));
/*    */     } catch (Exception e) {
/* 52 */       e.printStackTrace(); }
/* 53 */     return "0:00";
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static int dateDiffer(long d)
/*    */   {
/*    */     try
/*    */     {
/* 65 */       long l1 = Long.parseLong(String.valueOf(System.currentTimeMillis()).substring(0, 10));
/* 66 */       long interval = l1 - d;
/* 67 */       return (int)Math.abs(interval);
/*    */     } catch (Exception e) {
/* 69 */       e.printStackTrace(); }
/* 70 */     return -1;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static String cdTime(long sTime, long eTime)
/*    */   {
/* 82 */     long diff = eTime - sTime;
/* 83 */     return diff + "毫秒";
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\tools\DateUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */