/*    */ package com.luck.picture.lib.permissions;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Permission
/*    */ {
/*    */   public final String name;
/*    */   
/*    */ 
/*    */   public final boolean granted;
/*    */   
/*    */   public final boolean shouldShowRequestPermissionRationale;
/*    */   
/*    */ 
/*    */   Permission(String name, boolean granted)
/*    */   {
/* 17 */     this(name, granted, false);
/*    */   }
/*    */   
/*    */   Permission(String name, boolean granted, boolean shouldShowRequestPermissionRationale) {
/* 21 */     this.name = name;
/* 22 */     this.granted = granted;
/* 23 */     this.shouldShowRequestPermissionRationale = shouldShowRequestPermissionRationale;
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean equals(Object o)
/*    */   {
/* 29 */     if (this == o) return true;
/* 30 */     if ((o == null) || (getClass() != o.getClass())) { return false;
/*    */     }
/* 32 */     Permission that = (Permission)o;
/*    */     
/* 34 */     if (this.granted != that.granted) return false;
/* 35 */     if (this.shouldShowRequestPermissionRationale != that.shouldShowRequestPermissionRationale)
/* 36 */       return false;
/* 37 */     return this.name.equals(that.name);
/*    */   }
/*    */   
/*    */   public int hashCode()
/*    */   {
/* 42 */     int result = this.name.hashCode();
/* 43 */     result = 31 * result + (this.granted ? 1 : 0);
/* 44 */     result = 31 * result + (this.shouldShowRequestPermissionRationale ? 1 : 0);
/* 45 */     return result;
/*    */   }
/*    */   
/*    */   public String toString()
/*    */   {
/* 50 */     return "Permission{name='" + this.name + '\'' + ", granted=" + this.granted + ", shouldShowRequestPermissionRationale=" + this.shouldShowRequestPermissionRationale + '}';
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\permissions\Permission.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */