package com.luck.picture.lib.compress;

import java.io.Serializable;

public class CompressConfig
        implements Serializable
{
    private int maxPixel = 1200;
    private int maxSize = 102400;
    private boolean enablePixelCompress = true;
    private boolean enableQualityCompress = true;
    private boolean enableReserveRaw = true;
    private LubanOptions lubanOptions;

    public static CompressConfig ofDefaultConfig()
    {
        return new CompressConfig();
    }

    public static CompressConfig ofLuban(LubanOptions options)
    {
        return new CompressConfig(options);
    }

    private CompressConfig() {}

    private CompressConfig(LubanOptions options)
    {
        this.lubanOptions = options;
    }

    public LubanOptions getLubanOptions()
    {
        return this.lubanOptions;
    }

    public int getMaxPixel()
    {
        return this.maxPixel;
    }

    public CompressConfig setMaxPixel(int maxPixel)
    {
        this.maxPixel = maxPixel;
        return this;
    }

    public int getMaxSize()
    {
        return this.maxSize;
    }

    public void setMaxSize(int maxSize)
    {
        this.maxSize = maxSize;
    }

    public boolean isEnablePixelCompress()
    {
        return this.enablePixelCompress;
    }

    public void enablePixelCompress(boolean enablePixelCompress)
    {
        this.enablePixelCompress = enablePixelCompress;
    }

    public boolean isEnableQualityCompress()
    {
        return this.enableQualityCompress;
    }

    public void enableQualityCompress(boolean enableQualityCompress)
    {
        this.enableQualityCompress = enableQualityCompress;
    }

    public boolean isEnableReserveRaw()
    {
        return this.enableReserveRaw;
    }

    public void enableReserveRaw(boolean enableReserveRaw)
    {
        this.enableReserveRaw = enableReserveRaw;
    }

    public static class Builder
    {
        private CompressConfig config;

        public Builder()
        {
            this.config = new CompressConfig(null);
        }

        public Builder setMaxSize(int maxSize)
        {
            this.config.setMaxSize(maxSize);
            return this;
        }

        public Builder setMaxPixel(int maxPixel)
        {
            this.config.setMaxPixel(maxPixel);
            return this;
        }

        public Builder enablePixelCompress(boolean enablePixelCompress)
        {
            this.config.enablePixelCompress(enablePixelCompress);
            return this;
        }

        public Builder enableQualityCompress(boolean enableQualityCompress)
        {
            this.config.enableQualityCompress(enableQualityCompress);
            return this;
        }

        public Builder enableReserveRaw(boolean enableReserveRaw)
        {
            this.config.enableReserveRaw(enableReserveRaw);
            return this;
        }

        public CompressConfig create()
        {
            return this.config;
        }
    }
}
