/*    */ package com.luck.picture.lib.dialog;
/*    */ 
/*    */ import android.content.Context;
/*    */ import android.graphics.Canvas;
/*    */ import android.util.AttributeSet;
/*    */ import android.widget.ImageView;
/*    */ import com.luck.picture.lib.R.drawable;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class PictureSpinView
/*    */   extends ImageView
/*    */   implements PictureIndeterminate
/*    */ {
/*    */   private float mRotateDegrees;
/*    */   private int mFrameTime;
/*    */   private boolean mNeedToUpdateView;
/*    */   private Runnable mUpdateViewRunnable;
/*    */   
/*    */   public PictureSpinView(Context context)
/*    */   {
/* 34 */     super(context);
/* 35 */     init();
/*    */   }
/*    */   
/*    */   public PictureSpinView(Context context, AttributeSet attrs) {
/* 39 */     super(context, attrs);
/* 40 */     init();
/*    */   }
/*    */   
/*    */   private void init() {
/* 44 */     setImageResource(R.drawable.kprogresshud_spinner);
/* 45 */     this.mFrameTime = 83;
/* 46 */     this.mUpdateViewRunnable = new Runnable()
/*    */     {
/*    */       public void run() {
/* 49 */         PictureSpinView.this.mRotateDegrees = (PictureSpinView.this.mRotateDegrees + 30.0F);
/* 50 */         PictureSpinView.this.mRotateDegrees = (PictureSpinView.this.mRotateDegrees < 360.0F ? PictureSpinView.this.mRotateDegrees : PictureSpinView.this.mRotateDegrees - 360.0F);
/* 51 */         PictureSpinView.this.invalidate();
/* 52 */         if (PictureSpinView.this.mNeedToUpdateView) {
/* 53 */           PictureSpinView.this.postDelayed(this, PictureSpinView.this.mFrameTime);
/*    */         }
/*    */       }
/*    */     };
/*    */   }
/*    */   
/*    */   public void setAnimationSpeed(float scale)
/*    */   {
/* 61 */     this.mFrameTime = ((int)(83.0F / scale));
/*    */   }
/*    */   
/*    */   protected void onDraw(Canvas canvas)
/*    */   {
/* 66 */     canvas.rotate(this.mRotateDegrees, getWidth() / 2, getHeight() / 2);
/* 67 */     super.onDraw(canvas);
/*    */   }
/*    */   
/*    */   protected void onAttachedToWindow()
/*    */   {
/* 72 */     super.onAttachedToWindow();
/* 73 */     this.mNeedToUpdateView = true;
/* 74 */     post(this.mUpdateViewRunnable);
/*    */   }
/*    */   
/*    */   protected void onDetachedFromWindow()
/*    */   {
/* 79 */     this.mNeedToUpdateView = false;
/* 80 */     super.onDetachedFromWindow();
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\dialog\PictureSpinView.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */