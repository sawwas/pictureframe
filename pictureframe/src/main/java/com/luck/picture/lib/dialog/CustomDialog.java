/*    */ package com.luck.picture.lib.dialog;
/*    */ 
/*    */ import android.app.Dialog;
/*    */ import android.content.Context;
/*    */ import android.content.res.Resources;
/*    */ import android.util.DisplayMetrics;
/*    */ import android.view.Window;
/*    */ import android.view.WindowManager.LayoutParams;
/*    */ 
/*    */ public class CustomDialog
/*    */   extends Dialog
/*    */ {
/*    */   public CustomDialog(Context context, int width, int height, int layout, int style)
/*    */   {
/* 15 */     super(context, style);
/* 16 */     setContentView(layout);
/* 17 */     Window window = getWindow();
/* 18 */     WindowManager.LayoutParams params = window.getAttributes();
/* 19 */     params.width = width;
/* 20 */     params.height = height;
/* 21 */     params.gravity = 17;
/* 22 */     window.setAttributes(params);
/*    */   }
/*    */   
/*    */   public float getDensity(Context context) {
/* 26 */     Resources resources = context.getResources();
/* 27 */     DisplayMetrics dm = resources.getDisplayMetrics();
/* 28 */     return dm.density;
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\dialog\CustomDialog.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */