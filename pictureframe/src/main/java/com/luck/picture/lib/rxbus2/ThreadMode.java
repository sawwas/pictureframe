package com.luck.picture.lib.rxbus2;

public enum ThreadMode
{
  CURRENT_THREAD,  MAIN,  NEW_THREAD;
  
  private ThreadMode() {}
}


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\rxbus2\ThreadMode.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */