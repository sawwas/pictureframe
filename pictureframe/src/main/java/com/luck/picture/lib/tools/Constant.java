package com.luck.picture.lib.tools;

public class Constant
{
  public static final String ACTION_AC_FINISH = "app.activity.finish";
  public static final String ACTION_AC_REFRESH_DATA = "app.action.refresh.data";
  public static final String ACTION_CROP_DATA = "app.action.crop_data";
  public static final String ACTION_AC_SINGE_UCROP = "app.activity.singe.ucrop.finish";
  public static final int WRITE_EXTERNAL_STORAGE = 1;
}


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\tools\Constant.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */