/*     */ package com.luck.picture.lib;
/*     */ 
/*     */ import android.app.Activity;
/*     */ import android.content.Intent;
/*     */ import android.support.annotation.FloatRange;
/*     */ import android.support.annotation.IntRange;
/*     */ import android.support.annotation.StyleRes;
/*     */ import android.support.v4.app.Fragment;
/*     */ import com.luck.picture.lib.config.PictureSelectionConfig;
/*     */ import com.luck.picture.lib.entity.LocalMedia;
/*     */ import com.luck.picture.lib.tools.DoubleUtils;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class PictureSelectionModel
/*     */ {
/*     */   private PictureSelectionConfig selectionConfig;
/*     */   private PictureSelector selector;
/*     */   
/*     */   public PictureSelectionModel(PictureSelector selector, int mimeType)
/*     */   {
/*  31 */     this.selector = selector;
/*  32 */     this.selectionConfig = PictureSelectionConfig.getCleanInstance();
/*  33 */     this.selectionConfig.mimeType = mimeType;
/*     */   }
/*     */   
/*     */   public PictureSelectionModel(PictureSelector selector, int mimeType, boolean camera) {
/*  37 */     this.selector = selector;
/*  38 */     this.selectionConfig = PictureSelectionConfig.getCleanInstance();
/*  39 */     this.selectionConfig.camera = camera;
/*  40 */     this.selectionConfig.mimeType = mimeType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel theme(@StyleRes int themeStyleId)
/*     */   {
/*  48 */     this.selectionConfig.themeStyleId = themeStyleId;
/*  49 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel selectionMode(int selectionMode)
/*     */   {
/*  57 */     this.selectionConfig.selectionMode = selectionMode;
/*  58 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel enableCrop(boolean enableCrop)
/*     */   {
/*  66 */     this.selectionConfig.enableCrop = enableCrop;
/*  67 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel enablePreviewAudio(boolean enablePreviewAudio)
/*     */   {
/*  75 */     this.selectionConfig.enablePreviewAudio = enablePreviewAudio;
/*  76 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel freeStyleCropEnabled(boolean freeStyleCropEnabled)
/*     */   {
/*  84 */     this.selectionConfig.freeStyleCropEnabled = freeStyleCropEnabled;
/*  85 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel scaleEnabled(boolean scaleEnabled)
/*     */   {
/*  93 */     this.selectionConfig.scaleEnabled = scaleEnabled;
/*  94 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel rotateEnabled(boolean rotateEnabled)
/*     */   {
/* 102 */     this.selectionConfig.rotateEnabled = rotateEnabled;
/* 103 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel circleDimmedLayer(boolean circleDimmedLayer)
/*     */   {
/* 111 */     this.selectionConfig.circleDimmedLayer = circleDimmedLayer;
/* 112 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel showCropFrame(boolean showCropFrame)
/*     */   {
/* 120 */     this.selectionConfig.showCropFrame = showCropFrame;
/* 121 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel showCropGrid(boolean showCropGrid)
/*     */   {
/* 129 */     this.selectionConfig.showCropGrid = showCropGrid;
/* 130 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel hideBottomControls(boolean hideBottomControls)
/*     */   {
/* 138 */     this.selectionConfig.hideBottomControls = hideBottomControls;
/* 139 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel withAspectRatio(int aspect_ratio_x, int aspect_ratio_y)
/*     */   {
/* 148 */     this.selectionConfig.aspect_ratio_x = aspect_ratio_x;
/* 149 */     this.selectionConfig.aspect_ratio_y = aspect_ratio_y;
/* 150 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel maxSelectNum(int maxSelectNum)
/*     */   {
/* 158 */     this.selectionConfig.maxSelectNum = maxSelectNum;
/* 159 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel minSelectNum(int minSelectNum)
/*     */   {
/* 167 */     this.selectionConfig.minSelectNum = minSelectNum;
/* 168 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel videoQuality(int videoQuality)
/*     */   {
/* 176 */     this.selectionConfig.videoQuality = videoQuality;
/* 177 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel cropWH(int cropWidth, int cropHeight)
/*     */   {
/* 186 */     this.selectionConfig.cropWidth = cropWidth;
/* 187 */     this.selectionConfig.cropHeight = cropHeight;
/* 188 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel videoMaxSecond(int videoMaxSecond)
/*     */   {
/* 196 */     this.selectionConfig.videoMaxSecond = (videoMaxSecond * 1000);
/* 197 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel videoMinSecond(int videoMinSecond)
/*     */   {
/* 205 */     this.selectionConfig.videoMinSecond = (videoMinSecond * 1000);
/* 206 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel recordVideoSecond(int recordVideoSecond)
/*     */   {
/* 215 */     this.selectionConfig.recordVideoSecond = recordVideoSecond;
/* 216 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel glideOverride(@IntRange(from=100L) int width, @IntRange(from=100L) int height)
/*     */   {
/* 226 */     this.selectionConfig.overrideWidth = width;
/* 227 */     this.selectionConfig.overrideHeight = height;
/* 228 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel sizeMultiplier(@FloatRange(from=0.10000000149011612D) float sizeMultiplier)
/*     */   {
/* 238 */     this.selectionConfig.sizeMultiplier = sizeMultiplier;
/* 239 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel imageSpanCount(int imageSpanCount)
/*     */   {
/* 247 */     this.selectionConfig.imageSpanCount = imageSpanCount;
/* 248 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel compressMode(int compressMode)
/*     */   {
/* 256 */     this.selectionConfig.compressMode = compressMode;
/* 257 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel compressWH(int width, int height)
/*     */   {
/* 266 */     this.selectionConfig.compressWidth = width;
/* 267 */     this.selectionConfig.compressHeight = height;
/* 268 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel compressMaxKB(int kb)
/*     */   {
/* 276 */     this.selectionConfig.compressMaxkB = (kb * 1024);
/* 277 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel cropCompressQuality(int compressQuality)
/*     */   {
/* 285 */     this.selectionConfig.cropCompressQuality = compressQuality;
/* 286 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel compressGrade(int compressGrade)
/*     */   {
/* 294 */     this.selectionConfig.compressGrade = compressGrade;
/* 295 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel compress(boolean isCompress)
/*     */   {
/* 303 */     this.selectionConfig.isCompress = isCompress;
/* 304 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel isZoomAnim(boolean zoomAnim)
/*     */   {
/* 312 */     this.selectionConfig.zoomAnim = zoomAnim;
/* 313 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel previewEggs(boolean previewEggs)
/*     */   {
/* 321 */     this.selectionConfig.previewEggs = previewEggs;
/* 322 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel isCamera(boolean isCamera)
/*     */   {
/* 330 */     this.selectionConfig.isCamera = isCamera;
/* 331 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel setOutputCameraPath(String outputCameraPath)
/*     */   {
/* 339 */     this.selectionConfig.outputCameraPath = outputCameraPath;
/* 340 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel isGif(boolean isGif)
/*     */   {
/* 348 */     this.selectionConfig.isGif = isGif;
/* 349 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel previewImage(boolean enablePreview)
/*     */   {
/* 357 */     this.selectionConfig.enablePreview = enablePreview;
/* 358 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel previewVideo(boolean enPreviewVideo)
/*     */   {
/* 366 */     this.selectionConfig.enPreviewVideo = enPreviewVideo;
/* 367 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel openClickSound(boolean openClickSound)
/*     */   {
/* 375 */     this.selectionConfig.openClickSound = openClickSound;
/* 376 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PictureSelectionModel selectionMedia(List<LocalMedia> selectionMedia)
/*     */   {
/* 384 */     if (selectionMedia == null) {
/* 385 */       selectionMedia = new ArrayList();
/*     */     }
/* 387 */     this.selectionConfig.selectionMedias = selectionMedia;
/* 388 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void forResult(int requestCode)
/*     */   {
/* 397 */     if (!DoubleUtils.isFastDoubleClick()) {
/* 398 */       Activity activity = this.selector.getActivity();
/* 399 */       if (activity == null) {
/* 400 */         return;
/*     */       }
/* 402 */       Intent intent = new Intent(activity, PictureSelectorActivity.class);
/* 403 */       Fragment fragment = this.selector.getFragment();
/* 404 */       if (fragment != null) {
/* 405 */         fragment.startActivityForResult(intent, requestCode);
/*     */       } else {
/* 407 */         activity.startActivityForResult(intent, requestCode);
/*     */       }
/* 409 */       activity.overridePendingTransition(R.anim.a5, 0);
/*     */     }
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\PictureSelectionModel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */