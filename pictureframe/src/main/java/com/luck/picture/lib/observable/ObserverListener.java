package com.luck.picture.lib.observable;

import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.entity.LocalMediaFolder;
import java.util.List;

public abstract interface ObserverListener
{
  public abstract void observerUpFoldersData(List<LocalMediaFolder> paramList);
  
  public abstract void observerUpSelectsData(List<LocalMedia> paramList);
}


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\observable\ObserverListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */