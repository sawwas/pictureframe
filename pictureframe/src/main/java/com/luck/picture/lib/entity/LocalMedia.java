/*     */ package com.luck.picture.lib.entity;
/*     */ 
/*     */ import android.os.Parcel;
/*     */ import android.os.Parcelable;
/*     */ import android.os.Parcelable.Creator;
/*     */ import android.text.TextUtils;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LocalMedia
/*     */   implements Parcelable
/*     */ {
/*     */   private String path;
/*     */   private String compressPath;
/*     */   private String cutPath;
/*     */   private long duration;
/*     */   private boolean isChecked;
/*     */   private boolean isCut;
/*     */   public int position;
/*     */   private int num;
/*     */   private int mimeType;
/*     */   private String pictureType;
/*     */   private boolean compressed;
/*     */   private int width;
/*     */   private int height;
/*     */   
/*     */   public LocalMedia() {}
/*     */   
/*     */   public LocalMedia(String path, long duration, int mimeType, String pictureType)
/*     */   {
/*  36 */     this.path = path;
/*  37 */     this.duration = duration;
/*  38 */     this.mimeType = mimeType;
/*  39 */     this.pictureType = pictureType;
/*     */   }
/*     */   
/*     */   public LocalMedia(String path, long duration, int mimeType, String pictureType, int width, int height) {
/*  43 */     this.path = path;
/*  44 */     this.duration = duration;
/*  45 */     this.mimeType = mimeType;
/*  46 */     this.pictureType = pictureType;
/*  47 */     this.width = width;
/*  48 */     this.height = height;
/*     */   }
/*     */   
/*     */   public LocalMedia(String path, long duration, boolean isChecked, int position, int num, int mimeType)
/*     */   {
/*  53 */     this.path = path;
/*  54 */     this.duration = duration;
/*  55 */     this.isChecked = isChecked;
/*  56 */     this.position = position;
/*  57 */     this.num = num;
/*  58 */     this.mimeType = mimeType;
/*     */   }
/*     */   
/*     */   public String getPictureType() {
/*  62 */     if (TextUtils.isEmpty(this.pictureType)) {
/*  63 */       this.pictureType = "image/jpeg";
/*     */     }
/*  65 */     return this.pictureType;
/*     */   }
/*     */   
/*     */   public void setPictureType(String pictureType) {
/*  69 */     this.pictureType = pictureType;
/*     */   }
/*     */   
/*     */   public String getPath() {
/*  73 */     return this.path;
/*     */   }
/*     */   
/*     */   public void setPath(String path) {
/*  77 */     this.path = path;
/*     */   }
/*     */   
/*     */   public String getCompressPath() {
/*  81 */     return this.compressPath;
/*     */   }
/*     */   
/*     */   public void setCompressPath(String compressPath) {
/*  85 */     this.compressPath = compressPath;
/*     */   }
/*     */   
/*     */   public String getCutPath() {
/*  89 */     return this.cutPath;
/*     */   }
/*     */   
/*     */   public void setCutPath(String cutPath) {
/*  93 */     this.cutPath = cutPath;
/*     */   }
/*     */   
/*     */   public long getDuration() {
/*  97 */     return this.duration;
/*     */   }
/*     */   
/*     */   public void setDuration(long duration) {
/* 101 */     this.duration = duration;
/*     */   }
/*     */   
/*     */   public boolean isChecked()
/*     */   {
/* 106 */     return this.isChecked;
/*     */   }
/*     */   
/*     */   public void setChecked(boolean checked) {
/* 110 */     this.isChecked = checked;
/*     */   }
/*     */   
/*     */   public boolean isCut() {
/* 114 */     return this.isCut;
/*     */   }
/*     */   
/*     */   public void setCut(boolean cut) {
/* 118 */     this.isCut = cut;
/*     */   }
/*     */   
/*     */   public int getPosition() {
/* 122 */     return this.position;
/*     */   }
/*     */   
/*     */   public void setPosition(int position) {
/* 126 */     this.position = position;
/*     */   }
/*     */   
/*     */   public int getNum() {
/* 130 */     return this.num;
/*     */   }
/*     */   
/*     */   public void setNum(int num) {
/* 134 */     this.num = num;
/*     */   }
/*     */   
/*     */   public int getMimeType() {
/* 138 */     return this.mimeType;
/*     */   }
/*     */   
/*     */   public void setMimeType(int mimeType) {
/* 142 */     this.mimeType = mimeType;
/*     */   }
/*     */   
/*     */   public boolean isCompressed() {
/* 146 */     return this.compressed;
/*     */   }
/*     */   
/*     */   public void setCompressed(boolean compressed) {
/* 150 */     this.compressed = compressed;
/*     */   }
/*     */   
/*     */   public int getWidth() {
/* 154 */     return this.width;
/*     */   }
/*     */   
/*     */   public void setWidth(int width) {
/* 158 */     this.width = width;
/*     */   }
/*     */   
/*     */   public int getHeight() {
/* 162 */     return this.height;
/*     */   }
/*     */   
/*     */   public void setHeight(int height) {
/* 166 */     this.height = height;
/*     */   }
/*     */   
/*     */   public int describeContents()
/*     */   {
/* 171 */     return 0;
/*     */   }
/*     */   
/*     */   public void writeToParcel(Parcel dest, int flags)
/*     */   {
/* 176 */     dest.writeString(this.path);
/* 177 */     dest.writeString(this.compressPath);
/* 178 */     dest.writeString(this.cutPath);
/* 179 */     dest.writeLong(this.duration);
/* 180 */     dest.writeByte((byte)(this.isChecked ? 1 : 0));
/* 181 */     dest.writeByte((byte)(this.isCut ? 1 : 0));
/* 182 */     dest.writeInt(this.position);
/* 183 */     dest.writeInt(this.num);
/* 184 */     dest.writeInt(this.mimeType);
/* 185 */     dest.writeString(this.pictureType);
/* 186 */     dest.writeByte((byte)(this.compressed ? 1 : 0));
/* 187 */     dest.writeInt(this.width);
/* 188 */     dest.writeInt(this.height);
/*     */   }
/*     */   
/*     */   protected LocalMedia(Parcel in) {
/* 192 */     this.path = in.readString();
/* 193 */     this.compressPath = in.readString();
/* 194 */     this.cutPath = in.readString();
/* 195 */     this.duration = in.readLong();
/* 196 */     this.isChecked = (in.readByte() != 0);
/* 197 */     this.isCut = (in.readByte() != 0);
/* 198 */     this.position = in.readInt();
/* 199 */     this.num = in.readInt();
/* 200 */     this.mimeType = in.readInt();
/* 201 */     this.pictureType = in.readString();
/* 202 */     this.compressed = (in.readByte() != 0);
/* 203 */     this.width = in.readInt();
/* 204 */     this.height = in.readInt();
/*     */   }
/*     */   
/* 207 */   public static final Parcelable.Creator<LocalMedia> CREATOR = new Parcelable.Creator()
/*     */   {
/*     */     public LocalMedia createFromParcel(Parcel source) {
/* 210 */       return new LocalMedia(source);
/*     */     }
/*     */     
/*     */     public LocalMedia[] newArray(int size)
/*     */     {
/* 215 */       return new LocalMedia[size];
/*     */     }
/*     */   };
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\entity\LocalMedia.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */