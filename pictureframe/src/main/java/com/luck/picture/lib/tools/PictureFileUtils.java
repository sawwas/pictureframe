/*     */ package com.luck.picture.lib.tools;
/*     */ 
/*     */ import android.annotation.SuppressLint;
/*     */ import android.content.ContentResolver;
/*     */ import android.content.ContentUris;
/*     */ import android.content.Context;
/*     */ import android.database.Cursor;
/*     */ import android.graphics.Bitmap;
/*     */ import android.graphics.Bitmap.CompressFormat;
/*     */ import android.graphics.Bitmap.Config;
/*     */ import android.graphics.BitmapFactory;
/*     */ import android.graphics.BitmapFactory.Options;
/*     */ import android.graphics.Canvas;
/*     */ import android.graphics.Matrix;
/*     */ import android.graphics.Paint;
/*     */ import android.graphics.PorterDuff.Mode;
/*     */ import android.graphics.PorterDuffXfermode;
/*     */ import android.graphics.Rect;
/*     */ import android.graphics.RectF;
/*     */ import android.media.ExifInterface;
/*     */ import android.net.Uri;
/*     */ import android.os.Build.VERSION;
/*     */ import android.os.Environment;
/*     */ import android.provider.DocumentsContract;
/*     */ import android.provider.MediaStore.Audio.Media;
/*     */ import android.provider.MediaStore.Images.Media;
/*     */ import android.provider.MediaStore.Video.Media;
/*     */ import android.support.annotation.NonNull;
/*     */ import android.text.TextUtils;
/*     */ import android.util.Log;
/*     */ import java.io.BufferedOutputStream;
/*     */ import java.io.File;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ import java.nio.channels.FileChannel;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Date;
/*     */ import java.util.List;
/*     */ import java.util.Locale;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class PictureFileUtils
/*     */ {
/*  49 */   private static String DEFAULT_CACHE_DIR = "picture_cache";
/*     */   public static final String POSTFIX = ".JPEG";
/*     */   public static final String POST_VIDEO = ".mp4";
/*     */   public static final String POST_AUDIO = ".mp3";
/*     */   public static final String APP_NAME = "PictureSelector";
/*     */   public static final String CAMERA_PATH = "/PictureSelector/CameraImage/";
/*     */   public static final String CAMERA_AUDIO_PATH = "/PictureSelector/CameraAudio/";
/*     */   public static final String CROP_PATH = "/PictureSelector/CropImage/";
/*     */   static final String TAG = "PictureFileUtils";
/*     */   
/*     */   public static File createCameraFile(Context context, int type, String outputCameraPath) { String path;
/*     */     String path;
/*  61 */     if (type == 3) {
/*  62 */       path = !TextUtils.isEmpty(outputCameraPath) ? outputCameraPath : "/PictureSelector/CameraAudio/";
/*     */     }
/*     */     else {
/*  65 */       path = !TextUtils.isEmpty(outputCameraPath) ? outputCameraPath : "/PictureSelector/CameraImage/";
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  70 */     return type == 3 ? createMediaFile(context, path, type) : createMediaFile(context, path, type);
/*     */   }
/*     */   
/*     */   public static File createCropFile(Context context, int type) {
/*  74 */     return createMediaFile(context, "/PictureSelector/CropImage/", type);
/*     */   }
/*     */   
/*     */   private static File createMediaFile(Context context, String parentPath, int type) {
/*  78 */     String state = Environment.getExternalStorageState();
/*     */     
/*  80 */     File rootDir = state.equals("mounted") ? Environment.getExternalStorageDirectory() : context.getCacheDir();
/*     */     
/*  82 */     File folderDir = new File(rootDir.getAbsolutePath() + parentPath);
/*  83 */     if ((!folderDir.exists()) && (folderDir.mkdirs())) {}
/*     */     
/*     */ 
/*     */ 
/*  87 */     String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CHINA).format(new Date());
/*  88 */     String fileName = "PictureSelector_" + timeStamp + "";
/*  89 */     File tmpFile = null;
/*  90 */     switch (type) {
/*     */     case 1: 
/*  92 */       tmpFile = new File(folderDir, fileName + ".JPEG");
/*  93 */       break;
/*     */     case 2: 
/*  95 */       tmpFile = new File(folderDir, fileName + ".mp4");
/*  96 */       break;
/*     */     case 3: 
/*  98 */       tmpFile = new File(folderDir, fileName + ".mp3");
/*     */     }
/*     */     
/* 101 */     return tmpFile;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isExternalStorageDocument(Uri uri)
/*     */   {
/* 119 */     return "com.android.externalstorage.documents".equals(uri.getAuthority());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isDownloadsDocument(Uri uri)
/*     */   {
/* 128 */     return "com.android.providers.downloads.documents".equals(uri.getAuthority());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isMediaDocument(Uri uri)
/*     */   {
/* 137 */     return "com.android.providers.media.documents".equals(uri.getAuthority());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isGooglePhotosUri(Uri uri)
/*     */   {
/* 145 */     return "com.google.android.apps.photos.content".equals(uri.getAuthority());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs)
/*     */   {
/* 162 */     Cursor cursor = null;
/* 163 */     String column = "_data";
/* 164 */     String[] projection = { "_data" };
/*     */     
/*     */ 
/*     */     try
/*     */     {
/* 169 */       cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
/*     */       
/* 171 */       if ((cursor != null) && (cursor.moveToFirst())) {
/* 172 */         int column_index = cursor.getColumnIndexOrThrow("_data");
/* 173 */         return cursor.getString(column_index);
/*     */       }
/*     */     } catch (IllegalArgumentException ex) {
/* 176 */       Log.i("PictureFileUtils", String.format(Locale.getDefault(), "getDataColumn: _data - [%s]", new Object[] { ex.getMessage() }));
/*     */     } finally {
/* 178 */       if (cursor != null) {
/* 179 */         cursor.close();
/*     */       }
/*     */     }
/* 182 */     return null;
/*     */   }
/*     */   
/*     */   public static File getPhotoCacheDir(Context context, File file) {
/* 186 */     File cacheDir = context.getCacheDir();
/* 187 */     String file_name = file.getName();
/* 188 */     if (cacheDir != null) {
/* 189 */       File mCacheDir = new File(cacheDir, DEFAULT_CACHE_DIR);
/* 190 */       if ((!mCacheDir.mkdirs()) && ((!mCacheDir.exists()) || (!mCacheDir.isDirectory()))) {
/* 191 */         return file;
/*     */       }
/* 193 */       String fileName = "";
/* 194 */       if (file_name.endsWith(".webp")) {
/* 195 */         fileName = System.currentTimeMillis() + ".webp";
/*     */       } else {
/* 197 */         fileName = System.currentTimeMillis() + ".png";
/*     */       }
/* 199 */       return new File(mCacheDir, fileName);
/*     */     }
/*     */     
/* 202 */     if (Log.isLoggable("PictureFileUtils", 6)) {
/* 203 */       Log.e("PictureFileUtils", "default disk cache dir is null");
/*     */     }
/* 205 */     return file;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   @SuppressLint({"NewApi"})
/*     */   public static String getPath(Context context, Uri uri)
/*     */   {
/* 222 */     boolean isKitKat = Build.VERSION.SDK_INT >= 19;
/*     */     
/*     */ 
/* 225 */     if ((isKitKat) && (DocumentsContract.isDocumentUri(context, uri))) {
/* 226 */       if (isExternalStorageDocument(uri)) {
/* 227 */         String docId = DocumentsContract.getDocumentId(uri);
/* 228 */         String[] split = docId.split(":");
/* 229 */         String type = split[0];
/*     */         
/* 231 */         if ("primary".equalsIgnoreCase(type)) {
/* 232 */           return Environment.getExternalStorageDirectory() + "/" + split[1];
/*     */         }
/*     */         
/*     */       }
/*     */       else
/*     */       {
/* 238 */         if (isDownloadsDocument(uri))
/*     */         {
/* 240 */           String id = DocumentsContract.getDocumentId(uri);
/* 241 */           Uri contentUri = ContentUris.withAppendedId(
/* 242 */             Uri.parse("content://downloads/public_downloads"), Long.valueOf(id).longValue());
/*     */           
/* 244 */           return getDataColumn(context, contentUri, null, null);
/*     */         }
/*     */         
/* 247 */         if (isMediaDocument(uri)) {
/* 248 */           String docId = DocumentsContract.getDocumentId(uri);
/* 249 */           String[] split = docId.split(":");
/* 250 */           String type = split[0];
/*     */           
/* 252 */           Uri contentUri = null;
/* 253 */           if ("image".equals(type)) {
/* 254 */             contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
/* 255 */           } else if ("video".equals(type)) {
/* 256 */             contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
/* 257 */           } else if ("audio".equals(type)) {
/* 258 */             contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
/*     */           }
/*     */           
/* 261 */           String selection = "_id=?";
/* 262 */           String[] selectionArgs = { split[1] };
/*     */           
/*     */ 
/*     */ 
/* 266 */           return getDataColumn(context, contentUri, "_id=?", selectionArgs);
/*     */         }
/*     */       }
/*     */     } else {
/* 270 */       if ("content".equalsIgnoreCase(uri.getScheme()))
/*     */       {
/*     */ 
/* 273 */         if (isGooglePhotosUri(uri)) {
/* 274 */           return uri.getLastPathSegment();
/*     */         }
/*     */         
/* 277 */         return getDataColumn(context, uri, null, null);
/*     */       }
/*     */       
/* 280 */       if ("file".equalsIgnoreCase(uri.getScheme())) {
/* 281 */         return uri.getPath();
/*     */       }
/*     */     }
/* 284 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void copyFile(@NonNull String pathFrom, @NonNull String pathTo)
/*     */     throws IOException
/*     */   {
/* 294 */     if (pathFrom.equalsIgnoreCase(pathTo)) {
/* 295 */       return;
/*     */     }
/*     */     
/* 298 */     FileChannel outputChannel = null;
/* 299 */     FileChannel inputChannel = null;
/*     */     try {
/* 301 */       inputChannel = new FileInputStream(new File(pathFrom)).getChannel();
/* 302 */       outputChannel = new FileOutputStream(new File(pathTo)).getChannel();
/* 303 */       inputChannel.transferTo(0L, inputChannel.size(), outputChannel);
/* 304 */       inputChannel.close();
/*     */     } finally {
/* 306 */       if (inputChannel != null) inputChannel.close();
/* 307 */       if (outputChannel != null) { outputChannel.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void copyAudioFile(@NonNull String pathFrom, @NonNull String pathTo)
/*     */     throws IOException
/*     */   {
/* 318 */     if (pathFrom.equalsIgnoreCase(pathTo)) {
/* 319 */       return;
/*     */     }
/*     */     
/* 322 */     FileChannel outputChannel = null;
/* 323 */     FileChannel inputChannel = null;
/*     */     try {
/* 325 */       inputChannel = new FileInputStream(new File(pathFrom)).getChannel();
/* 326 */       outputChannel = new FileOutputStream(new File(pathTo)).getChannel();
/* 327 */       inputChannel.transferTo(0L, inputChannel.size(), outputChannel);
/* 328 */       inputChannel.close();
/*     */     } finally {
/* 330 */       if (inputChannel != null) inputChannel.close();
/* 331 */       if (outputChannel != null) outputChannel.close();
/* 332 */       deleteFile(pathFrom);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int readPictureDegree(String path)
/*     */   {
/* 343 */     int degree = 0;
/*     */     try {
/* 345 */       ExifInterface exifInterface = new ExifInterface(path);
/* 346 */       int orientation = exifInterface.getAttributeInt("Orientation", 1);
/* 347 */       switch (orientation) {
/*     */       case 6: 
/* 349 */         degree = 90;
/* 350 */         break;
/*     */       case 3: 
/* 352 */         degree = 180;
/* 353 */         break;
/*     */       case 8: 
/* 355 */         degree = 270;
/*     */       }
/*     */     }
/*     */     catch (IOException e) {
/* 359 */       e.printStackTrace();
/*     */     }
/* 361 */     return degree;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Bitmap rotaingImageView(int angle, Bitmap bitmap)
/*     */   {
/* 372 */     Matrix matrix = new Matrix();
/* 373 */     matrix.postRotate(angle);
/* 374 */     System.out.println("angle2=" + angle);
/*     */     
/* 376 */     Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap
/* 377 */       .getWidth(), bitmap.getHeight(), matrix, true);
/* 378 */     return resizedBitmap;
/*     */   }
/*     */   
/*     */   public static void saveBitmapFile(Bitmap bitmap, File file) {
/*     */     try {
/* 383 */       BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
/* 384 */       bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
/* 385 */       bos.flush();
/* 386 */       bos.close();
/*     */     } catch (IOException e) {
/* 388 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Bitmap toRoundBitmap(Bitmap bitmap)
/*     */   {
/* 399 */     int width = bitmap.getWidth();
/* 400 */     int height = bitmap.getHeight();
/*     */     float dst_bottom;
/*     */     float roundPx;
/* 403 */     float left; float right; float top; float bottom; float dst_left; float dst_top; float dst_right; float dst_bottom; if (width <= height) {
/* 404 */       float roundPx = width / 2;
/*     */       
/* 406 */       float left = 0.0F;
/* 407 */       float top = 0.0F;
/* 408 */       float right = width;
/* 409 */       float bottom = width;
/*     */       
/* 411 */       height = width;
/*     */       
/* 413 */       float dst_left = 0.0F;
/* 414 */       float dst_top = 0.0F;
/* 415 */       float dst_right = width;
/* 416 */       dst_bottom = width;
/*     */     } else {
/* 418 */       roundPx = height / 2;
/*     */       
/* 420 */       float clip = (width - height) / 2;
/*     */       
/* 422 */       left = clip;
/* 423 */       right = width - clip;
/* 424 */       top = 0.0F;
/* 425 */       bottom = height;
/* 426 */       width = height;
/*     */       
/* 428 */       dst_left = 0.0F;
/* 429 */       dst_top = 0.0F;
/* 430 */       dst_right = height;
/* 431 */       dst_bottom = height;
/*     */     }
/*     */     
/* 434 */     Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
/* 435 */     Canvas canvas = new Canvas(output);
/*     */     
/* 437 */     Paint paint = new Paint();
/* 438 */     Rect src = new Rect((int)left, (int)top, (int)right, (int)bottom);
/* 439 */     Rect dst = new Rect((int)dst_left, (int)dst_top, (int)dst_right, (int)dst_bottom);
/* 440 */     RectF rectF = new RectF(dst);
/*     */     
/* 442 */     paint.setAntiAlias(true);
/*     */     
/* 444 */     canvas.drawARGB(0, 0, 0, 0);
/*     */     
/*     */ 
/* 447 */     canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
/*     */     
/*     */ 
/* 450 */     paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
/* 451 */     canvas.drawBitmap(bitmap, src, dst, paint);
/*     */     
/* 453 */     return output;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String createDir(Context context, String filename, String directory_path)
/*     */   {
/* 463 */     String state = Environment.getExternalStorageState();
/* 464 */     File rootDir = state.equals("mounted") ? Environment.getExternalStorageDirectory() : context.getCacheDir();
/* 465 */     File path = null;
/* 466 */     if (!TextUtils.isEmpty(directory_path))
/*     */     {
/* 468 */       path = new File(rootDir.getAbsolutePath() + directory_path);
/*     */     } else {
/* 470 */       path = new File(rootDir.getAbsolutePath() + "/PictureSelector");
/*     */     }
/* 472 */     if (!path.exists())
/*     */     {
/* 474 */       path.mkdirs();
/*     */     }
/* 476 */     return path + "/" + filename;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int isDamage(String path)
/*     */   {
/* 487 */     BitmapFactory.Options options = null;
/* 488 */     if (options == null) options = new BitmapFactory.Options();
/* 489 */     options.inJustDecodeBounds = true;
/* 490 */     BitmapFactory.decodeFile(path, options);
/* 491 */     if ((options.mCancel) || (options.outWidth == -1) || (options.outHeight == -1))
/*     */     {
/*     */ 
/* 494 */       return -1;
/*     */     }
/* 496 */     return 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static List<String> getDirFiles(String dir)
/*     */   {
/* 505 */     File scanner5Directory = new File(dir);
/* 506 */     List<String> list = new ArrayList();
/* 507 */     if (scanner5Directory.isDirectory()) {
/* 508 */       for (File file : scanner5Directory.listFiles()) {
/* 509 */         String path = file.getAbsolutePath();
/* 510 */         if ((path.endsWith(".jpg")) || (path.endsWith(".jpeg")) || 
/* 511 */           (path.endsWith(".png")) || (path.endsWith(".gif")) || 
/* 512 */           (path.endsWith(".webp"))) {
/* 513 */           list.add(path);
/*     */         }
/*     */       }
/*     */     }
/* 517 */     return list;
/*     */   }
/*     */   
/*     */   public static String getDCIMCameraPath()
/*     */   {
/*     */     try
/*     */     {
/* 524 */       absolutePath = "%" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath() + "/Camera";
/*     */     } catch (Exception e) { String absolutePath;
/* 526 */       e.printStackTrace();
/* 527 */       return ""; }
/*     */     String absolutePath;
/* 529 */     return absolutePath;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void deleteCacheDirFile(Context mContext)
/*     */   {
/* 538 */     File cutDir = mContext.getCacheDir();
/* 539 */     File compressDir = new File(mContext.getCacheDir() + "/picture_cache");
/* 540 */     File lubanDir = new File(mContext.getCacheDir() + "/luban_disk_cache");
/* 541 */     if (cutDir != null) {
/* 542 */       File[] files = cutDir.listFiles();
/* 543 */       for (File file : files) {
/* 544 */         if (file.isFile()) {
/* 545 */           file.delete();
/*     */         }
/*     */       }
/*     */     }
/* 549 */     if (compressDir != null) {
/* 550 */       File[] files = compressDir.listFiles();
/* 551 */       if (files != null) {
/* 552 */         for (File file : files) {
/* 553 */           if (file.isFile())
/* 554 */             file.delete();
/*     */         }
/*     */       }
/*     */     }
/* 558 */     if (lubanDir != null) {
/* 559 */       File[] files = lubanDir.listFiles();
/* 560 */       if (files != null) {
/* 561 */         for (File file : files)
/* 562 */           if (file.isFile())
/* 563 */             file.delete();
/*     */       }
/*     */     }
/* 566 */     DebugUtil.i("PictureFileUtils", "Cache delete success!");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void deleteFile(String path)
/*     */   {
/*     */     try
/*     */     {
/* 576 */       if (!TextUtils.isEmpty(path)) {
/* 577 */         File file = new File(path);
/* 578 */         if (file != null) {
/* 579 */           file.delete();
/*     */         }
/*     */       }
/*     */     } catch (Exception e) {
/* 583 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\tools\PictureFileUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */