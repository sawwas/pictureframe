/*    */ package com.luck.picture.lib.compress;
/*    */ 
/*    */ import android.support.annotation.Nullable;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ final class Preconditions
/*    */ {
/*    */   static <T> T checkNotNull(T reference)
/*    */   {
/* 34 */     if (reference == null) {
/* 35 */       throw new NullPointerException();
/*    */     }
/* 37 */     return reference;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   static <T> T checkNotNull(T reference, @Nullable Object errorMessage)
/*    */   {
/* 50 */     if (reference == null) {
/* 51 */       throw new NullPointerException(String.valueOf(errorMessage));
/*    */     }
/* 53 */     return reference;
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\compress\Preconditions.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */