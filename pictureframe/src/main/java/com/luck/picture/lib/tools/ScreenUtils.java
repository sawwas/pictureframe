/*    */ package com.luck.picture.lib.tools;
/*    */ 
/*    */ import android.app.Activity;
/*    */ import android.content.Context;
/*    */ import android.content.res.Resources;
/*    */ import android.util.DisplayMetrics;
/*    */ import android.view.Display;
/*    */ import android.view.WindowManager;
/*    */ import java.lang.reflect.Field;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ScreenUtils
/*    */ {
/*    */   public static int dip2px(Context context, float dpValue)
/*    */   {
/* 17 */     float scale = context.getResources().getDisplayMetrics().density;
/* 18 */     return (int)(dpValue * scale + 0.5F);
/*    */   }
/*    */   
/* 21 */   public static int getScreenWidth(Context context) { DisplayMetrics localDisplayMetrics = new DisplayMetrics();
/* 22 */     ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
/* 23 */     return localDisplayMetrics.widthPixels;
/*    */   }
/*    */   
/* 26 */   public static int getScreenHeight(Context context) { DisplayMetrics localDisplayMetrics = new DisplayMetrics();
/* 27 */     ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
/* 28 */     return localDisplayMetrics.heightPixels - getStatusBarHeight(context);
/*    */   }
/*    */   
/* 31 */   public static int getStatusBarHeight(Context context) { Class<?> c = null;
/* 32 */     Object obj = null;
/* 33 */     Field field = null;
/* 34 */     int x = 0;int statusBarHeight = 0;
/*    */     try {
/* 36 */       c = Class.forName("com.android.internal.R$dimen");
/* 37 */       obj = c.newInstance();
/* 38 */       field = c.getField("status_bar_height");
/* 39 */       x = Integer.parseInt(field.get(obj).toString());
/* 40 */       statusBarHeight = context.getResources().getDimensionPixelSize(x);
/*    */     } catch (Exception e1) {
/* 42 */       e1.printStackTrace();
/*    */     }
/* 44 */     return statusBarHeight;
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\tools\ScreenUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */