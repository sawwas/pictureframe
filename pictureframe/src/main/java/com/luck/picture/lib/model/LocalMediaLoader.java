/*     */
package com.luck.picture.lib.model;
/*     */
/*     */

import android.database.Cursor;
/*     */ import android.net.Uri;
/*     */ import android.os.Bundle;
/*     */ import android.provider.MediaStore;
import android.provider.MediaStore.Audio.Media;
/*     */ import android.provider.MediaStore.Files;
/*     */ import android.provider.MediaStore.Images.Media;
/*     */ import android.provider.MediaStore.Video.Media;
/*     */ import android.support.v4.app.FragmentActivity;
/*     */ import android.support.v4.app.LoaderManager;
/*     */ import android.support.v4.app.LoaderManager.LoaderCallbacks;
/*     */ import android.support.v4.content.CursorLoader;
/*     */ import android.support.v4.content.Loader;
/*     */ import com.luck.picture.lib.R;
import com.luck.picture.lib.R.string;
/*     */ import com.luck.picture.lib.config.PictureMimeType;
/*     */ import com.luck.picture.lib.entity.LocalMedia;
/*     */ import com.luck.picture.lib.entity.LocalMediaFolder;
/*     */ import java.io.File;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import java.util.Comparator;
/*     */ import java.util.List;
/*     */ import java.util.Locale;

/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */ public class LocalMediaLoader
        /*     */ {
    /*  35 */   private static final Uri QUERY_URI = MediaStore.Files.getContentUri("external");
    /*     */   private static final String DURATION = "duration";
    /*     */   private static final int AUDIO_DURATION = 500;
    /*  38 */   private int type = 1;
    /*     */   private FragmentActivity activity;
    /*     */   private boolean isGif;
    /*  41 */   private long videoMaxS = 0L;
    /*  42 */   private long videoMinS = 0L;
    /*     */
    /*  44 */   private static final String[] IMAGE_PROJECTION = {"_id", "_data", "_display_name", "date_added", "width", "height", "mime_type", "_size"};
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*  55 */   private static final String[] VIDEO_PROJECTION = {"_id", "_data", "_display_name", "date_added", "width", "height", "mime_type", "duration"};
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*  67 */   private static final String[] PROJECTION = {"_id", "_data", "date_added", "_display_name", "_size", "duration", "mime_type", "width", "height"};
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*  79 */   private static final String[] AUDIO_PROJECTION = {"_id", "_data", "_display_name", "date_added", "is_music", "is_podcast", "mime_type", "duration"};
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */   private static final String SELECTION_ALL = "(media_type=? OR media_type=?) AND _size>0 AND width>0";
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /* 101 */   private static final String[] SELECTION_ALL_ARGS = {
/* 102 */     String.valueOf(1),
/* 103 */     String.valueOf(3)};
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */   private static final String CONDITION_GIF = "(mime_type=? or mime_type=? or mime_type=? or mime_type=?) AND width>0";
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /* 116 */   private static final String[] SELECT_GIF = {"image/jpeg", "image/png", "image/gif", "image/webp"};
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */   private static final String CONDITION = "(mime_type=? or mime_type=? or mime_type=?) AND width>0";
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /* 133 */   private static final String[] SELECT = {"image/jpeg", "image/png", "image/webp"};
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */   private static final String SELECTION_NOT_GIF = "(mime_type=? OR mime_type=? OR mime_type=? OR media_type=?) AND _size>0 AND width>0";
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /* 154 */   private static final String[] SELECTION_NOT_GIF_ARGS = {"image/jpeg", "image/png", "image/webp",
/*     */
/*     */
/*     */
/* 158 */     String.valueOf(3)};
    /*     */
    /*     */   private static final String ORDER_BY = "_id DESC";

    /*     */
    /*     */
    /*     */
    public LocalMediaLoader(FragmentActivity activity, int type, boolean isGif, long videoMaxS, long videoMinS)
    /*     */ {
        /* 165 */
        this.activity = activity;
        /* 166 */
        this.type = type;
        /* 167 */
        this.isGif = isGif;
        /* 168 */
        this.videoMaxS = videoMaxS;
        /* 169 */
        this.videoMinS = videoMinS;
        /*     */
    }

    /*     */
    /*     */
    public void loadAllMedia(final LocalMediaLoadListener imageLoadListener) {
        /* 173 */
        this.activity.getSupportLoaderManager().initLoader(this.type, null, new LoaderManager.LoaderCallbacks()
                /*     */ {
            /*     */
            public Loader<Cursor> onCreateLoader(int id, Bundle args)
            /*     */ {
                /* 177 */
                CursorLoader cursorLoader = null;
                /* 178 */
                switch (id) {
                    /*     */
                    case 0:
                        /* 180 */
                        String condition = LocalMediaLoader.this.getDurationCondition(0L, 0L);
                        /* 181 */
                        String selection_all = "(media_type=? OR media_type=? and " + condition + ")" + " AND " + "_size" + ">0" + " AND " + "width" + ">0";
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /* 189 */
                        String selection_not_gif = "(mime_type=? OR mime_type=? OR mime_type=? OR media_type=? and " + condition + ")" + " AND " + "_size" + ">0" + " AND " + "width" + ">0";
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /*     */
                        /* 205 */
                        cursorLoader = new CursorLoader(LocalMediaLoader.this.activity, LocalMediaLoader.QUERY_URI, LocalMediaLoader.PROJECTION, LocalMediaLoader.this.isGif ? selection_all : selection_not_gif, LocalMediaLoader.this.isGif ? LocalMediaLoader.SELECTION_ALL_ARGS : LocalMediaLoader.SELECTION_NOT_GIF_ARGS, "_id DESC");
                        /* 206 */
                        break;
                    /*     */
                    /*     */
                    /*     */
                    /*     */
                    case 1:
                        /* 211 */
                        cursorLoader = new CursorLoader(LocalMediaLoader.this.activity, MediaStore.Images.Media.EXTERNAL_CONTENT_URI, LocalMediaLoader.IMAGE_PROJECTION, LocalMediaLoader.this.isGif ? "(mime_type=? or mime_type=? or mime_type=? or mime_type=?) AND width>0" : "(mime_type=? or mime_type=? or mime_type=?) AND width>0", LocalMediaLoader.this.isGif ? LocalMediaLoader.SELECT_GIF : LocalMediaLoader.SELECT, LocalMediaLoader.IMAGE_PROJECTION[0] + " DESC");
                        /* 212 */
                        break;
                    /*     */
                    /*     */
                    /*     */
                    case 2:
                        /* 216 */
                        cursorLoader = new CursorLoader(LocalMediaLoader.this.activity, MediaStore.Video.Media.EXTERNAL_CONTENT_URI, LocalMediaLoader.VIDEO_PROJECTION, LocalMediaLoader.this.getDurationCondition(0L, 0L), null, LocalMediaLoader.VIDEO_PROJECTION[0] + " DESC");
                        /* 217 */
                        break;
                    /*     */
                    /*     */
                    /*     */
                    /*     */
                    case 3:
                        /* 222 */
                        cursorLoader = new CursorLoader(LocalMediaLoader.this.activity, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, LocalMediaLoader.AUDIO_PROJECTION, LocalMediaLoader.this.getDurationCondition(0L, 500L), null, LocalMediaLoader.AUDIO_PROJECTION[0] + " DESC");
                        /*     */
                }
                /*     */
                /* 225 */
                return cursorLoader;
                /*     */
            }

            @Override
            public void onLoadFinished(Loader loader, Object data) {
                try {
                    /* 231 */
                    List<LocalMediaFolder> imageFolders = new ArrayList();
                    /* 232 */
                    LocalMediaFolder allImageFolder = new LocalMediaFolder();
                    /* 233 */
                    List<LocalMedia> latelyImages = new ArrayList();
                    /* 234 */
                    if (data != null) {
                        /* 235 */
                        int count = data.getCount();
                        /* 236 */
                        if (count > 0) {
                            /* 237 */
                            data.moveToFirst();
                            /*     */
                            do
                                /*     */ {
                                /* 240 */
                                String path = data.getString(data.getColumnIndexOrThrow(LocalMediaLoader.IMAGE_PROJECTION[1]));
                                /*     */
                                /* 242 */
                                String pictureType = data.getString(data.getColumnIndexOrThrow(LocalMediaLoader.IMAGE_PROJECTION[6]));
                                /* 243 */
                                boolean eqImg = pictureType.startsWith("image");
                                /*     */
                                /* 245 */
                                int duration = eqImg ? 0 : data.getInt(data.getColumnIndexOrThrow(LocalMediaLoader.VIDEO_PROJECTION[7]));
                                /*     */
                                /* 247 */
                                int w = eqImg ? data.getInt(data.getColumnIndexOrThrow(LocalMediaLoader.IMAGE_PROJECTION[4])) : 0;
                                /*     */
                                /* 249 */
                                int h = eqImg ? data.getInt(data.getColumnIndexOrThrow(LocalMediaLoader.IMAGE_PROJECTION[5])) : 0;
                                /*     */
                                /* 251 */
                                LocalMedia image = new LocalMedia(path, duration, LocalMediaLoader.this.type, pictureType, w, h);
                                /*     */
                                /* 253 */
                                LocalMediaFolder folder = LocalMediaLoader.this.getImageFolder(path, imageFolders);
                                /* 254 */
                                List<LocalMedia> images = folder.getImages();
                                /* 255 */
                                images.add(image);
                                /* 256 */
                                folder.setImageNum(folder.getImageNum() + 1);
                                /* 257 */
                                latelyImages.add(image);
                                /* 258 */
                                int imageNum = allImageFolder.getImageNum();
                                /* 259 */
                                allImageFolder.setImageNum(imageNum + 1);
                                /* 260 */
                            } while (data.moveToNext());
                            /*     */
                            /* 262 */
                            if (latelyImages.size() > 0) {
                                /* 263 */
                                LocalMediaLoader.this.sortFolder(imageFolders);
                                /* 264 */
                                imageFolders.add(0, allImageFolder);
                                /* 265 */
                                allImageFolder
/* 266 */.setFirstImagePath(((LocalMedia) latelyImages.get(0)).getPath());
                                /*     */
                                /*     */
                                /* 269 */
                                String title = LocalMediaLoader.this.type == PictureMimeType.ofAudio() ? LocalMediaLoader.this.activity.getString(R.string.picture_all_audio) : LocalMediaLoader.this.activity.getString(R.string.picture_camera_roll);
                                /* 270 */
                                allImageFolder.setName(title);
                                /* 271 */
                                allImageFolder.setImages(latelyImages);
                                /*     */
                            }
                            /* 273 */
                            imageLoadListener.loadComplete(imageFolders);
                            /*     */
                        }
                        /*     */
                        else {
                            /* 276 */
                            imageLoadListener.loadComplete(imageFolders);
                            /*     */
                        }
                        /*     */
                    }
                    /*     */
                } catch (Exception e) {
                    /* 280 */
                    e.printStackTrace();
                    /*     */
                }
            }

            @Override
            public void onLoaderReset(Loader loader) {

            }

            /*     */
            /*     */
            public void onLoadFinished(Loader<Cursor> loader, Cursor data)
            /*     */ {
                /*     */
                /*     */
            }
            /*     */
            /*     */
            /*     */
        });
        /*     */
    }

    /*     */
    /*     */
    /*     */
    private void sortFolder(List<LocalMediaFolder> imageFolders)
    /*     */ {
        /* 292 */
        Collections.sort(imageFolders, new Comparator()
                /*     */ {
            /*     */
            public int compare(LocalMediaFolder lhs, LocalMediaFolder rhs) {
                /* 295 */
                if ((lhs.getImages() == null) || (rhs.getImages() == null)) {
                    /* 296 */
                    return 0;
                    /*     */
                }
                /* 298 */
                int lsize = lhs.getImageNum();
                /* 299 */
                int rsize = rhs.getImageNum();
                /* 300 */
                return lsize < rsize ? 1 : lsize == rsize ? 0 : -1;
                /*     */
            }
            /*     */
        });
        /*     */
    }

    /*     */
    /*     */
    private LocalMediaFolder getImageFolder(String path, List<LocalMediaFolder> imageFolders) {
        /* 306 */
        File imageFile = new File(path);
        /* 307 */
        File folderFile = imageFile.getParentFile();
        /* 308 */
        for (LocalMediaFolder folder : imageFolders)
            /*     */ {
            /* 310 */
            if (folder.getName().equals(folderFile.getName())) {
                /* 311 */
                return folder;
                /*     */
            }
            /*     */
        }
        /* 314 */
        LocalMediaFolder newFolder = new LocalMediaFolder();
        /* 315 */
        newFolder.setName(folderFile.getName());
        /* 316 */
        newFolder.setPath(folderFile.getAbsolutePath());
        /* 317 */
        newFolder.setFirstImagePath(path);
        /* 318 */
        imageFolders.add(newFolder);
        /* 319 */
        return newFolder;
        /*     */
    }

    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    private String getDurationCondition(long exMaxLimit, long exMinLimit)
    /*     */ {
        /* 327 */
        long maxS = this.videoMaxS == 0L ? Long.MAX_VALUE : this.videoMaxS;
        /* 328 */
        if (exMaxLimit != 0L) {
            maxS = Math.min(maxS, exMaxLimit);
            /*     */
        }
        /* 330 */
        return String.format(Locale.CHINA, "%d <%s duration and duration <= %d", new Object[]{
/* 331 */       Long.valueOf(Math.max(exMinLimit, this.videoMinS)),
/* 332 */       Math.max(exMinLimit, this.videoMinS) == 0L ? "" : "=",
/* 333 */       Long.valueOf(maxS)});
        /*     */
    }

    /*     */
    /*     */   public static abstract interface LocalMediaLoadListener
            /*     */ {
        /*     */
        public abstract void loadComplete(List<LocalMediaFolder> paramList);
        /*     */
    }
    /*     */
}


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\model\LocalMediaLoader.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */