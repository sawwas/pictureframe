/*     */ package com.luck.picture.lib.rxbus2;
/*     */ 
/*     */ import io.reactivex.BackpressureStrategy;
/*     */ import io.reactivex.Flowable;
/*     */ import io.reactivex.Scheduler;
/*     */ import io.reactivex.android.schedulers.AndroidSchedulers;
/*     */ import io.reactivex.disposables.Disposable;
/*     */ import io.reactivex.functions.Consumer;
/*     */ import io.reactivex.functions.Function;
/*     */ import io.reactivex.functions.Predicate;
/*     */ import io.reactivex.schedulers.Schedulers;
/*     */ import io.reactivex.subjects.PublishSubject;
/*     */ import io.reactivex.subjects.Subject;
/*     */ import java.lang.reflect.Method;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RxBus
/*     */ {
/*     */   public static final String LOG_BUS = "RXBUS_LOG";
/*     */   private static volatile RxBus defaultInstance;
/*  33 */   private Map<Class, List<Disposable>> subscriptionsByEventType = new HashMap();
/*     */   
/*  35 */   private Map<Object, List<Class>> eventTypesBySubscriber = new HashMap();
/*     */   
/*  37 */   private Map<Class, List<SubscriberMethod>> subscriberMethodByEventType = new HashMap();
/*     */   private final Subject<Object> bus;
/*     */   
/*     */   private RxBus()
/*     */   {
/*  42 */     this.bus = PublishSubject.create().toSerialized();
/*     */   }
/*     */   
/*     */   public static RxBus getDefault() {
/*  46 */     RxBus rxBus = defaultInstance;
/*  47 */     if (defaultInstance == null) {
/*  48 */       synchronized (RxBus.class) {
/*  49 */         rxBus = defaultInstance;
/*  50 */         if (defaultInstance == null) {
/*  51 */           rxBus = new RxBus();
/*  52 */           defaultInstance = rxBus;
/*     */         }
/*     */       }
/*     */     }
/*  56 */     return rxBus;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public <T> Flowable<T> toObservable(Class<T> eventType)
/*     */   {
/*  66 */     return this.bus.toFlowable(BackpressureStrategy.BUFFER).ofType(eventType);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private <T> Flowable<T> toObservable(final int code, final Class<T> eventType)
/*     */   {
/*  87 */     this.bus.toFlowable(BackpressureStrategy.BUFFER).ofType(Message.class).filter(new Predicate()
/*     */     {
/*  80 */       public boolean test(RxBus.Message o)
/*  80 */         throws Exception { return (o.getCode() == code) && (eventType.isInstance(o.getObject())); } }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  87 */       ).map(new Function()
/*     */       {
/*     */ 
/*     */         public Object apply(RxBus.Message o)
/*     */           throws Exception {
/*  85 */           return o.getObject(); } })
/*     */       
/*  87 */       .cast(eventType);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void register(Object subscriber)
/*     */   {
/*  96 */     Class<?> subClass = subscriber.getClass();
/*  97 */     Method[] methods = subClass.getDeclaredMethods();
/*  98 */     for (Method method : methods) {
/*  99 */       if (method.isAnnotationPresent(Subscribe.class))
/*     */       {
/* 101 */         Class[] parameterType = method.getParameterTypes();
/*     */         
/* 103 */         if ((parameterType != null) && (parameterType.length == 1))
/*     */         {
/* 105 */           Class eventType = parameterType[0];
/*     */           
/* 107 */           addEventTypeToMap(subscriber, eventType);
/* 108 */           Subscribe sub = (Subscribe)method.getAnnotation(Subscribe.class);
/* 109 */           int code = sub.code();
/* 110 */           ThreadMode threadMode = sub.threadMode();
/*     */           
/* 112 */           SubscriberMethod subscriberMethod = new SubscriberMethod(subscriber, method, eventType, code, threadMode);
/* 113 */           addSubscriberToMap(eventType, subscriberMethod);
/*     */           
/* 115 */           addSubscriber(subscriberMethod);
/* 116 */         } else if ((parameterType == null) || (parameterType.length == 0))
/*     */         {
/* 118 */           Class eventType = BusData.class;
/*     */           
/* 120 */           addEventTypeToMap(subscriber, eventType);
/* 121 */           Subscribe sub = (Subscribe)method.getAnnotation(Subscribe.class);
/* 122 */           int code = sub.code();
/* 123 */           ThreadMode threadMode = sub.threadMode();
/*     */           
/* 125 */           SubscriberMethod subscriberMethod = new SubscriberMethod(subscriber, method, eventType, code, threadMode);
/* 126 */           addSubscriberToMap(eventType, subscriberMethod);
/*     */           
/* 128 */           addSubscriber(subscriberMethod);
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void addEventTypeToMap(Object subscriber, Class eventType)
/*     */   {
/* 143 */     List<Class> eventTypes = (List)this.eventTypesBySubscriber.get(subscriber);
/* 144 */     if (eventTypes == null) {
/* 145 */       eventTypes = new ArrayList();
/* 146 */       this.eventTypesBySubscriber.put(subscriber, eventTypes);
/*     */     }
/*     */     
/* 149 */     if (!eventTypes.contains(eventType)) {
/* 150 */       eventTypes.add(eventType);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void addSubscriberToMap(Class eventType, SubscriberMethod subscriberMethod)
/*     */   {
/* 161 */     List<SubscriberMethod> subscriberMethods = (List)this.subscriberMethodByEventType.get(eventType);
/* 162 */     if (subscriberMethods == null) {
/* 163 */       subscriberMethods = new ArrayList();
/* 164 */       this.subscriberMethodByEventType.put(eventType, subscriberMethods);
/*     */     }
/*     */     
/* 167 */     if (!subscriberMethods.contains(subscriberMethod)) {
/* 168 */       subscriberMethods.add(subscriberMethod);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void addSubscriptionToMap(Class eventType, Disposable disposable)
/*     */   {
/* 179 */     List<Disposable> disposables = (List)this.subscriptionsByEventType.get(eventType);
/* 180 */     if (disposables == null) {
/* 181 */       disposables = new ArrayList();
/* 182 */       this.subscriptionsByEventType.put(eventType, disposables);
/*     */     }
/*     */     
/* 185 */     if (!disposables.contains(disposable)) {
/* 186 */       disposables.add(disposable);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void addSubscriber(final SubscriberMethod subscriberMethod)
/*     */   {
/*     */     Flowable flowable;
/*     */     
/*     */     Flowable flowable;
/*     */     
/* 198 */     if (subscriberMethod.code == -1) {
/* 199 */       flowable = toObservable(subscriberMethod.eventType);
/*     */     } else {
/* 201 */       flowable = toObservable(subscriberMethod.code, subscriberMethod.eventType);
/*     */     }
/*     */     
/* 204 */     Disposable subscription = postToObservable(flowable, subscriberMethod).subscribe(new Consumer()
/*     */     {
/*     */       public void accept(Object o) throws Exception {
/* 207 */         RxBus.this.callEvent(subscriberMethod, o);
/*     */       }
/*     */       
/* 210 */     });
/* 211 */     addSubscriptionToMap(subscriberMethod.subscriber.getClass(), subscription);
/*     */   }
/*     */   
/*     */ 
/*     */   private Flowable postToObservable(Flowable observable, SubscriberMethod subscriberMethod)
/*     */   {
/*     */     Scheduler scheduler;
/*     */     
/*     */     Scheduler scheduler;
/*     */     
/*     */     Scheduler scheduler;
/*     */     
/* 223 */     switch (subscriberMethod.threadMode) {
/*     */     case MAIN: 
/* 225 */       scheduler = AndroidSchedulers.mainThread();
/* 226 */       break;
/*     */     
/*     */     case NEW_THREAD: 
/* 229 */       scheduler = Schedulers.newThread();
/* 230 */       break;
/*     */     
/*     */     case CURRENT_THREAD: 
/* 233 */       scheduler = Schedulers.trampoline();
/* 234 */       break;
/*     */     default: 
/* 236 */       throw new IllegalStateException("Unknown thread mode: " + subscriberMethod.threadMode); }
/*     */     Scheduler scheduler;
/* 238 */     return observable.observeOn(scheduler);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void callEvent(SubscriberMethod method, Object object)
/*     */   {
/* 248 */     Class eventClass = object.getClass();
/* 249 */     List<SubscriberMethod> methods = (List)this.subscriberMethodByEventType.get(eventClass);
/* 250 */     if ((methods != null) && (methods.size() > 0)) {
/* 251 */       for (SubscriberMethod subscriberMethod : methods) {
/* 252 */         Subscribe sub = (Subscribe)subscriberMethod.method.getAnnotation(Subscribe.class);
/* 253 */         int c = sub.code();
/* 254 */         if ((c == method.code) && (method.subscriber.equals(subscriberMethod.subscriber)) && (method.method.equals(subscriberMethod.method))) {
/* 255 */           subscriberMethod.invoke(object);
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public synchronized boolean isRegistered(Object subscriber)
/*     */   {
/* 270 */     return this.eventTypesBySubscriber.containsKey(subscriber);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void unregister(Object subscriber)
/*     */   {
/* 279 */     List<Class> subscribedTypes = (List)this.eventTypesBySubscriber.get(subscriber);
/* 280 */     if (subscribedTypes != null) {
/* 281 */       for (Class<?> eventType : subscribedTypes) {
/* 282 */         unSubscribeByEventType(subscriber.getClass());
/* 283 */         unSubscribeMethodByEventType(subscriber, eventType);
/*     */       }
/* 285 */       this.eventTypesBySubscriber.remove(subscriber);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void unSubscribeByEventType(Class eventType)
/*     */   {
/* 295 */     List<Disposable> disposables = (List)this.subscriptionsByEventType.get(eventType);
/* 296 */     if (disposables != null) {
/* 297 */       Iterator<Disposable> iterator = disposables.iterator();
/* 298 */       while (iterator.hasNext()) {
/* 299 */         Disposable disposable = (Disposable)iterator.next();
/* 300 */         if ((disposable != null) && (!disposable.isDisposed())) {
/* 301 */           disposable.dispose();
/* 302 */           iterator.remove();
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void unSubscribeMethodByEventType(Object subscriber, Class eventType)
/*     */   {
/* 315 */     List<SubscriberMethod> subscriberMethods = (List)this.subscriberMethodByEventType.get(eventType);
/* 316 */     if (subscriberMethods != null) {
/* 317 */       Iterator<SubscriberMethod> iterator = subscriberMethods.iterator();
/* 318 */       while (iterator.hasNext()) {
/* 319 */         SubscriberMethod subscriberMethod = (SubscriberMethod)iterator.next();
/* 320 */         if (subscriberMethod.subscriber.equals(subscriber)) {
/* 321 */           iterator.remove();
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void send(int code, Object o) {
/* 328 */     this.bus.onNext(new Message(code, o, null));
/*     */   }
/*     */   
/*     */   public void post(Object o) {
/* 332 */     this.bus.onNext(o);
/*     */   }
/*     */   
/*     */   public void send(int code) {
/* 336 */     this.bus.onNext(new Message(code, new BusData(), null));
/*     */   }
/*     */   
/*     */   private class Message
/*     */   {
/*     */     private int code;
/*     */     private Object object;
/*     */     
/*     */     public Message() {}
/*     */     
/*     */     private Message(int code, Object o) {
/* 347 */       this.code = code;
/* 348 */       this.object = o;
/*     */     }
/*     */     
/*     */     private int getCode() {
/* 352 */       return this.code;
/*     */     }
/*     */     
/*     */     public void setCode(int code) {
/* 356 */       this.code = code;
/*     */     }
/*     */     
/*     */     private Object getObject() {
/* 360 */       return this.object;
/*     */     }
/*     */     
/*     */     public void setObject(Object object) {
/* 364 */       this.object = object;
/*     */     }
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\rxbus2\RxBus.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */