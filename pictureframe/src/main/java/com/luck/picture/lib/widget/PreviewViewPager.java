/*    */ package com.luck.picture.lib.widget;
/*    */ 
/*    */ import android.content.Context;
/*    */ import android.support.v4.view.ViewPager;
/*    */ import android.util.AttributeSet;
/*    */ import android.view.MotionEvent;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class PreviewViewPager
/*    */   extends ViewPager
/*    */ {
/*    */   public PreviewViewPager(Context context)
/*    */   {
/* 19 */     super(context);
/*    */   }
/*    */   
/*    */   public PreviewViewPager(Context context, AttributeSet attrs) {
/* 23 */     super(context, attrs);
/*    */   }
/*    */   
/*    */   public boolean onTouchEvent(MotionEvent ev)
/*    */   {
/*    */     try {
/* 29 */       return super.onTouchEvent(ev);
/*    */     } catch (IllegalArgumentException ex) {
/* 31 */       ex.printStackTrace();
/*    */     }
/* 33 */     return false;
/*    */   }
/*    */   
/*    */   public boolean onInterceptTouchEvent(MotionEvent ev)
/*    */   {
/*    */     try {
/* 39 */       return super.onInterceptTouchEvent(ev);
/*    */     } catch (IllegalArgumentException ex) {
/* 41 */       ex.printStackTrace();
/*    */     }
/* 43 */     return false;
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\widget\PreviewViewPager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */