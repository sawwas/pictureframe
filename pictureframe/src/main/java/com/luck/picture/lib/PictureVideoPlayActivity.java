/*     */ package com.luck.picture.lib;
/*     */ 
/*     */ import android.content.Context;
/*     */ import android.content.ContextWrapper;
/*     */ import android.media.MediaPlayer;
/*     */ import android.media.MediaPlayer.OnErrorListener;
/*     */ import android.os.Bundle;
/*     */ import android.view.View;
/*     */ import android.widget.ImageView;
/*     */ import android.widget.MediaController;
/*     */ import android.widget.VideoView;
/*     */ 
/*     */ public class PictureVideoPlayActivity extends PictureBaseActivity implements MediaPlayer.OnErrorListener, android.media.MediaPlayer.OnPreparedListener, android.media.MediaPlayer.OnCompletionListener, android.view.View.OnClickListener
/*     */ {
/*  15 */   private String video_path = "";
/*     */   private ImageView picture_left_back;
/*     */   private MediaController mMediaController;
/*     */   private VideoView mVideoView;
/*     */   private ImageView iv_play;
/*  20 */   private int mPositionWhenPaused = -1;
/*     */   
/*     */   protected void onCreate(Bundle savedInstanceState)
/*     */   {
/*  24 */     getWindow().addFlags(67108864);
/*  25 */     super.onCreate(savedInstanceState);
/*  26 */     setContentView(R.layout.picture_activity_video_play);
/*  27 */     this.video_path = getIntent().getStringExtra("video_path");
/*  28 */     this.picture_left_back = ((ImageView)findViewById(R.id.picture_left_back));
/*  29 */     this.mVideoView = ((VideoView)findViewById(R.id.video_view));
/*  30 */     this.mVideoView.setBackgroundColor(-16777216);
/*  31 */     this.iv_play = ((ImageView)findViewById(R.id.iv_play));
/*  32 */     this.mMediaController = new MediaController(this);
/*  33 */     this.mVideoView.setOnCompletionListener(this);
/*  34 */     this.mVideoView.setOnPreparedListener(this);
/*  35 */     this.mVideoView.setMediaController(this.mMediaController);
/*  36 */     this.picture_left_back.setOnClickListener(this);
/*  37 */     this.iv_play.setOnClickListener(this);
/*     */   }
/*     */   
/*     */ 
/*     */   public void onStart()
/*     */   {
/*  43 */     this.mVideoView.setVideoPath(this.video_path);
/*  44 */     this.mVideoView.start();
/*  45 */     super.onStart();
/*     */   }
/*     */   
/*     */   public void onPause()
/*     */   {
/*  50 */     this.mPositionWhenPaused = this.mVideoView.getCurrentPosition();
/*  51 */     this.mVideoView.stopPlayback();
/*     */     
/*  53 */     super.onPause();
/*     */   }
/*     */   
/*     */   protected void onDestroy()
/*     */   {
/*  58 */     super.onDestroy();
/*  59 */     this.mMediaController = null;
/*  60 */     this.mVideoView = null;
/*     */   }
/*     */   
/*     */   public void onResume()
/*     */   {
/*  65 */     if (this.mPositionWhenPaused >= 0) {
/*  66 */       this.mVideoView.seekTo(this.mPositionWhenPaused);
/*  67 */       this.mPositionWhenPaused = -1;
/*     */     }
/*     */     
/*  70 */     super.onResume();
/*     */   }
/*     */   
/*     */   public boolean onError(MediaPlayer player, int arg1, int arg2)
/*     */   {
/*  75 */     return false;
/*     */   }
/*     */   
/*     */   public void onCompletion(MediaPlayer mp)
/*     */   {
/*  80 */     this.iv_play.setVisibility(0);
/*     */   }
/*     */   
/*     */   public void onClick(View v)
/*     */   {
/*  85 */     int id = v.getId();
/*  86 */     if (id == R.id.picture_left_back) {
/*  87 */       finish();
/*  88 */     } else if (id == R.id.iv_play) {
/*  89 */       this.mVideoView.start();
/*  90 */       this.iv_play.setVisibility(4);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void attachBaseContext(Context newBase)
/*     */   {
/*  96 */     super.attachBaseContext(new ContextWrapper(newBase)
/*     */     {
/*     */       public Object getSystemService(String name) {
/*  99 */         if ("audio".equals(name))
/* 100 */           return getApplicationContext().getSystemService(name);
/* 101 */         return super.getSystemService(name);
/*     */       }
/*     */     });
/*     */   }
/*     */   
/*     */   public void onPrepared(MediaPlayer mp)
/*     */   {
/* 108 */     mp.setOnInfoListener(new android.media.MediaPlayer.OnInfoListener()
/*     */     {
/*     */       public boolean onInfo(MediaPlayer mp, int what, int extra) {
/* 111 */         if (what == 3)
/*     */         {
/* 113 */           PictureVideoPlayActivity.this.mVideoView.setBackgroundColor(0);
/* 114 */           return true;
/*     */         }
/* 116 */         return false;
/*     */       }
/*     */     });
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\PictureVideoPlayActivity.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */