/*    */ package com.luck.picture.lib.widget;
/*    */ 
/*    */ import android.annotation.TargetApi;
/*    */ import android.content.Context;
/*    */ import android.util.AttributeSet;
/*    */ import android.widget.RelativeLayout;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SquareRelativeLayout
/*    */   extends RelativeLayout
/*    */ {
/*    */   public SquareRelativeLayout(Context context)
/*    */   {
/* 20 */     super(context);
/*    */   }
/*    */   
/*    */   public SquareRelativeLayout(Context context, AttributeSet attrs) {
/* 24 */     super(context, attrs);
/*    */   }
/*    */   
/*    */   public SquareRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
/* 28 */     super(context, attrs, defStyleAttr);
/*    */   }
/*    */   
/*    */   @TargetApi(21)
/*    */   public SquareRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
/* 33 */     super(context, attrs, defStyleAttr, defStyleRes);
/*    */   }
/*    */   
/*    */ 
/*    */   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
/*    */   {
/* 39 */     super.onMeasure(widthMeasureSpec, widthMeasureSpec);
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\widget\SquareRelativeLayout.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */