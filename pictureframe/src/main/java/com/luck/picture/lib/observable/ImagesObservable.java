/*     */ package com.luck.picture.lib.observable;
/*     */ 
/*     */ import com.luck.picture.lib.entity.LocalMedia;
/*     */ import com.luck.picture.lib.entity.LocalMediaFolder;
/*     */ import com.luck.picture.lib.tools.DebugUtil;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ImagesObservable
/*     */   implements SubjectListener
/*     */ {
/*  20 */   private List<ObserverListener> observers = new ArrayList();
/*     */   private List<LocalMediaFolder> folders;
/*     */   private List<LocalMedia> medias;
/*     */   private List<LocalMedia> selectedImages;
/*     */   private static ImagesObservable sObserver;
/*     */   
/*     */   private ImagesObservable()
/*     */   {
/*  28 */     this.folders = new ArrayList();
/*  29 */     this.medias = new ArrayList();
/*  30 */     this.selectedImages = new ArrayList();
/*     */   }
/*     */   
/*     */   public static ImagesObservable getInstance() {
/*  34 */     if (sObserver == null) {
/*  35 */       synchronized (ImagesObservable.class) {
/*  36 */         if (sObserver == null) {
/*  37 */           sObserver = new ImagesObservable();
/*     */         }
/*     */       }
/*     */     }
/*  41 */     return sObserver;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void saveLocalFolders(List<LocalMediaFolder> list)
/*     */   {
/*  52 */     if (list != null) {
/*  53 */       this.folders = list;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void saveLocalMedia(List<LocalMedia> list)
/*     */   {
/*  64 */     this.medias = list;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocalMedia> readLocalMedias()
/*     */   {
/*  72 */     if (this.medias == null) {
/*  73 */       this.medias = new ArrayList();
/*     */     }
/*  75 */     return this.medias;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public List<LocalMediaFolder> readLocalFolders()
/*     */   {
/*  82 */     if (this.folders == null) {
/*  83 */       this.folders = new ArrayList();
/*     */     }
/*  85 */     return this.folders;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocalMedia> readSelectLocalMedias()
/*     */   {
/*  93 */     return this.selectedImages;
/*     */   }
/*     */   
/*     */   public void clearLocalFolders()
/*     */   {
/*  98 */     if (this.folders != null)
/*  99 */       this.folders.clear();
/*     */   }
/*     */   
/*     */   public void clearLocalMedia() {
/* 103 */     if (this.medias != null)
/* 104 */       this.medias.clear();
/* 105 */     DebugUtil.i("ImagesObservable:", "clearLocalMedia success!");
/*     */   }
/*     */   
/*     */   public void clearSelectedLocalMedia() {
/* 109 */     if (this.selectedImages != null) {
/* 110 */       this.selectedImages.clear();
/*     */     }
/*     */   }
/*     */   
/*     */   public void add(ObserverListener observerListener) {
/* 115 */     this.observers.add(observerListener);
/*     */   }
/*     */   
/*     */   public void remove(ObserverListener observerListener)
/*     */   {
/* 120 */     if (this.observers.contains(observerListener)) {
/* 121 */       this.observers.remove(observerListener);
/*     */     }
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\observable\ImagesObservable.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */