/*     */ package com.luck.picture.lib.tools;
/*     */ 
/*     */ import android.app.Activity;
/*     */ import android.content.Context;
/*     */ import android.content.res.Resources;
/*     */ import android.os.Build.VERSION;
/*     */ import android.view.View;
/*     */ import android.view.ViewGroup;
/*     */ import android.view.Window;
/*     */ import android.widget.LinearLayout.LayoutParams;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ToolbarUtil
/*     */ {
/*     */   private static final int DEFAULT_STATUS_BAR_ALPHA = 15;
/*     */   
/*     */   public static void setColor(Activity activity, int color, int statusBarAlpha)
/*     */   {
/*  27 */     if (Build.VERSION.SDK_INT >= 21) {
/*  28 */       activity.getWindow().addFlags(Integer.MIN_VALUE);
/*  29 */       activity.getWindow().clearFlags(67108864);
/*  30 */       activity.getWindow().setStatusBarColor(calculateStatusColor(color, statusBarAlpha));
/*  31 */     } else if (Build.VERSION.SDK_INT >= 19) {
/*  32 */       activity.getWindow().addFlags(67108864);
/*     */       
/*  34 */       View statusView = createStatusBarView(activity, color, statusBarAlpha);
/*     */       
/*  36 */       ViewGroup decorView = (ViewGroup)activity.getWindow().getDecorView();
/*  37 */       decorView.addView(statusView);
/*  38 */       setRootView(activity);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private static void setRootView(Activity activity)
/*     */   {
/*  46 */     ViewGroup rootView = (ViewGroup)((ViewGroup)activity.findViewById(16908290)).getChildAt(0);
/*  47 */     if (rootView != null) {
/*  48 */       rootView.setFitsSystemWindows(true);
/*  49 */       rootView.setClipToPadding(true);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private static View createStatusBarView(Activity activity, int color, int alpha)
/*     */   {
/*  58 */     View statusBarView = new View(activity);
/*     */     
/*  60 */     LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, getStatusBarHeight(activity));
/*  61 */     statusBarView.setLayoutParams(params);
/*  62 */     statusBarView.setBackgroundColor(calculateStatusColor(color, alpha));
/*  63 */     return statusBarView;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int getStatusBarHeight(Context context)
/*     */   {
/*  71 */     int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
/*  72 */     return context.getResources().getDimensionPixelSize(resourceId);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private static int calculateStatusColor(int color, int alpha)
/*     */   {
/*  79 */     float a = 1.0F - alpha / 255.0F;
/*  80 */     int red = color >> 16 & 0xFF;
/*  81 */     int green = color >> 8 & 0xFF;
/*  82 */     int blue = color & 0xFF;
/*  83 */     red = (int)(red * a + 0.5D);
/*  84 */     green = (int)(green * a + 0.5D);
/*  85 */     blue = (int)(blue * a + 0.5D);
/*  86 */     return 0xFF000000 | red << 16 | green << 8 | blue;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setColor(Activity activity, int color)
/*     */   {
/*  93 */     setColor(activity, color, 15);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setColorNoTranslucent(Activity activity, int color)
/*     */   {
/* 100 */     setColor(activity, color, 0);
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\tools\ToolbarUtil.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */