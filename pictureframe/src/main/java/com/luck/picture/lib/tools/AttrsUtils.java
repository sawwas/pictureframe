/*    */ package com.luck.picture.lib.tools;
/*    */ 
/*    */ import android.content.Context;
/*    */ import android.content.res.TypedArray;
/*    */ import android.graphics.drawable.Drawable;
/*    */ import android.util.TypedValue;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class AttrsUtils
/*    */ {
/*    */   public static int getTypeValueColor(Context mContext, int attr)
/*    */   {
/* 26 */     TypedValue typedValue = new TypedValue();
/* 27 */     int[] attribute = { attr };
/* 28 */     TypedArray array = mContext.obtainStyledAttributes(typedValue.resourceId, attribute);
/* 29 */     int color = array.getColor(0, -1);
/* 30 */     array.recycle();
/* 31 */     return color;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static boolean getTypeValueBoolean(Context mContext, int attr)
/*    */   {
/* 42 */     TypedValue typedValue = new TypedValue();
/* 43 */     int[] attribute = { attr };
/* 44 */     TypedArray array = mContext.obtainStyledAttributes(typedValue.resourceId, attribute);
/* 45 */     boolean statusFont = array.getBoolean(0, false);
/* 46 */     array.recycle();
/* 47 */     return statusFont;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static Drawable getTypeValuePopWindowImg(Context mContext, int attr)
/*    */   {
/* 58 */     TypedValue typedValue = new TypedValue();
/* 59 */     int[] attribute = { attr };
/* 60 */     TypedArray array = mContext.obtainStyledAttributes(typedValue.resourceId, attribute);
/* 61 */     Drawable drawable = array.getDrawable(0);
/* 62 */     array.recycle();
/* 63 */     return drawable;
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\tools\AttrsUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */