/*    */ package com.luck.picture.lib.entity;
/*    */ 
/*    */ import android.os.Parcel;
/*    */ import android.os.Parcelable;
/*    */ import android.os.Parcelable.Creator;
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class EventEntity
/*    */   implements Parcelable
/*    */ {
/*    */   public int what;
/*    */   public int position;
/* 20 */   public List<LocalMedia> medias = new ArrayList();
/*    */   
/*    */ 
/*    */   public EventEntity() {}
/*    */   
/*    */ 
/*    */   public EventEntity(int what)
/*    */   {
/* 28 */     this.what = what;
/*    */   }
/*    */   
/*    */   public EventEntity(int what, List<LocalMedia> medias)
/*    */   {
/* 33 */     this.what = what;
/* 34 */     this.medias = medias;
/*    */   }
/*    */   
/*    */   public EventEntity(int what, int position)
/*    */   {
/* 39 */     this.what = what;
/* 40 */     this.position = position;
/*    */   }
/*    */   
/*    */   public EventEntity(int what, List<LocalMedia> medias, int position)
/*    */   {
/* 45 */     this.what = what;
/* 46 */     this.position = position;
/* 47 */     this.medias = medias;
/*    */   }
/*    */   
/*    */   public int describeContents()
/*    */   {
/* 52 */     return 0;
/*    */   }
/*    */   
/*    */   public void writeToParcel(Parcel dest, int flags)
/*    */   {
/* 57 */     dest.writeInt(this.what);
/* 58 */     dest.writeInt(this.position);
/* 59 */     dest.writeTypedList(this.medias);
/*    */   }
/*    */   
/*    */   protected EventEntity(Parcel in) {
/* 63 */     this.what = in.readInt();
/* 64 */     this.position = in.readInt();
/* 65 */     this.medias = in.createTypedArrayList(LocalMedia.CREATOR);
/*    */   }
/*    */   
/* 68 */   public static final Parcelable.Creator<EventEntity> CREATOR = new Parcelable.Creator()
/*    */   {
/*    */     public EventEntity createFromParcel(Parcel source) {
/* 71 */       return new EventEntity(source);
/*    */     }
/*    */     
/*    */     public EventEntity[] newArray(int size)
/*    */     {
/* 76 */       return new EventEntity[size];
/*    */     }
/*    */   };
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\entity\EventEntity.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */