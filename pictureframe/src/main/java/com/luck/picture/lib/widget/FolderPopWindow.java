/*     */
package com.luck.picture.lib.widget;
/*     */
/*     */

import android.content.Context;
/*     */ import android.content.res.Resources;
/*     */ import android.graphics.Rect;
/*     */ import android.graphics.drawable.ColorDrawable;
/*     */ import android.graphics.drawable.Drawable;
/*     */ import android.os.Build;
import android.os.Build.VERSION;
/*     */ import android.os.Handler;
/*     */ import android.support.v4.content.ContextCompat;
/*     */ import android.support.v7.widget.RecyclerView;
/*     */ import android.util.DisplayMetrics;
/*     */ import android.view.LayoutInflater;
/*     */ import android.view.View;
/*     */ import android.view.View.OnClickListener;
/*     */ import android.view.animation.Animation;
/*     */ import android.view.animation.Animation.AnimationListener;
/*     */ import android.view.animation.AnimationUtils;
/*     */ import android.widget.LinearLayout;
/*     */ import android.widget.PopupWindow;
/*     */ import android.widget.TextView;
/*     */ import com.luck.picture.lib.R;
import com.luck.picture.lib.R.anim;
/*     */ import com.luck.picture.lib.R.attr;
/*     */ import com.luck.picture.lib.R.color;
/*     */ import com.luck.picture.lib.R.id;
/*     */ import com.luck.picture.lib.R.style;
/*     */ import com.luck.picture.lib.adapter.PictureAlbumDirectoryAdapter;
/*     */ import com.luck.picture.lib.adapter.PictureAlbumDirectoryAdapter.OnItemClickListener;
/*     */ import com.luck.picture.lib.decoration.RecycleViewDivider;
/*     */ import com.luck.picture.lib.entity.LocalMedia;
/*     */ import com.luck.picture.lib.entity.LocalMediaFolder;
/*     */ import com.luck.picture.lib.tools.AttrsUtils;
/*     */ import com.luck.picture.lib.tools.DebugUtil;
/*     */ import com.luck.picture.lib.tools.ScreenUtils;
/*     */ import com.luck.picture.lib.tools.StringUtils;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;

/*     */
/*     */ public class FolderPopWindow extends PopupWindow implements View.OnClickListener
        /*     */ {
    /*     */   private Context context;
    /*     */   private View window;
    /*     */   private RecyclerView recyclerView;
    /*     */   private PictureAlbumDirectoryAdapter adapter;
    /*     */   private Animation animationIn;
    /*     */   private Animation animationOut;
    /*  47 */   private boolean isDismiss = false;
    /*     */   private LinearLayout id_ll_root;
    /*     */   private TextView picture_title;
    /*     */   private Drawable drawableUp;
    /*     */   private Drawable drawableDown;
    /*     */   private int mimeType;

    /*     */
    /*  54 */
    public FolderPopWindow(Context context, int mimeType) {
        this.context = context;
        /*  55 */
        this.mimeType = mimeType;
        /*  56 */
        this.window = LayoutInflater.from(context).inflate(com.luck.picture.lib.R.layout.picture_window_folder, null);
        /*  57 */
        setContentView(this.window);
        /*  58 */
        setWidth(ScreenUtils.getScreenWidth(context));
        /*  59 */
        setHeight(ScreenUtils.getScreenHeight(context));
        /*  60 */
        setAnimationStyle(R.style.WindowStyle);
        /*  61 */
        setFocusable(true);
        /*  62 */
        setOutsideTouchable(true);
        /*  63 */
        update();
        /*  64 */
        setBackgroundDrawable(new ColorDrawable(android.graphics.Color.argb(123, 0, 0, 0)));
        /*  65 */
        this.drawableUp = AttrsUtils.getTypeValuePopWindowImg(context, R.attr.picture_arrow_up_icon);
        /*  66 */
        this.drawableDown = AttrsUtils.getTypeValuePopWindowImg(context, R.attr.picture_arrow_down_icon);
        /*  67 */
        this.animationIn = AnimationUtils.loadAnimation(context, R.anim.photo_album_show);
        /*  68 */
        this.animationOut = AnimationUtils.loadAnimation(context, R.anim.photo_album_dismiss);
        /*  69 */
        initView();
        /*     */
    }

    /*     */
    /*     */
    public void initView() {
        /*  73 */
        this.id_ll_root = ((LinearLayout) this.window.findViewById(R.id.id_ll_root));
        /*  74 */
        this.adapter = new PictureAlbumDirectoryAdapter(this.context);
        /*  75 */
        this.recyclerView = ((RecyclerView) this.window.findViewById(R.id.folder_list));
        /*  76 */
        this.recyclerView.getLayoutParams().height = ((int) (ScreenUtils.getScreenHeight(this.context) * 0.6D));
        /*  77 */
        this.recyclerView.addItemDecoration(new RecycleViewDivider(this.context, 0,
                /*  78 */       ScreenUtils.dip2px(this.context, 0.0F), ContextCompat.getColor(this.context, R.color.transparent)));
        /*  79 */
        this.recyclerView.setLayoutManager(new android.support.v7.widget.LinearLayoutManager(this.context));
        /*  80 */
        this.recyclerView.setAdapter(this.adapter);
        /*  81 */
        this.id_ll_root.setOnClickListener(this);
        /*     */
    }

    /*     */
    /*     */
    public void bindFolder(List<LocalMediaFolder> folders) {
        /*  85 */
        this.adapter.setMimeType(this.mimeType);
        /*  86 */
        this.adapter.bindFolderData(folders);
        /*     */
    }

    /*     */
    /*     */
    public void setPictureTitleView(TextView picture_title) {
        /*  90 */
        this.picture_title = picture_title;
        /*     */
    }

    /*     */
    /*     */
    public void showAsDropDown(View anchor)
    /*     */ {
        /*     */
        try {
            /*  96 */
            if (Build.VERSION.SDK_INT >= 24) {
                /*  97 */
                Rect rect = new Rect();
                /*  98 */
                anchor.getGlobalVisibleRect(rect);
                /*  99 */
                int h = anchor.getResources().getDisplayMetrics().heightPixels - rect.bottom;
                /* 100 */
                setHeight(h);
                /*     */
            }
            /* 102 */
            super.showAsDropDown(anchor);
            /* 103 */
            this.isDismiss = false;
            /* 104 */
            this.recyclerView.startAnimation(this.animationIn);
            /* 105 */
            StringUtils.modifyTextViewDrawable(this.picture_title, this.drawableUp, 2);
            /*     */
        } catch (Exception e) {
            /* 107 */
            e.printStackTrace();
            /*     */
        }
        /*     */
    }

    /*     */
    /*     */
    public void setOnItemClickListener(PictureAlbumDirectoryAdapter.OnItemClickListener onItemClickListener) {
        /* 112 */
        this.adapter.setOnItemClickListener(onItemClickListener);
        /*     */
    }

    /*     */
    /*     */
    public void dismiss()
    /*     */ {
        /* 117 */
        DebugUtil.i("PopWindow:", "dismiss");
        /* 118 */
        if (this.isDismiss) {
            /* 119 */
            return;
            /*     */
        }
        /* 121 */
        StringUtils.modifyTextViewDrawable(this.picture_title, this.drawableDown, 2);
        /* 122 */
        this.isDismiss = true;
        /* 123 */
        this.recyclerView.startAnimation(this.animationOut);
        /* 124 */
        dismiss();
        /* 125 */
        this.animationOut.setAnimationListener(new Animation.AnimationListener()
                /*     */ {
            /*     */
            public void onAnimationStart(Animation animation) {
            }

            /*     */
            /*     */
            /*     */
            /*     */
            public void onAnimationEnd(Animation animation)
            /*     */ {
                /* 133 */
                FolderPopWindow.this.isDismiss = false;
                /* 134 */
                if (Build.VERSION.SDK_INT <= 16) {
                    /* 135 */
                    FolderPopWindow.this.dismiss4Pop();
                    /*     */
                } else {
                    /* 137 */
                    FolderPopWindow.this.dismiss();
                    /*     */
                }
                /*     */
            }

            /*     */
            /*     */
            /*     */
            /*     */
            /*     */
            public void onAnimationRepeat(Animation animation) {
            }
            /*     */
        });
        /*     */
    }

    /*     */
    /*     */
    /*     */
    /*     */
    private void dismiss4Pop()
    /*     */ {
        /* 152 */
        new Handler().post(new Runnable()
                /*     */ {
            /*     */
            public void run() {
                /* 155 */
                FolderPopWindow.this.dismiss();
                /*     */
            }
            /*     */
        });
        /*     */
    }

    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    public void notifyDataCheckedStatus(List<LocalMedia> medias)
    /*     */ {
        /*     */
        try
            /*     */ {
            /* 167 */
            List<LocalMediaFolder> folders = this.adapter.getFolderData();
            /* 168 */
            for (LocalMediaFolder folder : folders) {
                /* 169 */
                folder.setCheckedNum(0);
                /*     */
            }
            /* 171 */
            /* 172 */
            /*     */
            LocalMediaFolder folder;
            /*     */
            int num;
            /*     */
            String path;
            if (medias.size() > 0)
                for (folders.iterator(); folders.iterator().hasNext(); ) {
                    folder = folders.iterator().next();
                    num = 0;
                    List<LocalMedia> images = folder.getImages();
                    /* 175 */
                    for (LocalMedia media : images) {
                        /* 176 */
                        path = media.getPath();
                        /* 177 */
                        for (LocalMedia m : medias)
                            /* 178 */
                            if (path.equals(m.getPath())) {
                                /* 179 */
                                num++;
                                /* 180 */
                                folder.setCheckedNum(num);
                                /*     */
                            }
                        /*     */
                    }
                }

            /* 186 */
            this.adapter.bindFolderData(folders);
            /*     */
        } catch (Exception e) {
            /* 188 */
            e.printStackTrace();
            /*     */
        }
        /*     */
    }

    /*     */
    /*     */
    public void onClick(View v)
    /*     */ {
        /* 194 */
        int id = v.getId();
        /* 195 */
        if (id == R.id.id_ll_root) {
            /* 196 */
            dismiss();
            /*     */
        }
        /*     */
    }
    /*     */
}


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\widget\FolderPopWindow.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */