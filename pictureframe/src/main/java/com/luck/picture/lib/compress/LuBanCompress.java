/*     */ package com.luck.picture.lib.compress;
/*     */ 
/*     */ import android.content.Context;
/*     */ import android.util.Log;
/*     */ import com.luck.picture.lib.entity.LocalMedia;
/*     */ import java.io.File;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LuBanCompress
/*     */   implements CompressInterface
/*     */ {
/*     */   private List<LocalMedia> images;
/*     */   private CompressInterface.CompressListener listener;
/*     */   private Context context;
/*     */   private LubanOptions options;
/*  24 */   private ArrayList<File> files = new ArrayList();
/*     */   
/*     */   public LuBanCompress(Context context, CompressConfig config, List<LocalMedia> images, CompressInterface.CompressListener listener)
/*     */   {
/*  28 */     this.options = config.getLubanOptions();
/*  29 */     this.images = images;
/*  30 */     this.listener = listener;
/*  31 */     this.context = context;
/*     */   }
/*     */   
/*     */   public void compress()
/*     */   {
/*  36 */     if ((this.images == null) || (this.images.isEmpty())) {
/*  37 */       this.listener.onCompressError(this.images, " images is null");
/*  38 */       return;
/*     */     }
/*  40 */     for (LocalMedia image : this.images) {
/*  41 */       if (image == null) {
/*  42 */         this.listener.onCompressError(this.images, " There are pictures of compress  is null.");
/*  43 */         return;
/*     */       }
/*  45 */       if (image.isCut()) {
/*  46 */         this.files.add(new File(image.getCutPath()));
/*     */       } else {
/*  48 */         this.files.add(new File(image.getPath()));
/*     */       }
/*     */     }
/*  51 */     if (this.images.size() == 1) {
/*  52 */       compressOne();
/*     */     } else {
/*  54 */       compressMulti();
/*     */     }
/*     */   }
/*     */   
/*     */   private void compressOne() {
/*  59 */     Log.i("压缩档次::", this.options.getGrade() + "");
/*  60 */     Luban.compress(this.context, (File)this.files.get(0))
/*  61 */       .putGear(this.options.getGrade())
/*  62 */       .setMaxHeight(this.options.getMaxHeight())
/*  63 */       .setMaxWidth(this.options.getMaxWidth())
/*  64 */       .setMaxSize(this.options.getMaxSize() / 1000)
/*  65 */       .launch(new OnCompressListener()
/*     */       {
/*     */         public void onStart() {}
/*     */         
/*     */ 
/*     */ 
/*     */         public void onSuccess(File file)
/*     */         {
/*  72 */           LocalMedia image = (LocalMedia)LuBanCompress.this.images.get(0);
/*  73 */           image.setCompressPath(file.getPath());
/*  74 */           image.setCompressed(true);
/*  75 */           LuBanCompress.this.listener.onCompressSuccess(LuBanCompress.this.images);
/*     */         }
/*     */         
/*     */         public void onError(Throwable e)
/*     */         {
/*  80 */           LuBanCompress.this.listener.onCompressError(LuBanCompress.this.images, e.getMessage() + " is compress failures");
/*     */         }
/*     */       });
/*     */   }
/*     */   
/*     */   private void compressMulti() {
/*  86 */     Log.i("压缩档次::", this.options.getGrade() + "");
/*  87 */     Luban.compress(this.context, this.files)
/*  88 */       .putGear(this.options.getGrade())
/*  89 */       .setMaxSize(this.options
/*  90 */       .getMaxSize() / 1000)
/*  91 */       .setMaxHeight(this.options.getMaxHeight())
/*  92 */       .setMaxWidth(this.options.getMaxWidth())
/*  93 */       .launch(new OnMultiCompressListener()
/*     */       {
/*     */         public void onStart() {}
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */         public void onSuccess(List<File> fileList)
/*     */         {
/* 101 */           LuBanCompress.this.handleCompressCallBack(fileList);
/*     */         }
/*     */         
/*     */         public void onError(Throwable e)
/*     */         {
/* 106 */           LuBanCompress.this.listener.onCompressError(LuBanCompress.this.images, e.getMessage() + " is compress failures");
/*     */         }
/*     */       });
/*     */   }
/*     */   
/*     */   private void handleCompressCallBack(List<File> files) {
/* 112 */     int i = 0; for (int j = this.images.size(); i < j; i++) {
/* 113 */       String path = ((File)files.get(i)).getPath();
/* 114 */       LocalMedia image = (LocalMedia)this.images.get(i);
/*     */       
/* 116 */       if ((path != null) && (path.startsWith("http"))) {
/* 117 */         image.setCompressPath("");
/*     */       } else {
/* 119 */         image.setCompressed(true);
/* 120 */         image.setCompressPath(path);
/*     */       }
/*     */     }
/* 123 */     this.listener.onCompressSuccess(this.images);
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\compress\LuBanCompress.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */