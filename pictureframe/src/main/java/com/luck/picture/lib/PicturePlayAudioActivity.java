/*     */ package com.luck.picture.lib;
/*     */ 
/*     */ import android.content.Intent;
/*     */ import android.media.MediaPlayer;
/*     */ import android.os.Bundle;
/*     */ import android.os.Handler;
/*     */ import android.view.View;
/*     */ import android.view.Window;
/*     */ import android.widget.SeekBar;
/*     */ import android.widget.TextView;
/*     */ import com.luck.picture.lib.tools.DateUtils;
/*     */ 
/*     */ public class PicturePlayAudioActivity extends PictureBaseActivity implements android.view.View.OnClickListener
/*     */ {
/*     */   private String audio_path;
/*     */   private MediaPlayer mediaPlayer;
/*     */   private SeekBar musicSeekBar;
/*  18 */   private boolean isPlayAudio = false;
/*     */   private TextView tv_PlayPause;
/*     */   private TextView tv_Stop;
/*     */   private TextView tv_Quit;
/*     */   
/*     */   protected void onCreate(Bundle savedInstanceState) {
/*  24 */     getWindow().setFlags(1024, 1024);
/*     */     
/*  26 */     super.onCreate(savedInstanceState);
/*  27 */     setContentView(R.layout.activity_picture_play_audio);
/*  28 */     this.audio_path = getIntent().getStringExtra("audio_path");
/*  29 */     this.tv_musicStatus = ((TextView)findViewById(R.id.tv_musicStatus));
/*  30 */     this.tv_musicTime = ((TextView)findViewById(R.id.tv_musicTime));
/*  31 */     this.musicSeekBar = ((SeekBar)findViewById(R.id.musicSeekBar));
/*  32 */     this.tv_musicTotal = ((TextView)findViewById(R.id.tv_musicTotal));
/*  33 */     this.tv_PlayPause = ((TextView)findViewById(R.id.tv_PlayPause));
/*  34 */     this.tv_Stop = ((TextView)findViewById(R.id.tv_Stop));
/*  35 */     this.tv_Quit = ((TextView)findViewById(R.id.tv_Quit));
/*  36 */     this.handler.postDelayed(new Runnable()
/*     */     {
/*     */ 
/*  39 */       public void run() { PicturePlayAudioActivity.this.initPlayer(PicturePlayAudioActivity.this.audio_path); } }, 30L);
/*     */     
/*     */ 
/*  42 */     this.tv_PlayPause.setOnClickListener(this);
/*  43 */     this.tv_Stop.setOnClickListener(this);
/*  44 */     this.tv_Quit.setOnClickListener(this);
/*  45 */     this.musicSeekBar.setOnSeekBarChangeListener(new android.widget.SeekBar.OnSeekBarChangeListener()
/*     */     {
/*     */       public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
/*  48 */         if (fromUser == true) {
/*  49 */           PicturePlayAudioActivity.this.mediaPlayer.seekTo(progress);
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*     */       public void onStartTrackingTouch(SeekBar seekBar) {}
/*     */       
/*     */ 
/*     */       public void onStopTrackingTouch(SeekBar seekBar) {}
/*     */     });
/*     */   }
/*     */   
/*     */   private TextView tv_musicStatus;
/*     */   private TextView tv_musicTotal;
/*     */   private TextView tv_musicTime;
/*  64 */   public Handler handler = new Handler();
/*  65 */   public Runnable runnable = new Runnable()
/*     */   {
/*     */     public void run() {
/*     */       try {
/*  69 */         if (PicturePlayAudioActivity.this.mediaPlayer != null) {
/*  70 */           PicturePlayAudioActivity.this.tv_musicTime.setText(DateUtils.timeParse(PicturePlayAudioActivity.this.mediaPlayer.getCurrentPosition()));
/*  71 */           PicturePlayAudioActivity.this.musicSeekBar.setProgress(PicturePlayAudioActivity.this.mediaPlayer.getCurrentPosition());
/*  72 */           PicturePlayAudioActivity.this.musicSeekBar.setMax(PicturePlayAudioActivity.this.mediaPlayer.getDuration());
/*  73 */           PicturePlayAudioActivity.this.tv_musicTotal.setText(DateUtils.timeParse(PicturePlayAudioActivity.this.mediaPlayer.getDuration()));
/*  74 */           PicturePlayAudioActivity.this.handler.postDelayed(PicturePlayAudioActivity.this.runnable, 200L);
/*     */         }
/*     */       } catch (Exception e) {
/*  77 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */   };
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initPlayer(String path)
/*     */   {
/*  88 */     this.mediaPlayer = new MediaPlayer();
/*     */     try {
/*  90 */       this.mediaPlayer.setDataSource(path);
/*  91 */       this.mediaPlayer.prepare();
/*  92 */       this.mediaPlayer.setLooping(true);
/*  93 */       playAudio();
/*     */     } catch (Exception e) {
/*  95 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */   public void onClick(View v)
/*     */   {
/* 101 */     int i = v.getId();
/* 102 */     if (i == R.id.tv_PlayPause) {
/* 103 */       playAudio();
/*     */     }
/*     */     
/* 106 */     if (i == R.id.tv_Stop) {
/* 107 */       this.tv_musicStatus.setText(getString(R.string.picture_stop_audio));
/* 108 */       this.tv_PlayPause.setText(getString(R.string.picture_play_audio));
/* 109 */       stop(this.audio_path);
/*     */     }
/*     */     
/* 112 */     if (i == R.id.tv_Quit) {
/* 113 */       this.handler.removeCallbacks(this.runnable);
/* 114 */       new Handler().postDelayed(new Runnable()
/*     */       {
/*     */ 
/* 117 */         public void run() { PicturePlayAudioActivity.this.stop(PicturePlayAudioActivity.this.audio_path); } }, 30L);
/*     */       
/*     */       try
/*     */       {
/* 121 */         closeActivity();
/*     */       } catch (Exception e) {
/* 123 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void playAudio()
/*     */   {
/* 133 */     if (this.mediaPlayer != null) {
/* 134 */       this.musicSeekBar.setProgress(this.mediaPlayer.getCurrentPosition());
/* 135 */       this.musicSeekBar.setMax(this.mediaPlayer.getDuration());
/*     */     }
/* 137 */     String ppStr = this.tv_PlayPause.getText().toString();
/* 138 */     if (ppStr.equals(getString(R.string.picture_play_audio))) {
/* 139 */       this.tv_PlayPause.setText(getString(R.string.picture_pause_audio));
/* 140 */       this.tv_musicStatus.setText(getString(R.string.picture_play_audio));
/* 141 */       playOrPause();
/*     */     } else {
/* 143 */       this.tv_PlayPause.setText(getString(R.string.picture_play_audio));
/* 144 */       this.tv_musicStatus.setText(getString(R.string.picture_pause_audio));
/* 145 */       playOrPause();
/*     */     }
/* 147 */     if (!this.isPlayAudio) {
/* 148 */       this.handler.post(this.runnable);
/* 149 */       this.isPlayAudio = true;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void stop(String path)
/*     */   {
/* 159 */     if (this.mediaPlayer != null) {
/*     */       try {
/* 161 */         this.mediaPlayer.stop();
/* 162 */         this.mediaPlayer.reset();
/* 163 */         this.mediaPlayer.setDataSource(path);
/* 164 */         this.mediaPlayer.prepare();
/* 165 */         this.mediaPlayer.seekTo(0);
/*     */       } catch (Exception e) {
/* 167 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void playOrPause()
/*     */   {
/*     */     try
/*     */     {
/* 177 */       if (this.mediaPlayer != null) {
/* 178 */         if (this.mediaPlayer.isPlaying()) {
/* 179 */           this.mediaPlayer.pause();
/*     */         } else {
/* 181 */           this.mediaPlayer.start();
/*     */         }
/*     */       }
/*     */     } catch (Exception e) {
/* 185 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */   public void onBackPressed()
/*     */   {
/* 191 */     super.onBackPressed();
/* 192 */     closeActivity();
/*     */   }
/*     */   
/*     */   protected void onDestroy()
/*     */   {
/* 197 */     super.onDestroy();
/* 198 */     if ((this.mediaPlayer != null) && (this.handler != null)) {
/* 199 */       this.handler.removeCallbacks(this.runnable);
/* 200 */       this.mediaPlayer.release();
/* 201 */       this.mediaPlayer = null;
/*     */     }
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\PicturePlayAudioActivity.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */