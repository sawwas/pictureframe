/*     */ package com.luck.picture.lib.permissions;
/*     */ 
/*     */ import android.annotation.TargetApi;
/*     */ import android.app.Activity;
/*     */ import android.app.FragmentManager;
/*     */ import android.app.FragmentTransaction;
/*     */ import android.os.Build.VERSION;
/*     */ import android.support.annotation.NonNull;
/*     */ import android.text.TextUtils;
/*     */ import io.reactivex.Observable;
/*     */ import io.reactivex.ObservableSource;
/*     */ import io.reactivex.ObservableTransformer;
/*     */ import io.reactivex.functions.Function;
/*     */ import io.reactivex.subjects.PublishSubject;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RxPermissions
/*     */ {
/*     */   public static final String TAG = "RxPermissions";
/*  29 */   public static final Object TRIGGER = new Object();
/*     */   public RxPermissionsFragment mRxPermissionsFragment;
/*     */   
/*     */   public RxPermissions(@NonNull Activity activity)
/*     */   {
/*  34 */     this.mRxPermissionsFragment = getRxPermissionsFragment(activity);
/*     */   }
/*     */   
/*     */   private RxPermissionsFragment getRxPermissionsFragment(Activity activity) {
/*  38 */     RxPermissionsFragment rxPermissionsFragment = null;
/*     */     try {
/*  40 */       rxPermissionsFragment = findRxPermissionsFragment(activity);
/*  41 */       boolean isNewInstance = rxPermissionsFragment == null;
/*  42 */       if (isNewInstance) {
/*  43 */         rxPermissionsFragment = new RxPermissionsFragment();
/*  44 */         FragmentManager fragmentManager = activity.getFragmentManager();
/*  45 */         fragmentManager
/*  46 */           .beginTransaction()
/*  47 */           .add(rxPermissionsFragment, "RxPermissions")
/*  48 */           .commitAllowingStateLoss();
/*  49 */         fragmentManager.executePendingTransactions();
/*     */       }
/*     */     } catch (Exception e) {
/*  52 */       e.printStackTrace();
/*     */     }
/*  54 */     return rxPermissionsFragment;
/*     */   }
/*     */   
/*     */   private RxPermissionsFragment findRxPermissionsFragment(Activity activity) {
/*  58 */     return (RxPermissionsFragment)activity.getFragmentManager().findFragmentByTag("RxPermissions");
/*     */   }
/*     */   
/*     */   public void setLogging(boolean logging) {
/*  62 */     this.mRxPermissionsFragment.setLogging(logging);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public <T> ObservableTransformer<T, Boolean> ensure(final String... permissions)
/*     */   {
/*  74 */     new ObservableTransformer()
/*     */     {
/*     */ 
/*     */       public ObservableSource<Boolean> apply(Observable<T> o)
/*     */       {
/*     */ 
/*  80 */         RxPermissions.this.request(o, permissions).buffer(permissions.length).flatMap(new Function()
/*     */         {
/*     */           public ObservableSource<Boolean> apply(List<Permission> permissions) throws Exception {
/*  83 */             if (permissions.isEmpty())
/*     */             {
/*     */ 
/*     */ 
/*  87 */               return Observable.empty();
/*     */             }
/*     */             
/*  90 */             for (Permission p : permissions) {
/*  91 */               if (!p.granted) {
/*  92 */                 return Observable.just(Boolean.valueOf(false));
/*     */               }
/*     */             }
/*  95 */             return Observable.just(Boolean.valueOf(true));
/*     */           }
/*     */         });
/*     */       }
/*     */     };
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public <T> ObservableTransformer<T, Permission> ensureEach(final String... permissions)
/*     */   {
/* 111 */     new ObservableTransformer()
/*     */     {
/*     */       public ObservableSource<Permission> apply(Observable<T> o) {
/* 114 */         return RxPermissions.this.request(o, permissions);
/*     */       }
/*     */     };
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Observable<Boolean> request(String... permissions)
/*     */   {
/* 125 */     return Observable.just(TRIGGER).compose(ensure(permissions));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Observable<Permission> requestEach(String... permissions)
/*     */   {
/* 134 */     return Observable.just(TRIGGER).compose(ensureEach(permissions));
/*     */   }
/*     */   
/*     */   private Observable<Permission> request(Observable<?> trigger, final String... permissions) {
/* 138 */     if ((permissions == null) || (permissions.length == 0)) {
/* 139 */       throw new IllegalArgumentException("RxPermissions.request/requestEach requires at least one input permission");
/*     */     }
/*     */     
/* 142 */     oneOf(trigger, pending(permissions)).flatMap(new Function()
/*     */     {
/*     */       public Observable<Permission> apply(Object o) throws Exception {
/* 145 */         return RxPermissions.this.requestImplementation(permissions);
/*     */       }
/*     */     });
/*     */   }
/*     */   
/*     */   private Observable<?> pending(String... permissions) {
/* 151 */     for (String p : permissions) {
/* 152 */       if (!this.mRxPermissionsFragment.containsByPermission(p)) {
/* 153 */         return Observable.empty();
/*     */       }
/*     */     }
/* 156 */     return Observable.just(TRIGGER);
/*     */   }
/*     */   
/*     */   private Observable<?> oneOf(Observable<?> trigger, Observable<?> pending) {
/* 160 */     if (trigger == null) {
/* 161 */       return Observable.just(TRIGGER);
/*     */     }
/* 163 */     return Observable.merge(trigger, pending);
/*     */   }
/*     */   
/*     */   @TargetApi(23)
/*     */   private Observable<Permission> requestImplementation(String... permissions) {
/* 168 */     List<Observable<Permission>> list = new ArrayList(permissions.length);
/* 169 */     List<String> unrequestedPermissions = new ArrayList();
/*     */     
/*     */ 
/*     */ 
/* 173 */     for (String permission : permissions) {
/* 174 */       this.mRxPermissionsFragment.log("Requesting permission " + permission);
/* 175 */       if (isGranted(permission))
/*     */       {
/*     */ 
/* 178 */         list.add(Observable.just(new Permission(permission, true, false)));
/*     */ 
/*     */ 
/*     */       }
/* 182 */       else if (isRevoked(permission))
/*     */       {
/* 184 */         list.add(Observable.just(new Permission(permission, false, false)));
/*     */       }
/*     */       else
/*     */       {
/* 188 */         PublishSubject<Permission> subject = this.mRxPermissionsFragment.getSubjectByPermission(permission);
/*     */         
/* 190 */         if (subject == null) {
/* 191 */           unrequestedPermissions.add(permission);
/* 192 */           subject = PublishSubject.create();
/* 193 */           this.mRxPermissionsFragment.setSubjectForPermission(permission, subject);
/*     */         }
/*     */         
/* 196 */         list.add(subject);
/*     */       }
/*     */     }
/* 199 */     if (!unrequestedPermissions.isEmpty()) {
/* 200 */       String[] unrequestedPermissionsArray = (String[])unrequestedPermissions.toArray(new String[unrequestedPermissions.size()]);
/* 201 */       requestPermissionsFromFragment(unrequestedPermissionsArray);
/*     */     }
/* 203 */     return Observable.concat(Observable.fromIterable(list));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Observable<Boolean> shouldShowRequestPermissionRationale(Activity activity, String... permissions)
/*     */   {
/* 220 */     if (!isMarshmallow()) {
/* 221 */       return Observable.just(Boolean.valueOf(false));
/*     */     }
/* 223 */     return Observable.just(Boolean.valueOf(shouldShowRequestPermissionRationaleImplementation(activity, permissions)));
/*     */   }
/*     */   
/*     */   @TargetApi(23)
/*     */   private boolean shouldShowRequestPermissionRationaleImplementation(Activity activity, String... permissions) {
/* 228 */     for (String p : permissions) {
/* 229 */       if ((!isGranted(p)) && (!activity.shouldShowRequestPermissionRationale(p))) {
/* 230 */         return false;
/*     */       }
/*     */     }
/* 233 */     return true;
/*     */   }
/*     */   
/*     */   @TargetApi(23)
/*     */   void requestPermissionsFromFragment(String[] permissions) {
/* 238 */     this.mRxPermissionsFragment.log("requestPermissionsFromFragment " + TextUtils.join(", ", permissions));
/* 239 */     this.mRxPermissionsFragment.requestPermissions(permissions);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isGranted(String permission)
/*     */   {
/* 249 */     return (!isMarshmallow()) || (this.mRxPermissionsFragment.isGranted(permission));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isRevoked(String permission)
/*     */   {
/* 259 */     return (isMarshmallow()) && (this.mRxPermissionsFragment.isRevoked(permission));
/*     */   }
/*     */   
/*     */   boolean isMarshmallow() {
/* 263 */     return Build.VERSION.SDK_INT >= 23;
/*     */   }
/*     */   
/*     */   void onRequestPermissionsResult(String[] permissions, int[] grantResults) {
/* 267 */     this.mRxPermissionsFragment.onRequestPermissionsResult(permissions, grantResults, new boolean[permissions.length]);
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\permissions\RxPermissions.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */