package com.luck.picture.lib.compress;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import java.io.File;

public class LubanBuilder
{
    int maxSize;
    int maxWidth;
    int maxHeight;
    File cacheDir;
    Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.JPEG;
    int gear = 3;

    LubanBuilder(File cacheDir)
    {
        this.cacheDir = cacheDir;
    }
}
