/*    */ package com.luck.picture.lib.decoration;
/*    */ 
/*    */ import android.graphics.Rect;
/*    */ import android.support.v7.widget.RecyclerView;
/*    */ import android.support.v7.widget.RecyclerView.ItemDecoration;
/*    */ import android.support.v7.widget.RecyclerView.State;
/*    */ import android.view.View;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class GridSpacingItemDecoration
/*    */   extends RecyclerView.ItemDecoration
/*    */ {
/*    */   private int spanCount;
/*    */   private int spacing;
/*    */   private boolean includeEdge;
/*    */   
/*    */   public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge)
/*    */   {
/* 22 */     this.spanCount = spanCount;
/* 23 */     this.spacing = spacing;
/* 24 */     this.includeEdge = includeEdge;
/*    */   }
/*    */   
/*    */   public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
/*    */   {
/* 29 */     int position = parent.getChildAdapterPosition(view);
/* 30 */     int column = position % this.spanCount;
/* 31 */     if (this.includeEdge) {
/* 32 */       outRect.left = (this.spacing - column * this.spacing / this.spanCount);
/* 33 */       outRect.right = ((column + 1) * this.spacing / this.spanCount);
/* 34 */       if (position < this.spanCount) {
/* 35 */         outRect.top = this.spacing;
/*    */       }
/* 37 */       outRect.bottom = this.spacing;
/*    */     } else {
/* 39 */       outRect.left = (column * this.spacing / this.spanCount);
/* 40 */       outRect.right = (this.spacing - (column + 1) * this.spacing / this.spanCount);
/* 41 */       if (position < this.spanCount) {
/* 42 */         outRect.top = this.spacing;
/*    */       }
/* 44 */       outRect.bottom = this.spacing;
/*    */     }
/*    */   }
/*    */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\decoration\GridSpacingItemDecoration.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */