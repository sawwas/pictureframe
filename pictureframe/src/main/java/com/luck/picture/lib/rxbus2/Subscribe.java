package com.luck.picture.lib.rxbus2;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({java.lang.annotation.ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Subscribe
{
  int code() default -1;
  
  ThreadMode threadMode() default ThreadMode.CURRENT_THREAD;
}


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\rxbus2\Subscribe.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */