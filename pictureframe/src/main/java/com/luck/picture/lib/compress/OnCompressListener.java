package com.luck.picture.lib.compress;

import java.io.File;

public abstract interface OnCompressListener
{
  public abstract void onStart();
  
  public abstract void onSuccess(File paramFile);
  
  public abstract void onError(Throwable paramThrowable);
}


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\compress\OnCompressListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */