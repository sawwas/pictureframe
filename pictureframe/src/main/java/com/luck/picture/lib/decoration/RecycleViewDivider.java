/*     */ package com.luck.picture.lib.decoration;
/*     */ 
/*     */ import android.content.Context;
/*     */ import android.content.res.TypedArray;
/*     */ import android.graphics.Canvas;
/*     */ import android.graphics.Paint;
/*     */ import android.graphics.Paint.Style;
/*     */ import android.graphics.Rect;
/*     */ import android.graphics.drawable.Drawable;
/*     */ import android.support.v4.content.ContextCompat;
/*     */ import android.support.v7.widget.RecyclerView;
/*     */ import android.support.v7.widget.RecyclerView.ItemDecoration;
/*     */ import android.support.v7.widget.RecyclerView.LayoutParams;
/*     */ import android.support.v7.widget.RecyclerView.State;
/*     */ import android.view.View;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RecycleViewDivider
/*     */   extends RecyclerView.ItemDecoration
/*     */ {
/*     */   private Paint mPaint;
/*     */   private Drawable mDivider;
/*  26 */   private int mDividerHeight = 2;
/*     */   private int mOrientation;
/*  28 */   private static final int[] ATTRS = { 16843284 };
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public RecycleViewDivider(Context context, int orientation)
/*     */   {
/*  37 */     if ((orientation != 1) && (orientation != 0)) {
/*  38 */       throw new IllegalArgumentException("请输入正确的参数！");
/*     */     }
/*  40 */     this.mOrientation = orientation;
/*     */     
/*  42 */     TypedArray a = context.obtainStyledAttributes(ATTRS);
/*  43 */     this.mDivider = a.getDrawable(0);
/*  44 */     a.recycle();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public RecycleViewDivider(Context context, int orientation, int drawableId)
/*     */   {
/*  55 */     this(context, orientation);
/*  56 */     this.mDivider = ContextCompat.getDrawable(context, drawableId);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public RecycleViewDivider(Context context, int orientation, int dividerHeight, int dividerColor)
/*     */   {
/*  69 */     this(context, orientation);
/*     */     
/*  71 */     this.mPaint = new Paint(1);
/*  72 */     this.mPaint.setColor(dividerColor);
/*  73 */     this.mPaint.setStyle(Paint.Style.FILL);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
/*     */   {
/*  80 */     super.getItemOffsets(outRect, view, parent, state);
/*  81 */     outRect.set(0, 0, 0, this.mDividerHeight);
/*     */   }
/*     */   
/*     */ 
/*     */   public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state)
/*     */   {
/*  87 */     super.onDraw(c, parent, state);
/*  88 */     if (this.mOrientation == 1) {
/*  89 */       drawVertical(c, parent);
/*     */     } else {
/*  91 */       drawHorizontal(c, parent);
/*     */     }
/*     */   }
/*     */   
/*     */   private void drawHorizontal(Canvas canvas, RecyclerView parent)
/*     */   {
/*  97 */     int left = parent.getPaddingLeft();
/*  98 */     int right = parent.getMeasuredWidth() - parent.getPaddingRight();
/*  99 */     int childSize = parent.getChildCount();
/* 100 */     for (int i = 0; i < childSize; i++) {
/* 101 */       View child = parent.getChildAt(i);
/* 102 */       RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)child.getLayoutParams();
/* 103 */       int top = child.getBottom() + layoutParams.bottomMargin;
/* 104 */       int bottom = top + this.mDividerHeight;
/* 105 */       if (this.mDivider != null) {
/* 106 */         this.mDivider.setBounds(left, top, right, bottom);
/* 107 */         this.mDivider.draw(canvas);
/*     */       }
/* 109 */       if (this.mPaint != null) {
/* 110 */         canvas.drawRect(left, top, right, bottom, this.mPaint);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void drawVertical(Canvas canvas, RecyclerView parent)
/*     */   {
/* 117 */     int top = parent.getPaddingTop();
/* 118 */     int bottom = parent.getMeasuredHeight() - parent.getPaddingBottom();
/* 119 */     int childSize = parent.getChildCount();
/* 120 */     for (int i = 0; i < childSize; i++) {
/* 121 */       View child = parent.getChildAt(i);
/* 122 */       RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)child.getLayoutParams();
/* 123 */       int left = child.getRight() + layoutParams.rightMargin;
/* 124 */       int right = left + this.mDividerHeight;
/* 125 */       if (this.mDivider != null) {
/* 126 */         this.mDivider.setBounds(left, top, right, bottom);
/* 127 */         this.mDivider.draw(canvas);
/*     */       }
/* 129 */       if (this.mPaint != null) {
/* 130 */         canvas.drawRect(left, top, right, bottom, this.mPaint);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\decoration\RecycleViewDivider.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */