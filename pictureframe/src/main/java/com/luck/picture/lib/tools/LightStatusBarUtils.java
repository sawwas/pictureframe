/*     */ package com.luck.picture.lib.tools;
/*     */ 
/*     */ import android.app.Activity;
/*     */ import android.os.Build;
/*     */ import android.os.Build.VERSION;
/*     */ import android.text.TextUtils;
/*     */ import android.view.View;
/*     */ import android.view.Window;
/*     */ import android.view.WindowManager.LayoutParams;
/*     */ import java.lang.reflect.Field;
/*     */ import java.lang.reflect.Method;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LightStatusBarUtils
/*     */ {
/*     */   public static void setLightStatusBar(Activity activity, boolean dark)
/*     */   {
/*  23 */     int availableRomType = RomUtils.getLightStatausBarAvailableRomType();
/*  24 */     switch (availableRomType) {
/*     */     case 1: 
/*  26 */       if (Build.VERSION.SDK_INT <= 19) {
/*  27 */         dark = false;
/*     */       }
/*  29 */       setMIUILightStatusBar(activity, dark);
/*  30 */       break;
/*     */     
/*     */     case 2: 
/*  33 */       if (Build.VERSION.SDK_INT <= 19) {
/*  34 */         dark = false;
/*     */       }
/*  36 */       setFlymeLightStatusBar(activity, dark);
/*  37 */       break;
/*     */     
/*     */     case 3: 
/*  40 */       setAndroidNativeLightStatusBar(activity, dark);
/*  41 */       break;
/*     */     
/*     */ 
/*     */     case 4: 
/*  45 */       setAndroidNativeLightStatusBar(activity, dark);
/*     */     }
/*     */   }
/*     */   
/*     */   private static boolean setMIUILightStatusBar(Activity activity, boolean darkmode)
/*     */   {
/*  51 */     Class<? extends Window> clazz = activity.getWindow().getClass();
/*     */     try {
/*  53 */       int darkModeFlag = 0;
/*  54 */       Class<?> layoutParams = Class.forName("android.view.MiuiWindowManager$LayoutParams");
/*  55 */       Field field = layoutParams.getField("EXTRA_FLAG_STATUS_BAR_DARK_MODE");
/*  56 */       darkModeFlag = field.getInt(layoutParams);
/*  57 */       Method extraFlagField = clazz.getMethod("setExtraFlags", new Class[] { Integer.TYPE, Integer.TYPE });
/*  58 */       extraFlagField.invoke(activity.getWindow(), new Object[] { Integer.valueOf(darkmode ? darkModeFlag : 0), Integer.valueOf(darkModeFlag) });
/*  59 */       return true;
/*     */     } catch (Exception e) {
/*  61 */       e.printStackTrace();
/*     */     }
/*  63 */     return false;
/*     */   }
/*     */   
/*     */   private static boolean setFlymeLightStatusBar(Activity activity, boolean dark) {
/*  67 */     boolean result = false;
/*  68 */     if (activity != null) {
/*     */       try {
/*  70 */         WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
/*     */         
/*  72 */         Field darkFlag = WindowManager.LayoutParams.class.getDeclaredField("MEIZU_FLAG_DARK_STATUS_BAR_ICON");
/*     */         
/*  74 */         Field meizuFlags = WindowManager.LayoutParams.class.getDeclaredField("meizuFlags");
/*  75 */         darkFlag.setAccessible(true);
/*  76 */         meizuFlags.setAccessible(true);
/*  77 */         int bit = darkFlag.getInt(null);
/*  78 */         int value = meizuFlags.getInt(lp);
/*  79 */         if (dark) {
/*  80 */           value |= bit;
/*     */         } else {
/*  82 */           value &= (bit ^ 0xFFFFFFFF);
/*     */         }
/*  84 */         meizuFlags.setInt(lp, value);
/*  85 */         activity.getWindow().setAttributes(lp);
/*  86 */         result = true;
/*     */       }
/*     */       catch (Exception localException) {}
/*     */     }
/*  90 */     return result;
/*     */   }
/*     */   
/*     */   private static void setAndroidNativeLightStatusBar(Activity activity, boolean dark) {
/*  94 */     View decor = activity.getWindow().getDecorView();
/*  95 */     if (dark) {
/*  96 */       String model = Build.MODEL;
/*  97 */       if ((TextUtils.isEmpty(model)) || (!model.startsWith("Le")))
/*     */       {
/*  99 */         decor.setSystemUiVisibility(8192);
/*     */       }
/*     */       
/*     */     }
/*     */     else
/*     */     {
/* 105 */       decor.setSystemUiVisibility(0);
/*     */     }
/*     */   }
/*     */ }


/* Location:              D:\new\picture-library\classes.jar!\com\luck\picture\lib\tools\LightStatusBarUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */