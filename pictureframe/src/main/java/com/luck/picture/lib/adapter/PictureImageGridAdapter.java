package com.luck.picture.lib.adapter;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.luck.picture.lib.R;
import com.luck.picture.lib.R.anim;
import com.luck.picture.lib.R.color;
import com.luck.picture.lib.R.drawable;
import com.luck.picture.lib.R.id;
import com.luck.picture.lib.R.layout;
import com.luck.picture.lib.R.string;
import com.luck.picture.lib.anim.OptAnimationLoader;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.PictureSelectionConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.DateUtils;
import com.luck.picture.lib.tools.DebugUtil;
import com.luck.picture.lib.tools.StringUtils;
import com.luck.picture.lib.tools.VoiceUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PictureImageGridAdapter
        extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private static final int DURATION = 450;
    private Context context;
    private boolean showCamera = true;
    private OnPhotoSelectChangedListener imageSelectChangedListener;
    private int maxSelectNum;
    private List<LocalMedia> images = new ArrayList();
    private List<LocalMedia> selectImages = new ArrayList();
    private boolean enablePreview;
    private int selectMode = 2;
    private boolean enablePreviewVideo = false;
    private boolean enablePreviewAudio = false;
    private boolean is_checked_num;
    private boolean enableVoice;
    private int overrideWidth;
    private int overrideHeight;
    private float sizeMultiplier;
    private Animation animation;
    private PictureSelectionConfig config;
    private int mimeType;
    private boolean zoomAnim;

    public PictureImageGridAdapter(Context context, PictureSelectionConfig config)
    {
        this.context = context;
        this.config = config;
        this.selectMode = config.selectionMode;
        this.showCamera = config.isCamera;
        this.maxSelectNum = config.maxSelectNum;
        this.enablePreview = config.enablePreview;
        this.enablePreviewVideo = config.enPreviewVideo;
        this.enablePreviewAudio = config.enablePreviewAudio;
        this.is_checked_num = config.checkNumMode;
        this.overrideWidth = config.overrideWidth;
        this.overrideHeight = config.overrideHeight;
        this.enableVoice = config.openClickSound;
        this.sizeMultiplier = config.sizeMultiplier;
        this.mimeType = config.mimeType;
        this.zoomAnim = config.zoomAnim;
        this.animation = OptAnimationLoader.loadAnimation(context, R.anim.modal_in);
    }

    public void setShowCamera(boolean showCamera)
    {
        this.showCamera = showCamera;
    }

    public void bindImagesData(List<LocalMedia> images)
    {
        this.images = images;
        notifyDataSetChanged();
    }

    public void bindSelectImages(List<LocalMedia> images)
    {
        List<LocalMedia> selection = new ArrayList();
        for (LocalMedia media : images) {
            selection.add(media);
        }
        this.selectImages = selection;
        subSelectPosition();
        if (this.imageSelectChangedListener != null) {
            this.imageSelectChangedListener.onChange(this.selectImages);
        }
    }

    public List<LocalMedia> getSelectedImages()
    {
        if (this.selectImages == null) {
            this.selectImages = new ArrayList();
        }
        return this.selectImages;
    }

    public List<LocalMedia> getImages()
    {
        if (this.images == null) {
            this.images = new ArrayList();
        }
        return this.images;
    }

    public int getItemViewType(int position)
    {
        if ((this.showCamera) && (position == 0)) {
            return 1;
        }
        return 2;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        if (viewType == 1)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.picture_item_camera, parent, false);
            return new HeaderViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.picture_image_grid_item, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position)
    {
        if (getItemViewType(position) == 1)
        {
            HeaderViewHolder headerHolder = (HeaderViewHolder)holder;
            headerHolder.headerView.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    if (PictureImageGridAdapter.this.imageSelectChangedListener != null) {
                        PictureImageGridAdapter.this.imageSelectChangedListener.onTakePhoto();
                    }
                }
            });
        }
        else
        {
            final ViewHolder contentHolder = (ViewHolder)holder;
            final LocalMedia image = (LocalMedia)this.images.get(this.showCamera ? position - 1 : position);
            image.position = contentHolder.getAdapterPosition();
            final String path = image.getPath();
            String pictureType = image.getPictureType();
            contentHolder.ll_check.setVisibility(this.selectMode == 1 ? 8 : 0);
            if (this.is_checked_num) {
                notifyCheckChanged(contentHolder, image);
            }
            selectImage(contentHolder, isSelected(image), false);

            final int picture = PictureMimeType.isPictureType(pictureType);
            boolean gif = PictureMimeType.isGif(pictureType);
            contentHolder.tv_isGif.setVisibility(gif ? 0 : 8);
            if (this.mimeType == PictureMimeType.ofAudio())
            {
                contentHolder.tv_duration.setVisibility(0);
                Drawable drawable = ContextCompat.getDrawable(this.context, R.drawable.picture_audio);
                StringUtils.modifyTextViewDrawable(contentHolder.tv_duration, drawable, 0);
            }
            else
            {
                Drawable drawable = ContextCompat.getDrawable(this.context, R.drawable.video_icon);
                StringUtils.modifyTextViewDrawable(contentHolder.tv_duration, drawable, 0);
                contentHolder.tv_duration.setVisibility(picture == 2 ? 0 : 8);
            }
            int width = image.getWidth();
            int height = image.getHeight();
            int h = width * 5;
            contentHolder.tv_long_chart.setVisibility(height > h ? 0 : 8);
            long duration = image.getDuration();
            contentHolder.tv_duration.setText(DateUtils.timeParse(duration));
            if (this.mimeType == PictureMimeType.ofAudio())
            {
                contentHolder.iv_picture.setImageResource(R.drawable.audio_placeholder);
            }
            else
            {
                RequestOptions options = new RequestOptions();
                if ((this.overrideWidth <= 0) && (this.overrideHeight <= 0)) {
                    options.sizeMultiplier(this.sizeMultiplier);
                } else {
                    options.override(this.overrideWidth, this.overrideHeight);
                }
                options.diskCacheStrategy(DiskCacheStrategy.ALL);
                options.centerCrop();
                options.placeholder(R.drawable.image_placeholder);
                Glide.with(this.context)
                        .asBitmap()

                        .load(path)
                        .apply(options)
                        .into(contentHolder.iv_picture);
            }
            if ((this.enablePreview) || (this.enablePreviewVideo) || (this.enablePreviewAudio)) {
                contentHolder.ll_check.setOnClickListener(new View.OnClickListener()
                {
                    public void onClick(View v)
                    {
                        PictureImageGridAdapter.this.changeCheckboxState(contentHolder, image);
                    }
                });
            }
            contentHolder.contentView.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    if (!new File(path).exists())
                    {
                        Toast.makeText(PictureImageGridAdapter.this.context, PictureImageGridAdapter.this.context.getString(R.string.picture_error), 1).show();
                        return;
                    }
                    if ((picture == 1) && ((PictureImageGridAdapter.this.enablePreview) ||
                            (PictureImageGridAdapter.this.selectMode == 1)))
                    {
                        int index = PictureImageGridAdapter.this.showCamera ? position - 1 : position;
                        PictureImageGridAdapter.this.imageSelectChangedListener.onPictureClick(image, index);
                    }
                    else if ((picture == 2) && ((PictureImageGridAdapter.this.enablePreviewVideo) ||
                            (PictureImageGridAdapter.this.selectMode == 1)))
                    {
                        int index = PictureImageGridAdapter.this.showCamera ? position - 1 : position;
                        PictureImageGridAdapter.this.imageSelectChangedListener.onPictureClick(image, index);
                    }
                    else if ((picture == 3) && ((PictureImageGridAdapter.this.enablePreviewAudio) ||
                            (PictureImageGridAdapter.this.selectMode == 1)))
                    {
                        int index = PictureImageGridAdapter.this.showCamera ? position - 1 : position;
                        PictureImageGridAdapter.this.imageSelectChangedListener.onPictureClick(image, index);
                    }
                    else
                    {
                        PictureImageGridAdapter.this.changeCheckboxState(contentHolder, image);
                    }
                }
            });
        }
    }

    public int getItemCount()
    {
        return this.showCamera ? this.images.size() + 1 : this.images.size();
    }

    public class HeaderViewHolder
            extends RecyclerView.ViewHolder
    {
        View headerView;
        TextView tv_title_camera;

        public HeaderViewHolder(View itemView)
        {
            super(itemView);
            this.headerView = itemView;
            this.tv_title_camera = ((TextView)itemView.findViewById(R.id.tv_title_camera));

            String title = PictureImageGridAdapter.this.mimeType == PictureMimeType.ofAudio() ? PictureImageGridAdapter.this.context.getString(R.string.picture_tape) : PictureImageGridAdapter.this.context.getString(R.string.picture_take_picture);
            this.tv_title_camera.setText(title);
        }
    }

    public class ViewHolder
            extends RecyclerView.ViewHolder
    {
        ImageView iv_picture;
        TextView check;
        TextView tv_duration;
        TextView tv_isGif;
        TextView tv_long_chart;
        View contentView;
        LinearLayout ll_check;

        public ViewHolder(View itemView)
        {
            super(itemView);
            this.contentView = itemView;
            this.iv_picture = ((ImageView)itemView.findViewById(R.id.iv_picture));
            this.check = ((TextView)itemView.findViewById(R.id.check));
            this.ll_check = ((LinearLayout)itemView.findViewById(R.id.ll_check));
            this.tv_duration = ((TextView)itemView.findViewById(R.id.tv_duration));
            this.tv_isGif = ((TextView)itemView.findViewById(R.id.tv_isGif));
            this.tv_long_chart = ((TextView)itemView.findViewById(R.id.tv_long_chart));
        }
    }

    public boolean isSelected(LocalMedia image)
    {
        for (LocalMedia media : this.selectImages) {
            if (media.getPath().equals(image.getPath())) {
                return true;
            }
        }
        return false;
    }

    private void notifyCheckChanged(ViewHolder viewHolder, LocalMedia imageBean)
    {
        viewHolder.check.setText("");
        for (LocalMedia media : this.selectImages) {
            if (media.getPath().equals(imageBean.getPath()))
            {
                imageBean.setNum(media.getNum());
                media.setPosition(imageBean.getPosition());
                viewHolder.check.setText(String.valueOf(imageBean.getNum()));
            }
        }
    }

    private void changeCheckboxState(ViewHolder contentHolder, LocalMedia image)
    {
        boolean isChecked = contentHolder.check.isSelected();
        String pictureType = this.selectImages.size() > 0 ? ((LocalMedia)this.selectImages.get(0)).getPictureType() : "";
        if (!TextUtils.isEmpty(pictureType))
        {
            boolean toEqual = PictureMimeType.mimeToEqual(pictureType, image.getPictureType());
            if (!toEqual)
            {
                Toast.makeText(this.context, this.context.getString(R.string.picture_rule), 1).show(); return;
            }
        }
        boolean eqImg;
        if ((this.selectImages.size() >= this.maxSelectNum) && (!isChecked))
        {
            eqImg = pictureType.startsWith("image");

            String str = eqImg ? this.context.getString(R.string.picture_message_max_num, new Object[] { Integer.valueOf(this.maxSelectNum) }) : this.context.getString(R.string.picture_message_video_max_num, new Object[] {Integer.valueOf(this.maxSelectNum) });
            Toast.makeText(this.context, str, 1).show();
            return;
        }
        if (isChecked)
        {
            for (LocalMedia media : this.selectImages) {
                if (media.getPath().equals(image.getPath()))
                {
                    this.selectImages.remove(media);
                    DebugUtil.i("selectImages remove::", this.config.selectionMedias.size() + "");
                    subSelectPosition();
                    disZoom(contentHolder.iv_picture);
                    break;
                }
            }
        }
        else
        {
            this.selectImages.add(image);
            DebugUtil.i("selectImages add::", this.config.selectionMedias.size() + "");
            image.setNum(this.selectImages.size());
            VoiceUtils.playVoice(this.context, this.enableVoice);
            zoom(contentHolder.iv_picture);
        }
        notifyItemChanged(contentHolder.getAdapterPosition());
        selectImage(contentHolder, !isChecked, true);
        if (this.imageSelectChangedListener != null) {
            this.imageSelectChangedListener.onChange(this.selectImages);
        }
    }

    private void subSelectPosition()
    {
        if (this.is_checked_num)
        {
            int size = this.selectImages.size();
            int index = 0;
            for (int length = size; index < length; index++)
            {
                LocalMedia media = (LocalMedia)this.selectImages.get(index);
                media.setNum(index + 1);
                notifyItemChanged(media.position);
            }
        }
    }

    public void selectImage(ViewHolder holder, boolean isChecked, boolean isAnim)
    {
        holder.check.setSelected(isChecked);
        if (isChecked)
        {
            if ((isAnim) &&
                    (this.animation != null)) {
                holder.check.startAnimation(this.animation);
            }
            holder.iv_picture.setColorFilter(
                    ContextCompat.getColor(this.context, R.color.image_overlay_true), PorterDuff.Mode.SRC_ATOP);
        }
        else
        {
            holder.iv_picture.setColorFilter(
                    ContextCompat.getColor(this.context, R.color.image_overlay_false), PorterDuff.Mode.SRC_ATOP);
        }
    }

    public void setOnPhotoSelectChangedListener(OnPhotoSelectChangedListener imageSelectChangedListener)
    {
        this.imageSelectChangedListener = imageSelectChangedListener;
    }

    private void zoom(ImageView iv_img)
    {
        if (this.zoomAnim)
        {
            AnimatorSet set = new AnimatorSet();
            set.playTogether(new Animator[] {
                    ObjectAnimator.ofFloat(iv_img, "scaleX", new float[] { 1.0F, 1.12F }),
                    ObjectAnimator.ofFloat(iv_img, "scaleY", new float[] { 1.0F, 1.12F }) });

            set.setDuration(450L);
            set.start();
        }
    }

    private void disZoom(ImageView iv_img)
    {
        if (this.zoomAnim)
        {
            AnimatorSet set = new AnimatorSet();
            set.playTogether(new Animator[] {
                    ObjectAnimator.ofFloat(iv_img, "scaleX", new float[] { 1.12F, 1.0F }),
                    ObjectAnimator.ofFloat(iv_img, "scaleY", new float[] { 1.12F, 1.0F }) });

            set.setDuration(450L);
            set.start();
        }
    }

    public static abstract interface OnPhotoSelectChangedListener
    {
        public abstract void onTakePhoto();

        public abstract void onChange(List<LocalMedia> paramList);

        public abstract void onPictureClick(LocalMedia paramLocalMedia, int paramInt);
    }
}
