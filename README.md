﻿# Android图片框架

## Please fix the following before submitting a JCenter inclusion request:- Add a POM file to the latest version of your package ：移步：https://blog.csdn.net/lxd_android/article/details/79076312
### 1、aar上传Jcenter仓库（bintray.com/： 使用Google 账号登录  操作流程移步：https://github.com/hehonghui/android-tech-frontier/blob/master/issue-17/%E5%A6%82%E4%BD%95%E4%BD%BF%E7%94%A8Android-Studio%E6%8A%8A%E8%87%AA%E5%B7%B1%E7%9A%84Android-library%E5%88%86%E5%8F%91%E5%88%B0jCenter%E5%92%8CMaven-Central.md）
```java
		apply plugin: 'com.github.dcendents.android-maven'
        
        group = publishedGroupId                               // Maven Group ID for the artifact
        
        install {
            repositories.mavenInstaller {
                // This generates POM.xml with proper parameters
                pom {
                    project {
                        packaging 'aar'
                        groupId publishedGroupId
                        artifactId artifact
        
                        // Add your description here
                        name libraryName
                        description libraryDescription
                        url siteUrl
        
                        // Set your license
                        licenses {
                            license {
                                name licenseName
                                url licenseUrl
                            }
                        }
                        developers {
                            developer {
                                id developerId
                                name developerName
                                email developerEmail
                            }
                        }
                        scm {
                            connection gitUrl
                            developerConnection gitUrl
                            url siteUrl
        
                        }
                    }
                }
            }
        }
```
